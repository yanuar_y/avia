package travel.avia.tour.controls.utils;

import android.util.Log;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

/**
 * Created by fitrahramadhan on 1/15/2016.
 */
public class Formatter {
    private final static String TAG = "Formatter";

    public String RupiahFormat(String strPrice){
        try {
            Double price = Double.parseDouble(strPrice);
            DecimalFormat rupiah = (DecimalFormat) DecimalFormat.getCurrencyInstance();
            DecimalFormatSymbols Rp = new DecimalFormatSymbols();
            Rp.setCurrencySymbol("IDR ");
            //Rp.setMonetaryDecimalSeparator(',');
            Rp.setGroupingSeparator('.');
            rupiah.setDecimalFormatSymbols(Rp);
            strPrice = rupiah.format(price);
            return strPrice.substring(0, strPrice.length()-3);
        }
        catch (NumberFormatException nfx){
            return strPrice;
        }
    }
    public String DateFormat(String tanggal) {
        String date;
        String bagi[] = tanggal.split("-");
        String tahun = bagi[0];
        String bulan = bagi[1];
        String hari = bagi[2];
        if (bulan.equals("01")) bulan = "Jan";
        else if (bulan.equals("02")) bulan = "Feb";
        else if (bulan.equals("03")) bulan = "Mar";
        else if (bulan.equals("04")) bulan = "Apr";
        else if (bulan.equals("05")) bulan = "May";
        else if (bulan.equals("06")) bulan = "Jun";
        else if (bulan.equals("07")) bulan = "Jul";
        else if (bulan.equals("08")) bulan = "Aug";
        else if (bulan.equals("09")) bulan = "Sep";
        else if (bulan.equals("10")) bulan = "Oct";
        else if (bulan.equals("11")) bulan = "Nov";
        else if (bulan.equals("12")) bulan = "Dec";
        date = hari+" "+bulan+" "+tahun;
        return date;
    }

    /**
     * format date from yy-mm-dd H:i:s
     * @param tanggal
     * @return
     */
    public String TanggalFormat(String tanggal) {
        // tanggal = 2017-06-21 00:00:00
        String date;
        String bagi[] = tanggal.split("-");
        String tahun = bagi[0];
        String bulan = bagi[1];
        String hari = bagi[2].split(" ")[0];
        if (bulan.equals("01")) bulan = "Jan";
        else if (bulan.equals("02")) bulan = "Feb";
        else if (bulan.equals("03")) bulan = "Mar";
        else if (bulan.equals("04")) bulan = "Apr";
        else if (bulan.equals("05")) bulan = "May";
        else if (bulan.equals("06")) bulan = "Jun";
        else if (bulan.equals("07")) bulan = "Jul";
        else if (bulan.equals("08")) bulan = "Aug";
        else if (bulan.equals("09")) bulan = "Sep";
        else if (bulan.equals("10")) bulan = "Oct";
        else if (bulan.equals("11")) bulan = "Nov";
        else if (bulan.equals("12")) bulan = "Dec";
        date = hari+" "+bulan+" "+tahun;
        return date;
    }
}