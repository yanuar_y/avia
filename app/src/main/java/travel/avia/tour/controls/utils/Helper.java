package travel.avia.tour.controls.utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Toast;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import travel.avia.tour.R;
import uk.co.senab.photoview.PhotoView;
import uk.co.senab.photoview.PhotoViewAttacher;

/**
 * Created by ss on 6/21/17.
 */

public class Helper {

    /**
     * Create fullscreen zoom Image with dialog
     * @param context
     * @param url
     */
    public static void zoomImage(final Context context, String url) {

        // build dialog
        final AlertDialog.Builder builder = new AlertDialog.Builder(context, android.R.style.Theme_Translucent_NoTitleBar_Fullscreen);
        View dialogLayout = LayoutInflater.from(context).inflate(R.layout.dialog_full_image, null);
        PhotoView fullImage = (PhotoView) dialogLayout.findViewById(R.id.fullimage);

        // create dialog
        final AlertDialog dialog = builder.create();
        dialog.setView(dialogLayout);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#D8000000"))); // set backgournd black 85% opacity
        dialog.show();

        // load full image
        final ProgressDialog pd = ProgressDialog.show(context, "", "Loading...", true);
        Picasso.with(context)
                .load(url)
                .into(fullImage, new Callback() {
                    @Override
                    public void onSuccess() {
                        pd.dismiss();
                    }

                    @Override
                    public void onError() {
                        pd.dismiss();
                        dialog.dismiss();
                        Toast.makeText(context, "Something went wrong when loading the image", Toast.LENGTH_SHORT).show();
                    }
                }); // set url for full image

        // close dialog when image tapped once
        fullImage.setOnViewTapListener(new PhotoViewAttacher.OnViewTapListener() {
            @Override
            public void onViewTap(View view, float x, float y) {
                dialog.dismiss();
            }
        });
    }
}
