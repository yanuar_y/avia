package travel.avia.tour.controls.gcm;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.android.gms.gcm.GcmListenerService;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import travel.avia.tour.BuildConfig;
import travel.avia.tour.R;
import travel.avia.tour.controls.utils.Formatter;
import travel.avia.tour.views.activity.MainActivity;
import travel.avia.tour.views.activity.offerandpromotion.DetailPromotionActivity;

/**
 * Created by fitrahramadhan on 1/23/16.
 */
public class GCMListener extends GcmListenerService {
    private final String TAG = "GCMListener";
    static String title, summary, url;
    @Override
    public void onMessageReceived(String from, Bundle data) {
        JSONObject message = null;
        try {
            message = new JSONObject(data.getString("message"));
            title = message.getString("title");
            summary = message.getString("description");
            url = BuildConfig.URL_IMAGE_PROMO + message.getString("image");
        }
        catch (JSONException je) {}
        SharedPreferences sharedpreferences = getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putInt("numPromo", sharedpreferences.getInt("numPromo", 0)+1);
        editor.commit();
        Log.i("DEBUG_"+TAG, "numPromo = "+sharedpreferences.getInt("numPromo", 0));
        NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(0, setBigPictureStyleNotification(message));
    }
    private Notification setBigPictureStyleNotification(JSONObject message) {
        Bitmap remote_picture = null;
        NotificationCompat.BigPictureStyle notiStyle = new NotificationCompat.BigPictureStyle();
        notiStyle.setBigContentTitle(title);

        try {
            remote_picture = BitmapFactory.decodeStream((InputStream) new URL(url).getContent());
        } catch (IOException e) {
            e.printStackTrace();
        }
        notiStyle.bigPicture(remote_picture);

        try {
            Log.i("DEBUG_"+TAG, message.toString());
            Formatter formatter = new Formatter();
            DetailPromotionActivity.title= title;
            DetailPromotionActivity.image = url;
            DetailPromotionActivity.id = message.getJSONObject("0").getString("id");
            DetailPromotionActivity.id_product = message.getString("id_product");
            DetailPromotionActivity.valid = formatter.TanggalFormat(message.getString("end_date"));
            DetailPromotionActivity.category = message.getInt("category");
        }
        catch (JSONException je) {
            Log.i("DEBUG_"+TAG, je.toString());
        }
        MainActivity.notif = true;
        Intent resultIntent = new Intent(this, MainActivity.class);

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addParentStack(MainActivity.class);
        stackBuilder.addNextIntent(resultIntent);
        int color = getResources().getColor(R.color.aviaColor);
        PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
        Bitmap largeIcon = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher);
        return new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setLargeIcon(largeIcon)
                .setAutoCancel(true)
                .setContentIntent(resultPendingIntent)
                .setContentTitle(title)
                .setContentText(summary)
                .setColor(color)
                .setStyle(notiStyle).build();
    }
}
