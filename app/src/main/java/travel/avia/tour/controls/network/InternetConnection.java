package travel.avia.tour.controls.network;

import android.content.Context;
import android.net.ConnectivityManager;

/**
 * Created by fitrahramadhan on 5/18/16.
 */
public class InternetConnection {
    public static boolean isAvailable(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isAvailable()
                && cm.getActiveNetworkInfo().isConnected()) return true;
        else return false;
    }
}
