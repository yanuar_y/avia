package travel.avia.tour.controls.gcm;


import android.app.IntentService;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;

import com.google.android.gms.gcm.GcmPubSub;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;

import java.io.IOException;

import travel.avia.tour.R;

public class Registrasi extends IntentService {
    public Registrasi() {
        super("Reg");
    }
    @Override
    protected void onHandleIntent(Intent intent) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
        try {
            synchronized("Reg") {
                InstanceID instanceID = InstanceID.getInstance(this);
                String token = instanceID.getToken(getString(R.string.gcm_id), GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
                langganan(token);
                sp.edit().putBoolean("kirimToken", true).apply();
            }
        }
        catch (IOException e) {
            sp.edit().putBoolean("kirimToken", false).apply();
        }
        Intent kelarDaftar = new Intent("Selesai");
        LocalBroadcastManager.getInstance(this).sendBroadcast(kelarDaftar);
    }

    private void langganan(String token) throws IOException {
        GcmPubSub pubSub = GcmPubSub.getInstance(this);
        pubSub.subscribe(token,"/topics/message", null);
    }
}
