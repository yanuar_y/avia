package travel.avia.tour.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by fitrahramadhan on 4/30/16.
 */
public class DBHelper extends SQLiteOpenHelper {
    public static final String DB_NAME = "avia_tour";
    public static final int DB_VERSION = 1;

    public static final String TABLE_TOUR_NAME = "tour";
    public static final String COLUMN_TOUR_ID = "tour_id";
    public static final String COLUMN_TOUR_NAME = "tour_name";
    public static final String COLUMN_TOUR_DESCRIPTION = "tour_description";
    public static final String COLUMN_TOUR_PRICE = "tour_price";
    public static final String COLUMN_TOUR_IMAGE = "tour_image";

    public static final String TABLE_BOOK_NAME = "booking";
    public static final String COLUMN_BOOK_DATA = "data";
    public static final String COLUMN_BOOK_DATE = "date";

    public DBHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE "
                + TABLE_TOUR_NAME + "("
                + COLUMN_TOUR_ID + " varchar(6) primary key NOT NULL, "
                + COLUMN_TOUR_NAME + " varchar(50) NOT NULL, "
                + COLUMN_TOUR_DESCRIPTION + " varchar(400) NOT NULL, "
                + COLUMN_TOUR_PRICE + " varchar(20) NOT NULL, "
                + COLUMN_TOUR_IMAGE + " varchar(50) NOT NULL)"
                );
        db.execSQL("CREATE TABLE "
                + TABLE_BOOK_NAME + "("
                + COLUMN_BOOK_DATA + " text primary key NOT NULL, "
                + COLUMN_BOOK_DATE + " varchar(20) NOT NULL)"
                );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_TOUR_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_BOOK_NAME);
        onCreate(db);
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_TOUR_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_BOOK_NAME);
        onCreate(db);
    }
}
