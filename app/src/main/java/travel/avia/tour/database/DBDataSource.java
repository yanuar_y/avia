package travel.avia.tour.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by fitrahramadhan on 4/30/16.
 */
public class DBDataSource {
    private Context context;
    private SQLiteDatabase database;
    private DBHelper dbHelper;
    private String[] columnsTour = { DBHelper.COLUMN_TOUR_ID, DBHelper.COLUMN_TOUR_NAME,
            DBHelper.COLUMN_TOUR_DESCRIPTION, DBHelper.COLUMN_TOUR_PRICE, DBHelper.COLUMN_TOUR_IMAGE
            };
    private String[] columnsBooking = { DBHelper.COLUMN_BOOK_DATA, DBHelper.COLUMN_BOOK_DATE };

    public DBDataSource (Context context) {
        this.context = context;
        dbHelper = new DBHelper(context);
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }

    private Tour cursorToTour(Cursor cursor) {
        Tour tour = new Tour();
        tour.setTourId(cursor.getString(0));
        tour.setTourName(cursor.getString(1));
        tour.setTourDescription(cursor.getString(2));
        tour.setTourPrice(cursor.getString(3));
        tour.setTourImage(cursor.getString(4));
        return tour;
    }

    private Booking cursorToBooking(Cursor cursor) {
        Booking booking = new Booking();
        try {
            booking.setData(new JSONObject(cursor.getString(0)));
        }
        catch (JSONException je){}
        booking.setDate(cursor.getString(1));
        return booking;
    }

    public void insertTour(String tourId, String tourName, String tourDescription, String tourPrice,
                           String tourImage) {
        ContentValues values = new ContentValues();
        values.put(columnsTour[0], tourId);
        values.put(columnsTour[1], tourName);
        values.put(columnsTour[2], tourDescription);
        values.put(columnsTour[3], tourPrice);
        values.put(columnsTour[4], tourImage);
        database.insert(DBHelper.TABLE_TOUR_NAME, null, values);
    }

    public void insertBooking(String data, String date) {
        ContentValues values = new ContentValues();
        values.put(columnsBooking[0], data);
        values.put(columnsBooking[1], date);
        database.insert(DBHelper.TABLE_BOOK_NAME, null, values);
    }

    public ArrayList<Tour> getAllTour() {
        ArrayList<Tour> daftarTour = new ArrayList<Tour>();
        Cursor cursor = database.query(DBHelper.TABLE_TOUR_NAME, columnsTour, null, null, null, null, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Tour tour = cursorToTour(cursor);
            daftarTour.add(tour);
            cursor.moveToNext();
        }
        cursor.close();
        return daftarTour;
    }

    public ArrayList<Booking> getAllBooking() {
        ArrayList<Booking> daftarBooking = new ArrayList<Booking>();
        Cursor cursor = database.query(DBHelper.TABLE_BOOK_NAME, columnsBooking, null, null, null, null, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Booking booking = cursorToBooking(cursor);
            daftarBooking.add(booking);
            cursor.moveToNext();
        }
        cursor.close();
        return daftarBooking;
    }

    public void deleteTour(String tourId) {
        String strFilter = DBHelper.COLUMN_TOUR_ID + " = " +  tourId;
        database.delete(DBHelper.TABLE_TOUR_NAME, strFilter, null);
    }

    public boolean findTour(String tourId) {
        String strFilter = DBHelper.COLUMN_TOUR_ID + " = " + tourId;
        Cursor cursor = database.query(DBHelper.TABLE_TOUR_NAME, columnsTour, strFilter, null, null, null, null);
        if (cursor.getCount() > 0) {
            return true;
        }
        else return false;
    }
}
