package travel.avia.tour.database;

import org.json.JSONObject;

/**
 * Created by fitrahramadhan on 5/6/16.
 */
public class Booking {
    private JSONObject data;
    private String date;

    public JSONObject getData() {
        return data;
    }

    public void setData(JSONObject data) {
        this.data = data;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
