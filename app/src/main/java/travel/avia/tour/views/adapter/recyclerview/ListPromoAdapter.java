package travel.avia.tour.views.adapter.recyclerview;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import travel.avia.tour.R;
import travel.avia.tour.controls.utils.Helper;
import uk.co.senab.photoview.PhotoView;
import uk.co.senab.photoview.PhotoViewAttacher;

/**
 * Created by EnjoyTeam on 20/11/2015.
 */
public class ListPromoAdapter extends RecyclerView.Adapter<ListPromoAdapter.ViewHolder>{
    Context context;
    String[] title, image, validity;

    public ListPromoAdapter(Context context, String title[], String image[], String validty[]) {
        super();
        this.context=context;
        this.title = title;
        this.image = image;
        this.validity = validty;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        this.context = parent.getContext();
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_list_promotions, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        Picasso.with(context).load(image[position]).fit().into(holder.img);
        holder.judul.setText(title[position]);
        holder.validity.setText("Offer Validity : "+validity[position]);

        holder.img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Helper.zoomImage(context, image[position]);
            }
        });
    }

    @Override
    public int getItemCount() {
        return image.length;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView img;
        TextView judul, validity;
        public ViewHolder(View v) {
            super(v);
            img=(ImageView) v.findViewById(R.id.promo_image);
            judul=(TextView)v.findViewById(R.id.promo_title);
            validity=(TextView)v.findViewById(R.id.promo_validity);
        }
    }
}
