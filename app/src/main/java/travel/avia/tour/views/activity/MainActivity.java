package travel.avia.tour.views.activity;

import android.app.AlertDialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import travel.avia.tour.R;
import travel.avia.tour.views.activity.offerandpromotion.DetailPromotionActivity;
import travel.avia.tour.views.activity.tour.RegionActivity;
import travel.avia.tour.views.dialog.SearchDialogFragmnet;
import travel.avia.tour.views.fragment.HomeFragment;
import travel.avia.tour.views.fragment.contactus.ContactUsFragment;
import travel.avia.tour.views.fragment.favorite.FavoriteFragment;
import travel.avia.tour.views.fragment.mybooking.ListMyBookingFragment;
import travel.avia.tour.views.fragment.offerpromotion.OfferPromotionFragment;
import travel.avia.tour.views.custom.recyclerview.CustomDrawerLayout;


public class MainActivity extends AppCompatActivity implements
        NavigationView.OnNavigationItemSelectedListener {

    public static boolean notif=false;

    private final String TAG = "MainActivity";
    private SharedPreferences sharedpreferences;
    private CustomDrawerLayout drawer;
    private NavigationView navigationView;
    private FragmentManager fm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Log.d("DEBUG_"+TAG, "Initial object shared preferences");
        sharedpreferences = getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);

        Log.d("DEBUG_"+TAG, "Set toolbar");
        Toolbar toolbar = (Toolbar) findViewById(R.id.main_toolbar);
        setSupportActionBar(toolbar);
        //ActionBar act = getSupportActionBar();
        /*View actionBarCustomView = this.getLayoutInflater().inflate(R.layout.custom_act, null);
        ImageView actionBarLogo = (ImageView) actionBarCustomView.findViewById(R.id.actionBarLogo);
        Picasso.with(this).load(R.drawable.aviatour).into(actionBarLogo);
        toolbar.addView(actionBarCustomView);
        act.setCustomView(actionBarCustomView);
        act.setDisplayShowTitleEnabled(false);
        act.setDisplayShowCustomEnabled(true);
        act.setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);*/

        Log.d("DEBUG_"+TAG, "Set drawer");
        drawer = (CustomDrawerLayout) findViewById(R.id.main_drawer_layout);
        ActionBarDrawerToggle mDrawerToggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open,
                R.string.navigation_drawer_close);
        drawer.setDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();

        Log.d("DEBUG_"+TAG, "Set navigation view");
        navigationView = (NavigationView) findViewById(R.id.main_nav_view);
        navigationView.setItemIconTintList(null);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.getMenu().getItem(0).setChecked(true);

        fm = getFragmentManager();

        Log.d("DEBUG_"+TAG, "Set first fragment - HomeFragment");
        HomeFragment.mainActivity = this;
        changeFragment(new HomeFragment(), "HomeFragment");

        if (notif) {
            changeFragment(new OfferPromotionFragment(), "OfferPromotionFragment");
            startActivity(new Intent(MainActivity.this, DetailPromotionActivity.class));
            notif = false;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        Log.d("DEBUG_"+TAG, "On create menu");
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        Log.d("DEBUG_"+TAG, "Menu item clicked");
        if (id == android.R.id.home) {
            Log.d("DEBUG_"+TAG, "Humberger clicked");
            if (drawer.isDrawerOpen(GravityCompat.START)) {
                drawer.closeDrawer(GravityCompat.START);
            }
            else drawer.openDrawer(GravityCompat.START);
        }
        else if (id == R.id.search_menu) {
            Log.d("DEBUG_"+TAG, "Menu search clicked");
            SearchDialogFragmnet newFragment = SearchDialogFragmnet.newInstance();
            newFragment.setStyle(DialogFragment.STYLE_NO_TITLE, 0);
            newFragment.show(getFragmentManager(), "dialog");
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        Log.d("DEBUG_"+TAG,"back pressed");
        if (fm.getBackStackEntryCount() > 1) {
            Log.d("DEBUG_"+TAG, "Back to previous fragment");
            fm.popBackStack();
        }
        else {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
            alertDialogBuilder.setMessage("Are you sure you want to exit?");
            alertDialogBuilder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface arg0, int arg1) {
                    finish();
                }
            });
            alertDialogBuilder.setNegativeButton("No",new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {}
            });
            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.home) {
            Log.d("DEBUG_"+TAG, "Nav home selected");
            changeFragment(new HomeFragment(), "HomeFragment");
        }
        else if (id == R.id.tour) {
            Log.d("DEBUG_"+TAG, "Nav tour selected");
            startActivity(new Intent(this, RegionActivity.class));
        }
        else if (id == R.id.paket) {
            Toast.makeText(this, "Fitur ini belum tersedia.", Toast.LENGTH_SHORT).show();
            //startActivity(new Intent(this, PackageActivity.class));
        }
        else if (id == R.id.passport_visa) {
            Toast.makeText(this, "Fitur ini belum tersedia.", Toast.LENGTH_SHORT).show();
            //startActivity(new Intent(this, PassportVisaActivity.class));
        }
        else if (id == R.id.insurance) {
            Toast.makeText(this, "Fitur ini belum tersedia.", Toast.LENGTH_SHORT).show();
            //startActivity(new Intent(this, InsuranceActivity.class));
        }
        else if (id == R.id.car_hire) {
            Toast.makeText(this, "Fitur ini belum tersedia.", Toast.LENGTH_SHORT).show();
            //startActivity(new Intent(this, CarActivity.class));
        }
        else if (id == R.id.ticket_attraction) {
            Toast.makeText(this, "Fitur ini belum tersedia.", Toast.LENGTH_SHORT).show();
            //startActivity(new Intent(this, TicketAttractionActivity.class));
        }
        else if (id == R.id.contact_us) {
            changeFragment(new ContactUsFragment(), "ContactUsFragment");
        }
        else if (id == R.id.my_trip) {
            Log.d("DEBUG_"+TAG, "Nav my trip selected");
            changeFragment(new ListMyBookingFragment(), "ListMyBookingFragment");
        }
        else if (id == R.id.my_favorite) {
            Log.d("DEBUG_"+TAG, "Nav my favorite selected");
            changeFragment(new FavoriteFragment(), "FavoriteFragment");
        }
        else if (id == R.id.offer_promotions) {
            Log.d("DEBUG_"+TAG, "Nav offer promotions selected");
            changeFragment(new OfferPromotionFragment(), "OfferPromotionFragment");
        }
        else if (id == R.id.cruise) {
            Toast.makeText(this, "Fitur ini belum tersedia.", Toast.LENGTH_SHORT).show();
            //startActivity(new Intent(this, InsuranceActivity.class));
        }
        else if (id == R.id.share) {
            Log.d("DEBUG_"+TAG, "Nav share selected");
            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.putExtra(Intent.EXTRA_TEXT, "Download Avia Travel app at https://play.google." +
                    "com/store/apps/details?id=travel.avia.tour");
            sendIntent.setType("text/plain");
            startActivity(sendIntent);
        }
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void changeTitlePromoMenu() {
        Log.i("DEBUG_"+TAG, "Num Promo = "+ sharedpreferences.getInt("numPromo", 0));
        TextView view = (TextView) navigationView.getMenu().findItem(R.id.offer_promotions).getActionView();
        int count = sharedpreferences.getInt("numPromo", 0);
        view.setText(count > 0 ? "("+count+")" : null);
    }

    public void changeFragment(Fragment fragment, String tag) {
        Log.d("DEBUG_"+TAG, "change fragment "+tag);
        if (fm.popBackStackImmediate (fragment.getClass().getName(), 0) || fm.findFragmentByTag(tag) != null) {
            fm.popBackStack(tag, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            Log.d("DEBUG_"+TAG, "Fragment "+tag+" in stack.");
        }
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.replace(R.id.main_container_fragment, fragment, tag);
        fragmentTransaction.addToBackStack(tag);
        fragmentTransaction.commit();
        Log.d("DEBUG_"+TAG, "Fragment count : "+fm.getBackStackEntryCount());
    }
}

