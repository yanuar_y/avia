package travel.avia.tour.views.adapter.recyclerview;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import travel.avia.tour.R;

/**
 * Created by fitrahramadhan on 2/7/16.
 */
public class ListContactUsAdapter extends ArrayAdapter<String> {
    private final Activity context;
    private final Integer[] icon;
    private final String[] title;

    public ListContactUsAdapter(Activity context, Integer[] icon, String[] title) {
        super(context, R.layout.adapter_list_contact_us, title);
        this.context = context;
        this.icon = icon;
        this.title = title;
    }

    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View v = inflater.inflate(R.layout.adapter_list_contact_us, null, true);

        ImageView iconContact = (ImageView) v.findViewById(R.id.icon_list_contact);
        TextView titleContact = (TextView) v.findViewById(R.id.title_list_contact);

        iconContact.setImageDrawable(context.getResources().getDrawable(icon[position]));
        titleContact.setText(title[position]);

        return v;
    }
}
