package travel.avia.tour.views.dialog;

import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import travel.avia.tour.R;
import travel.avia.tour.views.activity.tour.RegionActivity;

/**
 * Created by fitrahramadhan on 4/29/16.
 */
public class SortTourDialogFragment extends DialogFragment {
    ImageView centang[] = new ImageView[6];
    RelativeLayout teks[] = new RelativeLayout[6];

    public static SortTourDialogFragment newInstance() {
        return new SortTourDialogFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.dialog_fragment_sort, container, false);

        /*getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        //getDialog().getWindow().
        getDialog().getWindow().setGravity(Gravity.BOTTOM);
        WindowManager.LayoutParams p = getDialog().getWindow().getAttributes();
        p.width = ViewGroup.LayoutParams.MATCH_PARENT;
        p.height = ViewGroup.LayoutParams.WRAP_CONTENT;
        getDialog().getWindow().setAttributes(p);*/

        centang[0] = (ImageView) v.findViewById(R.id.centang_1);
        centang[1] = (ImageView) v.findViewById(R.id.centang_2);
        centang[2] = (ImageView) v.findViewById(R.id.centang_3);
        centang[3] = (ImageView) v.findViewById(R.id.centang_4);
        centang[4] = (ImageView) v.findViewById(R.id.centang_5);
        centang[5] = (ImageView) v.findViewById(R.id.centang_6);
        centang[RegionActivity.sort].setImageDrawable(getResources().getDrawable(R.mipmap.ic_done));

        teks[0] = (RelativeLayout) v.findViewById(R.id.teks_1);
        teks[1] = (RelativeLayout) v.findViewById(R.id.teks_2);
        teks[2] = (RelativeLayout) v.findViewById(R.id.teks_3);
        teks[3] = (RelativeLayout) v.findViewById(R.id.teks_4);
        teks[4] = (RelativeLayout) v.findViewById(R.id.teks_5);
        teks[5] = (RelativeLayout) v.findViewById(R.id.teks_6);
        teks[0].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (RegionActivity.sort != 0) {
                    centang[RegionActivity.sort].setVisibility(View.INVISIBLE);
                    RegionActivity.sort = 0;
                    centang[RegionActivity.sort].setVisibility(View.VISIBLE);
                    centang[RegionActivity.sort].setImageDrawable(getResources().getDrawable(R.mipmap.ic_done));
                    ((RegionActivity)getActivity()).refresh();
                    dismiss();
                }
            }
        });
        teks[1].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (RegionActivity.sort != 1) {
                    centang[RegionActivity.sort].setVisibility(View.INVISIBLE);
                    RegionActivity.sort = 1;
                    centang[RegionActivity.sort].setVisibility(View.VISIBLE);
                    centang[RegionActivity.sort].setImageDrawable(getResources().getDrawable(R.mipmap.ic_done));
                    ((RegionActivity)getActivity()).refresh();
                    dismiss();
                }
            }
        });
        teks[2].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (RegionActivity.sort != 2) {
                    centang[RegionActivity.sort].setVisibility(View.INVISIBLE);
                    RegionActivity.sort = 2;
                    centang[RegionActivity.sort].setVisibility(View.VISIBLE);
                    centang[RegionActivity.sort].setImageDrawable(getResources().getDrawable(R.mipmap.ic_done));
                    ((RegionActivity)getActivity()).refresh();
                    dismiss();
                }
            }
        });
        teks[3].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (RegionActivity.sort != 3) {
                    centang[RegionActivity.sort].setVisibility(View.INVISIBLE);
                    RegionActivity.sort = 3;
                    centang[RegionActivity.sort].setVisibility(View.VISIBLE);
                    centang[RegionActivity.sort].setImageDrawable(getResources().getDrawable(R.mipmap.ic_done));
                    ((RegionActivity)getActivity()).refresh();
                    dismiss();
                }
            }
        });
        teks[4].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (RegionActivity.sort != 4) {
                    centang[RegionActivity.sort].setVisibility(View.INVISIBLE);
                    RegionActivity.sort = 4;
                    centang[RegionActivity.sort].setVisibility(View.VISIBLE);
                    centang[RegionActivity.sort].setImageDrawable(getResources().getDrawable(R.mipmap.ic_done));
                    ((RegionActivity)getActivity()).refresh();
                    dismiss();
                }
            }
        });
        teks[5].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (RegionActivity.sort != 5) {
                    centang[RegionActivity.sort].setVisibility(View.INVISIBLE);
                    RegionActivity.sort = 5;
                    centang[RegionActivity.sort].setVisibility(View.VISIBLE);
                    centang[RegionActivity.sort].setImageDrawable(getResources().getDrawable(R.mipmap.ic_done));
                    ((RegionActivity)getActivity()).refresh();
                    dismiss();
                }
            }
        });
        return v;
    }
}
