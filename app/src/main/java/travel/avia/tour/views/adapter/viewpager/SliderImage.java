package travel.avia.tour.views.adapter.viewpager;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import travel.avia.tour.views.fragment.tour.ImageSliderFragment;

public class SliderImage extends FragmentStatePagerAdapter {
    int mNumOfTabs;
    String mImageLink[];

    public SliderImage(FragmentManager fm, int NumOfTabs, String imageLink[]) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
        this.mImageLink = imageLink;
    }

    @Override
    public Fragment getItem(int position) {
        ImageSliderFragment tab = new ImageSliderFragment();
        tab.setImageLink(mImageLink[position]);
        return tab;

    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}