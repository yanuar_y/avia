package travel.avia.tour.views.adapter.recyclerview;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import travel.avia.tour.R;
import travel.avia.tour.controls.utils.Formatter;
import travel.avia.tour.database.Booking;
import travel.avia.tour.views.activity.tour.TourReservedActivity;

/**
 * Created by fitrahramadhan on 2/21/16.
 */
public class ListBookingAdapter extends RecyclerView.Adapter<ListBookingAdapter.ViewHolder> {
    private Context context;
    private ArrayList<Booking> daftarBooking = new ArrayList<Booking>();
    private int lastPosition = -1;


    public ListBookingAdapter(Context context, ArrayList<Booking> daftarBooking) {
        super();
        this.context = context;
        this.daftarBooking = daftarBooking;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        this.context = parent.getContext();
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_list_booking, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        setAnimation(holder.container, position);
        Booking booking = daftarBooking.get(position);
        JSONObject book = booking.getData();
        try {
            book = book.getJSONObject("data");
            JSONObject dummy = book.getJSONObject("summary");
            Formatter formatter = new Formatter();
            holder.booking_title.setText(dummy.getString("booking_code") + " - " + dummy.getString("tour_name"));
            //holder.booking_order_date.setText(booking.getDate());
            holder.booking_departute_date.setText(formatter.TanggalFormat(dummy.getString("departure_date")));
            holder.booking_arrival_date.setText(formatter.TanggalFormat(dummy.getString("return_date")));
            holder.booking_adult.setText(dummy.getString("adult"));
            holder.booking_child.setText(dummy.getString("child"));
            holder.booking_infant.setText(dummy.getString("infant"));
        }
        catch (JSONException je) {}
        holder.container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Booking booking = daftarBooking.get(position);
                JSONObject book = booking.getData();
                try {
                    book = book.getJSONObject("data");
                }
                catch (JSONException je) {}
                Intent i = new Intent(context, TourReservedActivity.class);
                TourReservedActivity.dataResponse = book;
                context.startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return daftarBooking.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView booking_title, booking_order_date, booking_departute_date, booking_arrival_date,
                booking_adult, booking_child, booking_infant;
        LinearLayout container;
        public ViewHolder(View v) {
            super(v);
            container = (LinearLayout) v.findViewById(R.id.booking_container);
            booking_title = (TextView) v.findViewById(R.id.booking_title);
            //booking_order_date = (TextView) v.findViewById(R.id.booking_order_date);
            booking_departute_date = (TextView) v.findViewById(R.id.booking_departure_date);
            booking_arrival_date = (TextView) v.findViewById(R.id.booking_arrival_date);
            booking_adult = (TextView) v.findViewById(R.id.booking_adult);
            booking_child = (TextView) v.findViewById(R.id.booking_child);
            booking_infant = (TextView) v.findViewById(R.id.booking_infant);
        }
    }

    private void setAnimation(View viewToAnimate, int position) {
        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(context, android.R.anim.slide_in_left);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }
}
