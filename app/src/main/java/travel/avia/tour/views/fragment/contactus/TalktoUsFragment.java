package travel.avia.tour.views.fragment.contactus;

import android.app.Fragment;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import travel.avia.tour.R;
import travel.avia.tour.views.adapter.recyclerview.ListTalkAdapter;

/**
 * Created by fitrahramadhan on 2/7/16.
 */
public class TalktoUsFragment extends Fragment {
    ListView listTalk;

    String[] namaCabang = {
            "AVIA HEAD OFFICE",
            "AVIA EMPORIUM PLUIT MAL",
            "AVIA PANGLIMA POLIM",
            "AVIA SUMMARECON MAL SERPONG",
            "AVIA MAL KELAPA GADING",
            "AVIA SUMMARECON MAL BEKASI",
            "AVIA GANDARIA CITY",
            "AVIA THEPLAZA",
            "AVIA CIBUBUR",
            "AVIA CENTRAL PARK MALL"
    };

    String[] alamatCabang = {
            "Monday-Friday (08.30-16.30)\n" +
                    "Saturday (08.30-13.30)",
            "Monday-Friday (09.00-21.00) \n" +
                    "Saturday-Sunday (09.30-21.30)",
            "Monday-Friday (08.30-16.30)\n" +
                    "Saturday (08.30-13.30)",
            "Monday-Friday (09.00-21.00) \n" +
                    "Saturday-Sunday (10.00-21.00)",
            "Monday-Friday (09.30-22.00) \n" +
                    "Saturday-Sunday (10.00-22.00)",
            "Monday-Friday (09.00-22.00) \n" +
                    "Saturday-Sunday (10.00-22.00)",
            "Monday-Friday (09.00-21.00) \n" +
                    "Saturday-Sunday (10.00-21.00)",
            "Monday-Friday (08.30-17.30) \n" +
                    "Saturday (08.30-13.30)",
            "Monday-Friday (09.00-17.00) \n" +
                    "Saturday (09.00-14.00)",
            "Monday-Saturday (09.00-22.00) \n" +
                    "Sunday (10.00-22.00)"
    };

    String[] nomorHpCabang = {
            "(+62-21) 422 3838",
            "(+62-21) 666 76684",
            "(+62-21) 7279 0989",
            "(+62-21) 2931 0386",
            "(+62-21) 452 9528",
            "(+62-21) 2957 2397",
            "(+62-21) 2900 8066",
            "(+62 21) 2992 0068",
            "(+62-21) 8430 2493",
            "(+62-21) 2970 2888"
    };

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_talk, container, false);

        getActivity().setTitle("Talk to Us");

        listTalk = (ListView) v.findViewById(R.id.list_talk);

        ListTalkAdapter lta = new ListTalkAdapter(getActivity(), namaCabang, alamatCabang, nomorHpCabang);
        listTalk.setAdapter(lta);
        listTalk.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + nomorHpCabang[position]));
                startActivity(intent);
            }
        });

        return v;
    }
}
