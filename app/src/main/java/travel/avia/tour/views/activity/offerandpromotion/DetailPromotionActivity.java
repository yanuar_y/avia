package travel.avia.tour.views.activity.offerandpromotion;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import travel.avia.tour.BuildConfig;
import travel.avia.tour.R;
import travel.avia.tour.controls.utils.Helper;
import travel.avia.tour.views.activity.tour.TourDetailActivity;
import travel.avia.tour.views.fragment.HomeFragment;
import uk.co.senab.photoview.PhotoView;
import uk.co.senab.photoview.PhotoViewAttacher;

/**
 * Created by fitrahramadhan on 2/15/16.
 */

public class DetailPromotionActivity extends AppCompatActivity {

    private static final String TAG = "DetailPromotion";

    public static String id, title, valid,image, id_product;
    public static int category;

    private TextView textValid, textApplicableOn, textTitle;
    private ImageView imagePromo;
    private WebView webViewContent;
    private Button button;
    private CollapsingToolbarLayout collapsingToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_promotion_detail);

        if (valid.equals("")) finish();

        Log.d("DEBUG_"+TAG,"Initial and set toolbar");
        final Toolbar toolbar = (Toolbar) findViewById(R.id.promoion_detail_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Log.d("DEBUG_"+TAG,"Initial collapsing toolbar");
        collapsingToolbar  = (CollapsingToolbarLayout) findViewById(R.id.promoion_detail_collapsing_toolbar);
        collapsingToolbar.setTitle("Offers and Promotions");

        SharedPreferences sharedpreferences = getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putInt("numPromo", 0);
        editor.commit();
        HomeFragment.mainActivity.changeTitlePromoMenu();

        webViewContent = (WebView) findViewById(R.id.promotion_detail_content);
        button = (Button) findViewById(R.id.promotion_detail_show);
        textTitle = (TextView) findViewById(R.id.promotion_detail_title);
        textApplicableOn = (TextView) findViewById(R.id.promotion_detail_applicable_on);
        textValid = (TextView) findViewById(R.id.promotion_detail_offer_validity);
        imagePromo = (ImageView) findViewById(R.id.promotion_detail_image);

        getSupportActionBar().setTitle(title);
        textTitle.setText(title);

        Picasso.with(this).load(image).fit().into(imagePromo);
        // set category text
        switch (category) {
            case 0 :
                textApplicableOn.setText("General");
                button.setVisibility(View.GONE);
                break;
            case 1 : textApplicableOn.setText("Tour");
                break;
            case 2 : textApplicableOn.setText("Pacakage");
                break;
            case 3 : textApplicableOn.setText("Passport and Visa");
                break;
            case 4 : textApplicableOn.setText("Insurance");
                break;
        }

        textValid.setText(valid);

        webViewContent.setFocusable(true);
        webViewContent.setFocusableInTouchMode(true);
        webViewContent.requestFocus(View.FOCUS_DOWN | View.FOCUS_UP);
        webViewContent.getSettings().setJavaScriptEnabled(true);
        webViewContent.getSettings().setDomStorageEnabled(true);
        webViewContent.getSettings().setAppCacheEnabled(true);
        webViewContent.setWebViewClient(new WebViewClient());
        webViewContent.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        webViewContent.setWebChromeClient(new WebChromeClient());
        Log.d("DEBUG_"+TAG, "Link content : "+BuildConfig.URL_PROMO+"show_content_promo_apps/"+id);
        webViewContent.loadUrl(BuildConfig.URL_PROMO+"show_content_promo_apps/"+id);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (category == 1) {
                    Log.d("DEBUG_"+TAG, "Show tour : "+id_product);
                    TourDetailActivity.tourId = id_product;
                    TourDetailActivity.deskripsi = "";
                    Intent i = new Intent(DetailPromotionActivity.this, TourDetailActivity.class);
                    startActivity(i);
                }
                else if (category == 2) {
                    //Package
                }
                else if (category == 3) {
                    //PassportAndVisa
                }
                else if (category == 4) {
                    //insurance
                }
            }
        });

        imagePromo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Context context = v.getContext();
                Helper.zoomImage(context, image);
            }
        });
    }

    @Override
    protected void onResume() {
        if (valid.equals("") || textValid.getText().toString().equals("")) finish();
        super.onResume();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
