package travel.avia.tour.views.fragment.tour;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import travel.avia.tour.R;

/**
 * Created by fitrahramadhan on 4/9/16.
 */
public class ImageSliderFragment extends Fragment {
    private String imageLink;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fargment_image_slider, container, false);
        ImageView imageView = (ImageView) v.findViewById(R.id.image_slider);
        Picasso.with(getActivity()).load(imageLink).error(R.drawable
                .aviatour).into(imageView);
        return v;
    }

    public void setImageLink(String image) {
        imageLink = image;
    }
}
