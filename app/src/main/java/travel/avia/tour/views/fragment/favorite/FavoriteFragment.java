package travel.avia.tour.views.fragment.favorite;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;

import travel.avia.tour.R;
import travel.avia.tour.views.custom.recyclerview.ItemClickSupport;
import travel.avia.tour.controls.utils.Formatter;
import travel.avia.tour.database.DBDataSource;
import travel.avia.tour.database.Tour;
import travel.avia.tour.views.activity.tour.TourDetailActivity;
import travel.avia.tour.views.adapter.recyclerview.ListTourAdapter;

public class FavoriteFragment extends Fragment {

    private String[] idTour, descriptionTour;

    private RecyclerView list_tour;
    private ProgressBar progressBar;
    private LinearLayout dataNotFound, noConnection;
    private TextView textDataNotFound;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_list_tour, container, false);

        getActivity().setTitle("My Favorite");

        list_tour = (RecyclerView) v.findViewById(R.id.list_tour_region);
        progressBar = (ProgressBar) v.findViewById(R.id.list_tour_progress);
        list_tour.setLayoutManager(new LinearLayoutManager(getActivity()));
        dataNotFound = (LinearLayout) v.findViewById(R.id.list_tour_not_found);
        noConnection = (LinearLayout) v.findViewById(R.id.list_tour_no_connection);
        textDataNotFound = (TextView) v.findViewById(R.id.list_tour_not_found_ket);
        textDataNotFound.setText("No Favorites Found\nAll your favorite Tour will appear here");

        getTour();

        return v;
    }

    public void getTour() {
        list_tour.setVisibility(View.INVISIBLE);
        noConnection.setVisibility(View.INVISIBLE);
        dataNotFound.setVisibility(View.INVISIBLE);
        progressBar.setVisibility(View.VISIBLE);
        DBDataSource dds = new DBDataSource(getActivity());
        dds.open();
        ArrayList<Tour> listTour = dds.getAllTour();
        int numTour = listTour.size();
        if (numTour == 0) {
            dataNotFound.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.INVISIBLE);
        }
        else {
            String[] imageTour = new String[numTour];
            String[] titleTour = new String[numTour];
            String[] priceTour = new String[numTour];
            idTour = new String[numTour];
            descriptionTour = new String[numTour];
            Formatter harga = new Formatter();
            Tour tour;
            for (int i = 0; i < numTour; i++) {
                tour = listTour.get(i);
                imageTour[i] = tour.getTourImage();
                titleTour[i] = tour.getTourName();
                priceTour[i] = tour.getTourPrice();
                idTour[i] = tour.getTourId();
                descriptionTour[i] = tour.getTourDescription();
            }
            ListTourAdapter adapterListNewest = new ListTourAdapter(getActivity(), imageTour, titleTour, priceTour);
            list_tour.setAdapter(adapterListNewest);
            list_tour.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.INVISIBLE);
            ItemClickSupport.addTo(list_tour).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
                @Override
                public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                    TourDetailActivity.tourId = idTour[position];
                    TourDetailActivity.deskripsi = descriptionTour[position];
                    Intent i = new Intent(getActivity(), TourDetailActivity.class);
                    startActivity(i);
                }
            });
        }
        dds.close();
    }
}
