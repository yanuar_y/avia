package travel.avia.tour.views.fragment.tour;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import travel.avia.tour.BuildConfig;
import travel.avia.tour.R;
import travel.avia.tour.controls.network.InternetConnection;
import travel.avia.tour.views.custom.recyclerview.ItemClickSupport;
import travel.avia.tour.controls.utils.Formatter;
import travel.avia.tour.views.activity.tour.RegionActivity;
import travel.avia.tour.views.activity.tour.TourDetailActivity;
import travel.avia.tour.views.adapter.recyclerview.ListTourAdapter;

/**
 * Created by fitrahramadhan on 2/20/16.
 */
public class ListTourFragment extends Fragment {

    private static final String TAG = "ListTourFragment";

    private String regionId;

    private RecyclerView list_tour;
    private ProgressBar progressBar;
    private LinearLayout dataNotFound, noConnection;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_list_tour, container, false);

        Log.d("DEBUG_"+TAG, "Initial view");
        list_tour = (RecyclerView) v.findViewById(R.id.list_tour_region);
        progressBar = (ProgressBar) v.findViewById(R.id.list_tour_progress);
        dataNotFound = (LinearLayout) v.findViewById(R.id.list_tour_not_found);
        noConnection = (LinearLayout) v.findViewById(R.id.list_tour_no_connection);
        list_tour.setLayoutManager(new LinearLayoutManager(getActivity()));

        getTour();

        return v;
    }

    public void getTour() {
        if (InternetConnection.isAvailable(getActivity())) {
            Log.d("DEBUG_"+TAG, "get tour");
            list_tour.setVisibility(View.INVISIBLE);
            dataNotFound.setVisibility(View.INVISIBLE);
            progressBar.setVisibility(View.VISIBLE);
            noConnection.setVisibility(View.INVISIBLE);
            String url = BuildConfig.URL_BASE + "/" +
                    "?method=list_tour_product" +
                    "&region_id=" + regionId + "" +
                    "&sortTour=" + (RegionActivity.sort + 1) + "" +
                    "&key=" + BuildConfig.KEY;
            JsonObjectRequest jar = new JsonObjectRequest(Request.Method.GET, url, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.d("DEBUG_"+TAG, "get tour berhasil");
                    progressBar.setVisibility(View.INVISIBLE);
                    try {
                        JSONArray listTour = response.getJSONArray("data");
                        int numTour = listTour.length();
                        if (numTour == 0) {
                            dataNotFound.setVisibility(View.VISIBLE);
                        } else {
                            String[] imageTour = new String[numTour];
                            String[] titleTour = new String[numTour];
                            String[] priceTour = new String[numTour];
                            final String[] idTour = new String[numTour];
                            final String[] descriptionTour = new String[numTour];
                            JSONObject tour;
                            Formatter harga = new Formatter();
                            for (int i = 0; i < numTour; i++) {
                                tour = listTour.getJSONObject(i);
                                imageTour[i] = BuildConfig.URL_IMAGE + tour.getString("image_name");
                                titleTour[i] = tour.getString("tour_master_name");
                                priceTour[i] = "Start form " + harga.RupiahFormat(tour.getString("price_in_idr"));
                                descriptionTour[i] = tour.getString("tour_master_description");
                                idTour[i] = tour.getString("tour_product_id");
                            }
                            ListTourAdapter adapterListNewest = new ListTourAdapter(getActivity(), imageTour, titleTour, priceTour);
                            list_tour.setAdapter(adapterListNewest);
                            list_tour.setVisibility(View.VISIBLE);
                            ItemClickSupport.addTo(list_tour).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
                                @Override
                                public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                                    TourDetailActivity.tourId = idTour[position];
                                    TourDetailActivity.deskripsi = descriptionTour[position];
                                    Intent i = new Intent(getActivity(), TourDetailActivity.class);
                                    startActivity(i);
                                }
                            });
                        }
                    } catch (JSONException je) {
                        Log.d("DEBUG_"+TAG, "JSON Error : "+je.toString());
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.d("DEBUG_" + TAG, "Error : " + error.toString());
                    list_tour.setVisibility(View.INVISIBLE);
                    dataNotFound.setVisibility(View.INVISIBLE);
                    progressBar.setVisibility(View.INVISIBLE);
                    noConnection.setVisibility(View.VISIBLE);
                }
            });
            jar.setRetryPolicy(new DefaultRetryPolicy(
                    15000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            try {
                Volley.newRequestQueue(getActivity()).add(jar);
            }
            catch (Exception npe) {
                Log.d("DEBUG_"+TAG, "Error : "+npe.toString());
            }
        }
        else {
            Log.d("DEBUG_"+TAG, "No internet");
            list_tour.setVisibility(View.INVISIBLE);
            dataNotFound.setVisibility(View.INVISIBLE);
            progressBar.setVisibility(View.INVISIBLE);
            noConnection.setVisibility(View.VISIBLE);
        }
    }

    public void setRegionId(String regionId) {
        this.regionId = regionId;
    }
}
