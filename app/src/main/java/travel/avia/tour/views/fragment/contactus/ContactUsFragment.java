package travel.avia.tour.views.fragment.contactus;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import travel.avia.tour.R;
import travel.avia.tour.views.adapter.recyclerview.ListContactUsAdapter;
import travel.avia.tour.views.fragment.HomeFragment;


public class ContactUsFragment extends Fragment {
    private String[] titleContact = {
            "Talk to Us",
            "Write to Us",
            "Chat with Us",
            "FAQs"
    };
    private Integer[] iconContact = {
            R.mipmap.ic_call,
            R.mipmap.ic_write,
            R.mipmap.ic_chat,
            R.mipmap.ic_faq
    };

    ListView listContact;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_contactus, container, false);

        getActivity().setTitle("Contact Us");

        listContact = (ListView) v.findViewById(R.id.contact_list);
        ListContactUsAdapter lcua = new ListContactUsAdapter(getActivity(), iconContact, titleContact);
        listContact.setAdapter(lcua);
        listContact.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent i;
                if (position==0) {
                    HomeFragment.mainActivity.changeFragment(new TalktoUsFragment(), "TalktoUsFragment");
                }
                else if (position==1) {
                    HomeFragment.mainActivity.changeFragment(new WriteToUsFragment(), "WritetoUsFragment");
                }
                else if (position==2) {
                    HomeFragment.mainActivity.changeFragment(new LiveChatFragment(), "LiveChatFragment");
                }
                else {
                    HomeFragment.mainActivity.changeFragment(new FaqFragment(), "FaqFragment");
                }
            }
        });
        
        return v;
    }
}
