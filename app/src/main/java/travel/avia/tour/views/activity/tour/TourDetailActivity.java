package travel.avia.tour.views.activity.tour;

import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

import travel.avia.tour.BuildConfig;
import travel.avia.tour.R;
import travel.avia.tour.controls.utils.CustomMapView;
import travel.avia.tour.controls.utils.Formatter;
import travel.avia.tour.database.DBDataSource;
import travel.avia.tour.views.adapter.viewpager.SliderImage;
import travel.avia.tour.views.dialog.TourOrderDialogFragment;

/**
 * Created by fitrahramadhan on 2/15/16.
 */
public class TourDetailActivity extends AppCompatActivity implements
        View.OnClickListener,
        ViewPager.OnPageChangeListener{

    private static final String TAG = "TourDetailAct";

    public static String  tourId, deskripsi, harga, image, nameTour;
    public static boolean jenisProduk[]=new boolean[5];
    public static String  hargaProduk[] = new String[7], departure, arrival, day, flight, judul,
            city[], cityId[], roomId[] = new String[6], visaCountryId[], visaCountryName[], visaNameAdult[][], visaNameChild[][],
            visaIdAdult[][], visaIdChild[][], visaPriceAdult[][], visaPriceChild[][];
    public static int visaNumAdult[], visaNumChild[];

    private int numAvailable, numAvailble, numItinerary;
    private String availId[];
    private DBDataSource dds;
    private MenuItem favorite;
    private int currentPage;
    private ImageButton fab_share;
    private Button fab_book;
    private int visa_id_length;
    private TextView description, title, date, days, airline, price, priceIDR[]=new TextView[7],
            titleTerm, titleInclude, titleExclude, titleItenarary, slideIndicator, titleVisa, noVisa;
    private WebView  include, exclude, term;
    private GoogleMap googleMap;
    private LinearLayout layoutTerm, layoutInclude, layoutExclude, layoutItinerary,
            hargaKategori[] = new LinearLayout[7], itenerary, available, layoutVisa,
            layoutVisaParent, availableDate[], availableLayout;
    private CustomMapView map;
    private ViewPager sliderImage;
    private ProgressDialog pd;
    private int indicator = 1;
    private NestedScrollView scrollView;
    private boolean maxInclude=false, maxExclude=false, maxTerm=false, maxItinerary=false, maxVisa=false;
    private CollapsingToolbarLayout collapsingToolbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("DEBUG_"+TAG,"Set content view");
        setContentView(R.layout.activity_tour_detail);

        Log.d("DEBUG_"+TAG,"Initial and set toolbar");
        final Toolbar toolbar = (Toolbar) findViewById(R.id.main_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Log.d("DEBUG_"+TAG,"Initial collapsing toolbar");
        collapsingToolbar  = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);

        Log.d("DEBUG_"+TAG, "Initial view");
        scrollView = (NestedScrollView) findViewById(R.id.promotion_detail_scroll);
        fab_share = (ImageButton) findViewById(R.id.tour_detail_fab_share);
        fab_book = (Button) findViewById(R.id.fab_book);
        title = (TextView) findViewById(R.id.tour_detail_title);
        description = (TextView) findViewById(R.id.tour_detail_description);
        include = (WebView) findViewById(R.id.tour_detail_include);
        exclude = (WebView) findViewById(R.id.tour_detail_exclude);
        term = (WebView) findViewById(R.id.tour_detail_term);
        date = (TextView) findViewById(R.id.tour_detail_date);
        days = (TextView) findViewById(R.id.tour_detail_tour_days);
        airline = (TextView) findViewById(R.id.tour_detail_tour_airline);
        price = (TextView) findViewById(R.id.tour_detail_tour_price);
        priceIDR[0] = (TextView) findViewById(R.id.tour_detail_adult_twin_share_idr);
        priceIDR[1] = (TextView) findViewById(R.id.tour_detail_child_twin_share_idr);
        priceIDR[2] = (TextView) findViewById(R.id.tour_detail_child_with_extra_bed_idr);
        priceIDR[3] = (TextView) findViewById(R.id.tour_detail_child_without_bed_idr);
        priceIDR[4] = (TextView) findViewById(R.id.tour_detail_single_suplement_idr);
        priceIDR[5] = (TextView) findViewById(R.id.tour_detail_international_airlane_tax_idr);
        priceIDR[6] = (TextView) findViewById(R.id.tour_detail_infant_idr);
        noVisa = (TextView) findViewById(R.id.no_visa);
        //favorite = (FloatingActionButton) findViewById(R.id.favorite);
        hargaKategori[0] = (LinearLayout) findViewById(R.id.tour_detail_adult_twin_share);
        hargaKategori[1] = (LinearLayout) findViewById(R.id.tour_detail_child_twin_share);
        hargaKategori[2] = (LinearLayout) findViewById(R.id.tour_detail_child_with_extra_bed);
        hargaKategori[3] = (LinearLayout) findViewById(R.id.tour_detail_child_without_bed);
        hargaKategori[4] = (LinearLayout) findViewById(R.id.tour_detail_single_suplement);
        hargaKategori[5] = (LinearLayout) findViewById(R.id.tour_detail_international_airlane_tax);
        hargaKategori[6] = (LinearLayout) findViewById(R.id.tour_detail_infant);
        available = (LinearLayout) findViewById(R.id.tour_detail_booking_available);
        itenerary = (LinearLayout) findViewById(R.id.tour_detail_itinerary);
        layoutExclude = (LinearLayout) findViewById(R.id.layout_price_exclude);
        layoutInclude = (LinearLayout) findViewById(R.id.layout_price_include);
        layoutTerm = (LinearLayout) findViewById(R.id.layout_term);
        layoutItinerary = (LinearLayout) findViewById(R.id.layoutItinearary);
        layoutVisa = (LinearLayout) findViewById(R.id.tour_book_layout_visa);
        layoutVisaParent = (LinearLayout) findViewById(R.id.layout_visa_parent);
        titleItenarary = (TextView) findViewById(R.id.title_itinerary);
        titleInclude = (TextView) findViewById(R.id.title_include);
        titleExclude = (TextView) findViewById(R.id.title_exclude);
        titleTerm = (TextView) findViewById(R.id.title_term);
        titleVisa = (TextView) findViewById(R.id.book_tour_title_visa);
        final float scale = getResources().getDisplayMetrics().density;
        map = (CustomMapView) findViewById(R.id.tour_detail_maps);
        sliderImage = (ViewPager) findViewById(R.id.tour_detail_slide_show);
        sliderImage.setOnPageChangeListener(this);
        slideIndicator = (TextView) findViewById(R.id.tour_detail_slide_indicator);
        availableLayout = (LinearLayout) findViewById(R.id.tour_detail_layout_available);

        Log.d("DEBUG_"+TAG,"Initial maps");
        map.onCreate(savedInstanceState);
        map.onResume();
        MapsInitializer.initialize(TourDetailActivity.this.getApplicationContext());
        googleMap = map.getMap();

        Log.d("DEBUG_"+TAG,"Show / hide layout");
        final int pixels2 = (int) (200 * scale + 0.5f);
        final int margin10 = (int) (10 * scale + 0.5f);
        final int margin15 = (int) (15 * scale + 0.5f);
        titleItenarary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("DEBUG_"+TAG,"Show / hide itenarary");
                if (maxItinerary) {
                    maxItinerary = false;
                    layoutItinerary.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, pixels2));
                    titleItenarary.setCompoundDrawablesWithIntrinsicBounds(R.mipmap.ic_itinerary, 0, R.mipmap.ic_arrow_down, 0);
                } else {
                    maxItinerary = true;
                    layoutItinerary.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams
                            .MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                    titleItenarary.setCompoundDrawablesWithIntrinsicBounds(R.mipmap.ic_itinerary, 0, R.mipmap.ic_arrow_up, 0);
                }
            }
        });
        titleVisa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("DEBUG_"+TAG,"show / hide visa");
                if (maxVisa) {
                    maxVisa = false;
                    noVisa.setVisibility(View.GONE);
                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout
                            .LayoutParams.MATCH_PARENT, 0);
                    params.setMargins(margin15,margin10,margin15,margin10);
                    layoutVisa.setLayoutParams(params);
                    titleVisa.setCompoundDrawablesWithIntrinsicBounds(R.mipmap.ic_visa, 0,
                            R.mipmap.ic_arrow_down, 0);
                } else {
                    if (visa_id_length == 0) noVisa.setVisibility(View.VISIBLE);
                    else noVisa.setVisibility(View.GONE);
                    maxVisa = true;
                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout
                            .LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                    params.setMargins(margin15,margin10,margin15,margin10);
                    layoutVisa.setLayoutParams(params);
                    titleVisa.setCompoundDrawablesWithIntrinsicBounds(R.mipmap.ic_visa, 0,
                            R.mipmap.ic_arrow_up, 0);
                }
            }
        });
        titleInclude.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("DEBUG_"+TAG,"show / hide include");
                if (maxInclude) {
                    maxInclude = false;
                    layoutInclude.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 0));
                    titleInclude.setCompoundDrawablesWithIntrinsicBounds(R.mipmap.ic_price_include, 0,
                            R.mipmap.ic_arrow_down, 0);
                }
                else {
                    maxInclude = true;
                    layoutInclude.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams
                            .MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                    titleInclude.setCompoundDrawablesWithIntrinsicBounds(R.mipmap.ic_price_include, 0,
                            R.mipmap.ic_arrow_up, 0);
                }
            }
        });
        titleExclude.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("DEBUG_"+TAG,"show / hide exclude");
                if (maxExclude) {
                    maxExclude = false;
                    layoutExclude.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 0));
                    titleExclude.setCompoundDrawablesWithIntrinsicBounds(R.mipmap.ic_price_exclude, 0,
                            R.mipmap.ic_arrow_down, 0);
                } else {
                    maxExclude = true;
                    layoutExclude.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams
                            .MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                    titleExclude.setCompoundDrawablesWithIntrinsicBounds(R.mipmap.ic_price_exclude, 0,
                            R.mipmap.ic_arrow_up, 0);
                }
            }
        });
        titleTerm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("DEBUG_"+TAG,"show / hide term");
                if (maxTerm) {
                    maxTerm = false;
                    layoutTerm.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams
                            .MATCH_PARENT, 0));
                    titleTerm.setCompoundDrawablesWithIntrinsicBounds(R.mipmap.ic_term, 0, R.mipmap.ic_arrow_down, 0);
                }
                else {
                    maxTerm = true;
                    layoutTerm.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams
                            .MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                    titleTerm.setCompoundDrawablesWithIntrinsicBounds(R.mipmap.ic_term, 0, R.mipmap.ic_arrow_up, 0);
                }
            }
        });

        Log.d("DEBUG_"+TAG,"Set share on click");
        fab_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, "Check this out http://www.avia.travel/tour/detail/"
                        +tourId+"/"+nameTour.replace(" ", "_").replace("'", "").replace("+", "")
                        .replace("&", "").replace("(", "").replace(")", "").replace(",", "").toLowerCase());
                sendIntent.setType("text/plain");
                startActivity(sendIntent);
            }
        });

        Log.d("DEBUG_"+TAG,"Set book on click");
        fab_book.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TourOrderDialogFragment newFragment = TourOrderDialogFragment.newInstance();
                newFragment.setStyle(DialogFragment.STYLE_NO_TITLE, 0);
                newFragment.show(getFragmentManager(), "dialog");
            }
        });

        pd = ProgressDialog.show(TourDetailActivity.this, "", "Loading...", true);
        getDetail();
    }
    
    public void onResume() {
        Log.d("DEBUG_"+TAG,"on resume");
        super.onResume();
        map.onResume();
    }

    @Override
    public void onPause() {
        Log.d("DEBUG_"+TAG,"on pause");
        super.onPause();
        map.onPause();
        pd.dismiss();
    }
    @Override
    public void onDestroy() {
        Log.d("DEBUG_"+TAG,"on destroy");
        super.onDestroy();
        map.onDestroy();
        pd.dismiss();
    }
    @Override
    public void onLowMemory() {
        Log.d("DEBUG_"+TAG,"on low memory");
        super.onLowMemory();
        map.onLowMemory();
        pd.dismiss();
    }


    private void getDetail() {
        Log.d("DEBUG_"+TAG, "Get Detail");
        String url= BuildConfig.URL_BASE+"/?method=detail_tour_product&tour_product_id="+tourId+"&key="+BuildConfig.KEY;
        JsonObjectRequest jar = new JsonObjectRequest(Request.Method.GET,url,new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject hasil) {
                try {
                    Formatter formatter = new Formatter();
                    Log.d("DEBUG_"+TAG, "Data");
                    JSONObject response = hasil.getJSONObject("data");

                    Log.d("DEBUG_"+TAG, "VISA");
                    JSONArray visa_id = response.getJSONArray("visa_id");
                    visa_id_length = visa_id.length();
                    if (visa_id_length > 0) {
                        noVisa.setVisibility(View.GONE);
                        //Ambil Id Negara
                        visaCountryId = new String[visa_id_length];
                        visaCountryName = new String[visa_id_length];

                        for (int i = 0; i < visa_id_length; i++) {
                            visaCountryId[i] = visa_id.getString(i);
                        }
                        //Ambil Visa
                        Log.d("DEBUG_"+TAG, "Visa Adult");
                        visaNumAdult = new int[visa_id_length];
                        visaNameAdult = new String[visa_id_length][20];
                        visaIdAdult = new String[visa_id_length][20];
                        visaPriceAdult = new String[visa_id_length][20];
                        JSONObject visa;
                        for (int i = 0; i < visa_id_length; i++) {
                            visa = response.getJSONObject("visa_adult");
                            visa = visa.getJSONObject(visaCountryId[i]);
                            visaCountryName[i] = visa.getString("tour_country_name");
                            visa = visa.getJSONObject("nationality");
                            visa = visa.getJSONObject("1");
                            JSONArray listVisa = visa.getJSONArray("visa");
                            visaNumAdult[i] = listVisa.length();
                            Log.d("DEBUG_"+TAG, "Get Visa Adult");
                            for (int j=0; j<visaNumAdult[i]; j++) {
                                visa = listVisa.getJSONObject(j);
                                View layout = TourDetailActivity.this.getLayoutInflater()
                                        .inflate(R.layout.layout_tour_detail_visa, null);
                                visaNameAdult[i][j] = visa.getString("visa_name");
                                visaIdAdult[i][j] = visa.getString("visa_id");
                                visaPriceAdult[i][j] = visa.getString("value");
                                TextView titleVisa = (TextView) layout.findViewById(R.id.tour_detail_title_visa);
                                TextView countryVisa = (TextView) layout.findViewById(R.id.tour_detail_type_visa);
                                TextView priceVisa = (TextView) layout.findViewById(R.id.tour_detail_visa_price);
                                titleVisa.setText(visaNameAdult[i][j]);
                                countryVisa.setText(visaCountryName[i] + " - Adult");
                                priceVisa.setText(formatter.RupiahFormat(visaPriceAdult[i][j]));
                                layoutVisa.addView(layout);
                            }
                        }
                        visaNameChild = new String[visa_id_length][20];
                        visaIdChild = new String[visa_id_length][20];
                        visaPriceChild = new String[visa_id_length][20];
                        Log.d("DEBUG_"+TAG, "Visa Child");
                        visaNumChild = new int[visa_id_length];
                        for (int i = 0; i < visa_id_length; i++) {
                            visa = response.getJSONObject("visa_child");
                            visa = visa.getJSONObject(visaCountryId[i]);
                            visa = visa.getJSONObject("nationality");
                            visa = visa.getJSONObject("1");
                            JSONArray listVisa = visa.getJSONArray("visa");
                            visaNumChild[i] = listVisa.length();
                            Log.d("DEBUG_"+TAG, "Get Visa Child");
                            for (int j=0; j<visaNumChild[i]; j++) {
                                visa = listVisa.getJSONObject(j);
                                View layout = TourDetailActivity.this.getLayoutInflater()
                                        .inflate(R.layout.layout_tour_detail_visa, null);
                                visaNameChild[i][j] = visa.getString("visa_name");
                                visaIdChild[i][j] = visa.getString("visa_id");
                                visaPriceChild[i][j] = visa.getString("value");
                                TextView titleVisa = (TextView) layout.findViewById(R.id.tour_detail_title_visa);
                                TextView countryVisa = (TextView) layout.findViewById(R.id.tour_detail_type_visa);
                                TextView priceVisa = (TextView) layout.findViewById(R.id.tour_detail_visa_price);
                                titleVisa.setText(visaNameChild[i][j]);
                                countryVisa.setText(visaCountryName[i] + " - Child");
                                priceVisa.setText(formatter.RupiahFormat(visaPriceChild[i][j]));
                                layoutVisa.addView(layout);
                            }
                        }
                    } else {
                        noVisa.setVisibility(View.GONE);
                        visaCountryId = new String[0];
                        visaCountryName = new String[0];
                    }

                    Log.d("DEBUG_"+TAG, "Room Is");
                    JSONObject ass;
                    ass = response.getJSONObject("adult_twin_bed");
                    roomId[0] = ass.getString("pid");
                    ass = response.getJSONObject("child_with_bed");
                    roomId[1] = ass.getString("pid");
                    ass = response.getJSONObject("child_with_extra_bed");
                    roomId[2] = ass.getString("pid");
                    ass = response.getJSONObject("child_without_bed");
                    roomId[3] = ass.getString("pid");
                    ass = response.getJSONObject("single_suplement");
                    roomId[4] = ass.getString("pid");
                    ass = response.getJSONObject("infant");
                    roomId[5] = ass.getString("pid");

                    Log.d("DEBUG_"+TAG, "Available");
                    departure = formatter.DateFormat(response.getString("date_departure")
                            .substring(0, 10));
                    JSONArray avaible = response.getJSONArray("tour_avaible");
                    numAvailble = avaible.length();
                    if (numAvailble==1) {
                        availableLayout.setVisibility(View.GONE);
                    }
                    else {
                        TextView[] tanggal = new TextView[numAvailble], bulan = new TextView[numAvailble],
                                tahun = new TextView[numAvailble];
                        availableDate = new LinearLayout[numAvailble];
                        availId = new String[numAvailble];
                        String pecah[];
                        for (int i = 0; i < numAvailble; i++) {
                            JSONObject bookAvailable = avaible.getJSONObject(i);
                            View form = TourDetailActivity.this.getLayoutInflater()
                                    .inflate(R.layout.layout_tour_detail_available, null);
                            availableDate[i] = (LinearLayout) form.findViewById(R.id.available_date);
                            tanggal[i] = (TextView) form.findViewById(R.id.available_tanggal);
                            bulan[i] = (TextView) form.findViewById(R.id.available_bulan);
                            tahun[i] = (TextView) form.findViewById(R.id.available_tahun);
                            availId[i] = bookAvailable.getString("link");
                            pecah = availId[i].split("/");
                            availId[i] = pecah[pecah.length - 1];
                            String date = formatter.DateFormat(bookAvailable.getString("depature")
                                    .substring(0, 10));
                            if (tourId.equals(availId[i])) {
                                Log.d("DEBUG_"+TAG, "ganti warna abu2");
                                availableDate[i].setBackgroundColor(getResources().getColor(R.color.aviaColor));
                                tanggal[i].setTextColor(getResources().getColor(android.R.color.white));
                                bulan[i].setTextColor(getResources().getColor(android.R.color.white));
                                tahun[i].setTextColor(getResources().getColor(android.R.color.white));
                            }
                            availableDate[i].setOnClickListener(TourDetailActivity.this);
                            String tgl[] = date.split(" ");
                            tanggal[i].setText(tgl[0]);
                            bulan[i].setText(tgl[1]);
                            tahun[i].setText(tgl[2]);
                            available.addView(form);
                        }
                    }

                    Log.d("DEBUG_"+TAG, "City");
                    JSONArray country = response.getJSONArray("country");
                    int numCountry = country.length()+1;
                    city = new String[numCountry];
                    cityId = new String[numCountry];
                    city[0] = "Place of Issue";
                    cityId[0] = "";
                    for (int i=1; i<numCountry; i++) {
                        JSONObject kota = country.getJSONObject(i-1);
                        city[i] = kota.getString("tour_country_name").toUpperCase();
                        cityId[i] = kota.getString("tour_country_id");
                    }
                    nameTour = response.getString("tour_product_name");
                    //TextData
                    title.setText(nameTour);
                    collapsingToolbar.setTitle(nameTour);

                    judul = nameTour;
                    day = response.getString("tour_days");
                    days.setText(day+" days");
                    harga = "IDR " + formatter.RupiahFormat(response.getString("price"));
                    price.setText(harga);

                    arrival = formatter.DateFormat(response.getString("date_return").substring(0, 10));
                    date.setText(departure + " - " + arrival);
                    flight = response.getString("tour_airline_name") + " (" + response
                            .getString("tour_airline_code") + ")";
                    airline.setText(flight);
                    description.setText(deskripsi);
                    Log.d("DEBUG_"+TAG, "WebView");
                    term.loadData("<font size='2' color='#000000'>"+response
                            .getString("tour_itinerary_terms").replace("<li>","<li><p align='justify'>")
                            .replace("</li>", "</li></p>")+"</font>", "text/html", "UTF-8");
                    include.loadData("<font size='2' color='#000000'>"+response
                            .getString("included_information").replace("<li>","<li><p align='justify'>")
                            .replace("</li>", "</li></p>")+"</font>", "text/html", "UTF-8");
                    exclude.loadData("<font size='2' color='#000000'>"+response
                            .getString("excluded_information").replace("<li>","<li><p align='justify'>")
                            .replace("</li>", "</li></p>")+"</font>", "text/html", "UTF-8");
                    JSONArray itinerary = response.getJSONArray("itinerary");
                    numItinerary = itinerary.length();
                    HashMap<String,String> url_maps = new HashMap<String, String>();
                    JSONObject subItinerary;
                    googleMap.getUiSettings().setZoomControlsEnabled(true);
                    googleMap.clear();
                    MarkerOptions marker;
                    Double latitude=0.0, longitude=0.0;
                    String linkImage[] = new String[numItinerary];
                    sliderImage.setOffscreenPageLimit(numItinerary);
                    for (int i=0; i<numItinerary; i++) {
                        subItinerary = itinerary.getJSONObject(i);
                        //Itinerary
                        View child = TourDetailActivity.this.getLayoutInflater()
                                .inflate(R.layout.adapter_list_day, null);
                        TextView title = (TextView) child.findViewById(R.id.list_day_title);
                        TextView bld = (TextView) child.findViewById(R.id.list_day_bld);
                        WebView content = (WebView) child.findViewById(R.id.list_day_content);
                        title.setText("Day " + (i + 1));
                        String strBld="";
                        if (subItinerary.getInt("breakfast")==1) strBld+="B ";
                        if (subItinerary.getInt("lunch")==1) strBld+="L ";
                        if (subItinerary.getInt("dinner")==1) strBld+="D";
                        strBld = strBld.trim();
                        bld.setText(strBld);
                        content.loadData("<font size='2' color='#000000'>" + subItinerary
                                .getString("description").replace("<p>","<p align='justify'>")
                                + "</font>", "text/html", "UTF-8");
                        itenerary.addView(child);

                        //Slide Picture
                        if (i==0) image = BuildConfig.URL_IMAGE_PACKAGE+subItinerary
                                .getString("tour_product_detail_information_photo");
                        linkImage[i] = BuildConfig.URL_IMAGE_PACKAGE+subItinerary
                                .getString("tour_product_detail_information_photo");

                        //Maps
                        String latlong = subItinerary.getString("latlong");
                        if (!(latlong.equals(""))) {
                            String[] latilong = latlong.split(",");
                            latitude = Double.parseDouble(latilong[0]);
                            longitude = Double.parseDouble(latilong[1]);
                            marker = new MarkerOptions().position(new LatLng(latitude,
                                    longitude)).title("Day "+(i+1));
                            marker.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ROSE));
                            googleMap.addMarker(marker);
                            if (i==2) {
                                CameraPosition cameraPosition = new CameraPosition.Builder()
                                        .target(new LatLng(latitude, longitude)).zoom(4).build();
                                googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                            }
                        }
                    }
                    googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                        @Override
                        public boolean onMarkerClick(Marker marker) {
                            marker.showInfoWindow();
                            //googleMap.animateCamera(CameraUpdateFactory.newLatLng(marker.getPosition()));
                            return false;
                        }
                    });
                    SliderImage viewPagerSlider = new SliderImage(getSupportFragmentManager(),
                            numItinerary, linkImage);
                    sliderImage.setAdapter(viewPagerSlider);
                    currentPage = 0;
                    final Handler handler = new Handler();
                    final Runnable Update = new Runnable() {
                        public void run() {
                            if (currentPage == numItinerary-1) {
                                currentPage = 0;
                            }
                            sliderImage.setCurrentItem(currentPage++, true);
                        }
                    };
                    final Timer swipeTimer = new Timer();
                    swipeTimer.schedule(new TimerTask() {
                        @Override
                        public void run() {
                            handler.post(Update);
                        }
                    }, 500, 2000);

                    Log.d("DEBUG_"+TAG, "Category Price");
                    JSONArray categoryPrice = response.getJSONArray("category_price");
                    JSONObject category;
                    for (int i=0; i<7; i++) {
                        category = categoryPrice.getJSONObject(i);
                        hargaProduk[i] = category.getString("price");
                        String harga = formatter.RupiahFormat(hargaProduk[i]);
                        if (harga.startsWith("IDR 0")) {
                            if (i<4) jenisProduk[i] = false;
                            if (i==6) jenisProduk[4] = false;
                            hargaKategori[i].setVisibility(View.GONE);
                        }
                        else {
                            if (i<4) jenisProduk[i] = true;
                            if (i==6) jenisProduk[4] = true;
                            priceIDR[i].setText(harga);
                        }
                        if (deskripsi.equals("")) {
                            deskripsi = category.getString("tour_master_description");
                            description.setText(deskripsi);
                        }
                    }
                }
                catch (JSONException je) {
                    Log.d("DEBUG_"+TAG, "Error JSON : " +je.toString());
                    Toast.makeText(TourDetailActivity.this, "JSON Format : We will fix it soon.", Toast.LENGTH_SHORT).show();
                    finish();
                }
                pd.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("DEBUG_"+TAG, "Error : " +error.toString());
                pd.dismiss();
                if (error.toString().equals("com.android.volley.ServerError")) {
                    Toast.makeText(TourDetailActivity.this, "Database : Can't find tour with ID = "+tourId, Toast.LENGTH_SHORT).show();
                }
                else if (error.toString().equals("com.android.volley.TimeOutError")) {
                    Toast.makeText(TourDetailActivity.this, "Time Out : Please make sure you have good internet access.", Toast.LENGTH_SHORT).show();
                }
                finish();
            }
        });
        jar.setRetryPolicy(new DefaultRetryPolicy(
                30000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Volley.newRequestQueue(TourDetailActivity.this).add(jar);
    }

    @Override
    public void onClick(View v) {
        for (numAvailable = 0; numAvailable<numAvailble; numAvailable++) {
            if (v==availableDate[numAvailable]) {
                if (!(availId[numAvailable].equals(tourId))) {
                    this.tourId = availId[numAvailable];
                    startActivity(new Intent(TourDetailActivity.this, TourDetailActivity.class));
                    finish();
                    break;
                }
                else break;
            }
        }
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        indicator = position;
        slideIndicator.setText((indicator+1)+"/"+numItinerary);
    }

    @Override
    public void onPageSelected(int position) {
    }

    @Override
    public void onPageScrollStateChanged(int state) {
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        Log.d("DEBUG_"+TAG,"Initial and set menu");
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.menu_tour_detail, menu);
        favorite = menu.findItem(R.id.menu_favorite);
        dds = new DBDataSource(TourDetailActivity.this);
        dds.open();
        if (dds.findTour(tourId)) {
            favorite.setIcon(getResources().getDrawable(R.drawable.favorited));
        }
        else {
            favorite.setIcon(getResources().getDrawable(R.drawable.favorite));
        }
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            Log.d("DEBUG_"+TAG,"menu home clicked");
            finish();
        }
        else if (id == R.id.menu_favorite) {
            Log.d("DEBUG_"+TAG,"menu favorite cliked");
            if (dds.findTour(tourId)) {
                dds.deleteTour(tourId);
                favorite.setIcon(getResources().getDrawable(R.drawable.favorite));
            }
            else {
                dds.insertTour(tourId, judul, deskripsi, harga, image);
                favorite.setIcon(getResources().getDrawable(R.drawable.favorited));
            }
        }
        return super.onOptionsItemSelected(item);
    }
}