package travel.avia.tour.views.adapter.recyclerview;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import travel.avia.tour.R;

/**
 * Created by fitrahramadhan on 2/7/16.
 */
public class ListTalkAdapter extends ArrayAdapter<String> {
    private final Activity context;
    private final String[] namaCabang;
    private final String[] alamatCabang;
    private final String[] nomorHpCabang;

    public ListTalkAdapter(Activity context, String[] namaCabang, String[] alamatCabang, String[] nomorHpCabang) {
        super(context, R.layout.adapter_list_talk, namaCabang);
        this.context = context;
        this.namaCabang = namaCabang;
        this.alamatCabang = alamatCabang;
        this.nomorHpCabang = nomorHpCabang;
    }

    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View v = inflater.inflate(R.layout.adapter_list_talk, null, true);

        TextView nama_cabang = (TextView) v.findViewById(R.id.nama_cabang);
        TextView alamat_cabang = (TextView) v.findViewById(R.id.alamat_cabang);
        TextView nomor_hp_cabang = (TextView) v.findViewById(R.id.nomor_hp);


        nama_cabang.setText(namaCabang[position]);
        alamat_cabang.setText(alamatCabang[position]);
        nomor_hp_cabang.setText(nomorHpCabang[position]);

        return v;
    }
}
