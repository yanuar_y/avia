package travel.avia.tour.views.fragment.mybooking;

import android.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import java.util.ArrayList;

import travel.avia.tour.R;
import travel.avia.tour.database.Booking;
import travel.avia.tour.database.DBDataSource;
import travel.avia.tour.views.adapter.recyclerview.ListBookingAdapter;

public class ListMyBookingFragment extends Fragment {
    RecyclerView listBooking;
    ProgressBar progressBar;
    LinearLayout dataNotFound;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_my_booking, container, false);

        getActivity().setTitle("My Booking");

        listBooking = (RecyclerView) v.findViewById(R.id.list_booking);
        progressBar = (ProgressBar) v.findViewById(R.id.list_booking_progress);
        listBooking.setLayoutManager(new LinearLayoutManager(getActivity()));
        dataNotFound = (LinearLayout) v.findViewById(R.id.data_booking_not_found);

        getBooking();

        return v;
    }

    public void getBooking() {
        listBooking.setVisibility(View.INVISIBLE);
        dataNotFound.setVisibility(View.INVISIBLE);
        progressBar.setVisibility(View.VISIBLE);

        DBDataSource dds = new DBDataSource(getActivity());
        dds.open();
        ArrayList<Booking> listBooking = dds.getAllBooking();

        if (listBooking.size() == 0) {
            this.dataNotFound.setVisibility(View.VISIBLE);
            this.progressBar.setVisibility(View.INVISIBLE);
        }
        else {
            ListBookingAdapter adapterListNewest = new ListBookingAdapter(getActivity(), listBooking);
            this.listBooking.setAdapter(adapterListNewest);
            this.listBooking.setVisibility(View.VISIBLE);
            this.progressBar.setVisibility(View.INVISIBLE);
        }

        dds.close();
    }
}
