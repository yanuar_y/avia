package travel.avia.tour.views.fragment.offerpromotion;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import travel.avia.tour.BuildConfig;
import travel.avia.tour.R;
import travel.avia.tour.controls.utils.Formatter;
import travel.avia.tour.views.activity.offerandpromotion.DetailPromotionActivity;
import travel.avia.tour.views.adapter.recyclerview.ListPromoAdapter;
import travel.avia.tour.views.custom.recyclerview.ItemClickSupport;
import travel.avia.tour.views.custom.recyclerview.SpaceItemDecoration;
import travel.avia.tour.views.fragment.HomeFragment;

public class OfferPromotionFragment extends Fragment {

    private static final String TAG = "DEBUG_OfferPromotion";

    RecyclerView list;
    ProgressDialog pd;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_offers_promo, container, false);
        Log.d(TAG, "On create view");
        getActivity().setTitle("Offers and Promotions");

        HomeFragment.mainActivity.changeTitlePromoMenu();

        list = (RecyclerView) v.findViewById(R.id.list);
        list.addItemDecoration(new SpaceItemDecoration(getResources().getDimensionPixelSize(R.dimen.divider)));

        pd = ProgressDialog.show(getActivity(), "", "Loading...", true);
        Log.d(TAG, "Get data");
        String url= BuildConfig.URL_PROMO+"get_list_promo_apps";
        Log.d(TAG, "URL Request : "+url);
        JsonArrayRequest jar = new JsonArrayRequest(Request.Method.GET,url,new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray hasil) {
                int numHasil = hasil.length();
                if (numHasil > 0) {
                    Log.d(TAG, "Ada promo");
                    pd.dismiss();
                    final String[]
                            listId = new String[numHasil],
                            listTitle = new String[numHasil],
                            listImage = new String[numHasil],
                            listValid = new String[numHasil],
                            listIdProduct = new String[numHasil];
                    final int[] listCategory = new int[numHasil];
                    list.setLayoutManager(new LinearLayoutManager(getActivity()));
                    JSONObject jo;
                    Log.d(TAG, "Parsing json");
                    for (int i = 0; i < numHasil; i++) {
                        Formatter formatter = new Formatter();
                        try {
                            jo = hasil.getJSONObject(i);
                            listId[i] = jo.getString("id");
                            listTitle[i] = jo.getString("title");
//                            listImage[i] = BuildConfig.URL_IMAGE_PROMO + jo.getString("image"); // old one
                            listImage[i] = jo.getString("image_location");
                            listCategory[i] = jo.getInt("category");
                            listIdProduct[i] = jo.getString("id_product");
                            listValid[i] = formatter.TanggalFormat(jo.getString("start_date")) + " to " + formatter.TanggalFormat(jo.getString("end_date"));
                        } catch (JSONException je) {
                            pd.dismiss();
                            Toast.makeText(getActivity(), je.toString(), Toast.LENGTH_SHORT).show();
                        }
                    }
                    Log.d(TAG, "set adapter");
                    ListPromoAdapter adapter = new ListPromoAdapter(getActivity(), listTitle, listImage, listValid);
                    list.setAdapter(adapter);
                    ItemClickSupport.addTo(list).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
                        @Override
                        public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                            DetailPromotionActivity.id = listId[position];
                            DetailPromotionActivity.title = listTitle[position];
                            DetailPromotionActivity.image = listImage[position];
                            DetailPromotionActivity.category = listCategory[position];
                            DetailPromotionActivity.valid = listValid[position];
                            DetailPromotionActivity.id_product = listIdProduct[position];
                            startActivity(new Intent(getActivity(), DetailPromotionActivity.class));
                        }
                    });
                }
                else {
                    Log.d(TAG, "Tidak ada promo");
                    pd.dismiss();
                    Toast.makeText(getActivity(), "Tidak ada promo", Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pd.dismiss();
                if (error.toString().contains("TimeOut")) {
                    Toast.makeText(getActivity(), "No Connection !", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getActivity(), "Something went wrong, we'll fix this one soon !", Toast.LENGTH_SHORT).show();
                }
//                getActivity().finish();
            }
        });
        jar.setRetryPolicy(new DefaultRetryPolicy(
                15000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Volley.newRequestQueue(getActivity()).add(jar);

        return v;
    }
}
