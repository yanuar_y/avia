package travel.avia.tour.views.adapter.recyclerview;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import travel.avia.tour.R;

/**
 * Created by fitrahramadhan on 2/21/16.
 */
public class ListTourAdapter extends RecyclerView.Adapter<ListTourAdapter.ViewHolder> {
    Context context;
    private final String[] imageTour;
    private final String[] titleTour;
    private final String[] priceTour;
    private int lastPosition = -1;


    public ListTourAdapter(Context context, String[] imageStore, String[] titleTour, String[] priceStore) {
        super();
        this.context = context;
        this.imageTour = imageStore;
        this.titleTour = titleTour;
        this.priceTour = priceStore;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        this.context = parent.getContext();
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_list_tour,parent, false);
        return new ViewHolder(v);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView image_tour;
        TextView title_tour;
        TextView price_tour;
        RelativeLayout container;

        public ViewHolder(View v) {
            super(v);
            container = (RelativeLayout) v.findViewById(R.id.container_list_tour);
            image_tour = (ImageView) v.findViewById(R.id.image_list_tour);
            title_tour = (TextView) v.findViewById(R.id.list_tour_title);
            price_tour = (TextView) v.findViewById(R.id.list_tour_price);
        }
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.price_tour.setText(priceTour[position]);
        Picasso.with(context).load(imageTour[position]).fit().placeholder(R.drawable.aviatour).error(R.drawable.aviatour).into(holder.image_tour);
        holder.title_tour.setText(titleTour[position]);
        setAnimation(holder.container, position);
    }

    @Override
    public int getItemCount() {
        return imageTour.length;
    }

    private void setAnimation(View viewToAnimate, int position) {
        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(context, android.R.anim.slide_in_left);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }
}
