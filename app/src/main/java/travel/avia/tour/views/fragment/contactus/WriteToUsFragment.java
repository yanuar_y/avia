package travel.avia.tour.views.fragment.contactus;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import travel.avia.tour.R;

/**
 * Created by fitrahramadhan on 2/7/16.
 */
public class WriteToUsFragment extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fargment_write, container, false);

        getActivity().setTitle("Write to Us");

        return v;
    }
}
