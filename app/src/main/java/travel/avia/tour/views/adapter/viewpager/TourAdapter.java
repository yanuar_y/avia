package travel.avia.tour.views.adapter.viewpager;

/**
 * Created by fitrahramadhan on 2/20/16.
 */

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import travel.avia.tour.views.fragment.tour.ListTourFragment;

public class TourAdapter extends FragmentStatePagerAdapter {
    int numOfTabs;
    String[] regionId;

    public TourAdapter(FragmentManager fm, int numOfTabs, String[] regionId) {
        super(fm);
        this.numOfTabs = numOfTabs;
        this.regionId = regionId;
    }

    @Override
    public Fragment getItem(int position) {
        ListTourFragment tab = new ListTourFragment();
        tab.setRegionId(regionId[position]);
        return tab;
    }

    @Override
    public int getCount() {
        return numOfTabs;
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }
}
