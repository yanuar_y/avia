package travel.avia.tour.views.adapter.expandablelistview;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import java.util.HashMap;
import java.util.List;

import travel.avia.tour.R;

public class ListFaqAdapter extends BaseExpandableListAdapter {
    Context konteks;
    List<String> parent;
    HashMap<String,List<String>> child;

    public ListFaqAdapter(Context konteks, List<String> parent, HashMap<String, List<String>> child) {
        this.konteks=konteks;
        this.parent=parent;
        this.child=child;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    //Grup

    @Override
    public int getGroupCount() {
        return parent.size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return parent.get(groupPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView,
                             ViewGroup parent) {
        String judul = (String) getGroup(groupPosition);
        if(convertView == null){
            LayoutInflater inflater=(LayoutInflater) konteks
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView=inflater.inflate(R.layout.adapter_faq_parent_item, parent, false);
        }
        TextView t1=(TextView)convertView.findViewById(R.id.parent_item);

        t1.setTypeface(null, Typeface.BOLD);
        t1.setText(judul);
        return convertView;
    }

    //Child

    @Override
    public int getChildrenCount(int groupPosition) {
        int size;
        try {
            size = child.get(parent.get(groupPosition)).size();
        }
        catch (NullPointerException npe) {
            return 0;
        }
        return size;
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return child.get(parent.get(groupPosition)).get(childPosition);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild,
                             View convertView, ViewGroup parent) {

        String item = (String) getChild(groupPosition, childPosition);
        if(convertView==null){
            LayoutInflater inflater=(LayoutInflater)konteks
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView=inflater.inflate(R.layout.adapter_faq_child_item,parent,false);
        }
        WebView t1 = (WebView) convertView.findViewById(R.id.faq_child_item_content);
        t1.loadData(item, "text/html; charset=UTF-8", null);

        return convertView;
    }
}
