package travel.avia.tour.views.activity.tour;

import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import travel.avia.tour.BuildConfig;
import travel.avia.tour.R;
import travel.avia.tour.controls.network.InternetConnection;
import travel.avia.tour.views.adapter.viewpager.TourAdapter;
import travel.avia.tour.views.dialog.SortTourDialogFragment;

/**
 * Created by fitrahramadhan on 2/15/16.
 */
public class RegionActivity extends AppCompatActivity{

    private static final String TAG = "RegionActivity";
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private String[] idRegion;
    private String[] descriptionRegion;
    private ProgressDialog pd;
    private TourAdapter viewPagerTourAdapter;

    public static int sort = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.d("DEBUG_"+TAG, "Set content view");
        setContentView(R.layout.activity_region);

        Log.d("DEBUG_"+TAG, "Set toolbar");
        Toolbar toolbar = (Toolbar) findViewById(R.id.region_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Log.d("DEBUG_"+TAG, "Initial layout");
        viewPager = (ViewPager) findViewById(R.id.region_view_pager);
        tabLayout = (TabLayout) findViewById(R.id.region_tab_layout);

        Log.d("DEBUG_"+TAG, "Hide tab layout and view pager");
        viewPager.setVisibility(View.INVISIBLE);
        tabLayout.setVisibility(View.INVISIBLE);

        Log.d("DEBUG_"+TAG, "Show progress dialog");
        pd = ProgressDialog.show(this, "", "Loading...", true);

        Log.d("DEBUG_"+TAG, "Load data region");
        getRegion();
    }
    private void getRegion() {
        if (InternetConnection.isAvailable(this)) {
            Log.d("DEBUG_"+TAG, "get region");
            String url = BuildConfig.URL_BASE + "/?method=list_region&key=" + BuildConfig.KEY;
            JsonObjectRequest jar = new JsonObjectRequest(Request.Method.GET, url, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.d("DEBUG_"+TAG, "get region berhasil");
                    try {
                        JSONArray listRegion = response.optJSONArray("data");
                        int numRegion = listRegion.length();
                        idRegion = new String[numRegion];
                        descriptionRegion = new String[numRegion];
                        JSONObject region;
                        for (int i = 0; i < numRegion; i++) {
                            region = listRegion.getJSONObject(i);
                            tabLayout.addTab(tabLayout.newTab().setText(region.getString("tour_region_name")));
                            idRegion[i] = region.getString("tour_region_id");
                            descriptionRegion[i] = region.getString("tour_region_description");
                        }
                        viewPager.setVisibility(View.VISIBLE);
                        tabLayout.setVisibility(View.VISIBLE);
                        pd.dismiss();
                        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
                        viewPagerTourAdapter = new TourAdapter(getSupportFragmentManager(),
                                tabLayout.getTabCount(), idRegion);
                        viewPager.setAdapter(viewPagerTourAdapter);
                        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
                        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                            @Override
                            public void onTabSelected(TabLayout.Tab tab) {
                                viewPager.setCurrentItem(tab.getPosition());
                            }

                            @Override
                            public void onTabUnselected(TabLayout.Tab tab) {

                            }

                            @Override
                            public void onTabReselected(TabLayout.Tab tab) {

                            }
                        });
                        viewPager.setCurrentItem(2);
                    } catch (JSONException je) {
                        Log.d("DEBUG_" + TAG, "JSON error : " + je.toString());
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.d("DEBUG_"+TAG, "Error : "+error.toString());
                    finish();
                }
            });
            jar.setRetryPolicy(new DefaultRetryPolicy(
                    15000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            Volley.newRequestQueue(this).add(jar);
        }
        else {
            Log.d("DEBUG_" + TAG, "No Internet Connection");
            finish();
        }
    }

    @Override
    public void onBackPressed() {
        Log.d("DEBUG_"+TAG, "Back pressed");
        finish();
        super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        Log.d("DEBUG_"+TAG, "Initial menu");
        getMenuInflater().inflate(R.menu.menu_sort, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            Log.d("DEBUG_"+TAG, "Home clicked");
            finish();
        }
        else if (id == R.id.sort) {
            Log.d("DEBUG_"+TAG, "Sort clicked");
            SortTourDialogFragment newFragment = SortTourDialogFragment.newInstance();
            newFragment.setStyle(DialogFragment.STYLE_NO_TITLE, 0);
            newFragment.show(getFragmentManager(), "dialog");
        }
        return true;
    }

    public void refresh() {
        Log.d("DEBUG_"+TAG, "Data tour refresh");
        viewPagerTourAdapter.notifyDataSetChanged();
    }

}
