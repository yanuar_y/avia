package travel.avia.tour.views.adapter.scrollview;

/**
 * Created by fitrahramadhan on 2/21/2016.
 */
public interface CustomScrollViewListener {
   void onScrollChanged(CustomScrollView scrollView, int x, int y, int oldx, int oldy);
   void onScrollChangedParallax(int deltaX, int deltaY);
}
