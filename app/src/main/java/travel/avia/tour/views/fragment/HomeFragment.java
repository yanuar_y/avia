package travel.avia.tour.views.fragment;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import travel.avia.tour.BuildConfig;
import travel.avia.tour.R;
import travel.avia.tour.controls.network.InternetConnection;
import travel.avia.tour.controls.utils.Formatter;
import travel.avia.tour.views.activity.MainActivity;
import travel.avia.tour.views.activity.tour.RegionActivity;
import travel.avia.tour.views.activity.tour.TourDetailActivity;
import travel.avia.tour.views.adapter.scrollview.CustomScrollView;
import travel.avia.tour.views.adapter.scrollview.CustomScrollViewListener;
import travel.avia.tour.views.custom.imageview.RoundedImage;


public class HomeFragment extends Fragment implements CustomScrollViewListener{
    private static final String TAG = "HomeFragment";

    private ImageView hot[] = new ImageView[10];
    private TextView titlehot[] = new TextView[10], hargahot[] = new TextView[10];
    private Formatter harga;
    private CustomScrollView skrol;
    private LinearLayout content, noConnection, menu;
    private int pixels, tinggi=200;
    private float scale;
    private boolean up=false, down=false;
    public static MainActivity mainActivity;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v  =  inflater.inflate(R.layout.fragment_home, container, false);

        getActivity().setTitle("Avia Tour");

        Log.d("DEBUG_"+TAG, "Initial title text");
        final TextView titleTourRandom = (TextView) v.findViewById(R.id.title_tour_random);

        Log.d("DEBUG_"+TAG, "Initial harga tour");
        hargahot[0]  =  (TextView)  v.findViewById(R.id.hargahot1);
        hargahot[1] = (TextView) v.findViewById(R.id.hargahot2);
        hargahot[2] = (TextView) v.findViewById(R.id.hargahot3);
        hargahot[3] = (TextView) v.findViewById(R.id.hargahot4);
        hargahot[4] = (TextView) v.findViewById(R.id.hargahot5);
        hargahot[5] = (TextView) v.findViewById(R.id.hargahot6);
        hargahot[6] = (TextView) v.findViewById(R.id.hargahot7);
        hargahot[7] = (TextView) v.findViewById(R.id.hargahot8);
        hargahot[8] = (TextView) v.findViewById(R.id.hargahot9);
        hargahot[9] = (TextView) v.findViewById(R.id.hargahot10);

        Log.d("DEBUG_"+TAG, "Initial title tour");
        titlehot[0] = (TextView)  v.findViewById(R.id.titlehot1);
        titlehot[1] = (TextView) v.findViewById(R.id.titlehot2);
        titlehot[2] = (TextView) v.findViewById(R.id.titlehot3);
        titlehot[3] = (TextView) v.findViewById(R.id.titlehot4);
        titlehot[4] = (TextView) v.findViewById(R.id.titlehot5);
        titlehot[5] = (TextView) v.findViewById(R.id.titlehot6);
        titlehot[6] = (TextView) v.findViewById(R.id.titlehot7);
        titlehot[7] = (TextView) v.findViewById(R.id.titlehot8);
        titlehot[8] = (TextView) v.findViewById(R.id.titlehot9);
        titlehot[9] = (TextView) v.findViewById(R.id.titlehot10);

        Log.d("DEBUG_"+TAG, "Initial image tour");
        hot[0] = (ImageView)  v.findViewById(R.id.hot1);
        hot[1] = (ImageView) v.findViewById(R.id.hot2);
        hot[2] = (ImageView) v.findViewById(R.id.hot3);
        hot[3] = (ImageView) v.findViewById(R.id.hot4);
        hot[4] = (ImageView) v.findViewById(R.id.hot5);
        hot[5] = (ImageView) v.findViewById(R.id.hot6);
        hot[6] = (ImageView) v.findViewById(R.id.hot7);
        hot[7] = (ImageView) v.findViewById(R.id.hot8);
        hot[8] = (ImageView) v.findViewById(R.id.hot9);
        hot[9] = (ImageView) v.findViewById(R.id.hot10);

        Log.d("DEBUG_"+TAG, "Initial image menu");
        final ImageView menu1 = (ImageView) v.findViewById(R.id.menu1);
        final ImageView menu2 = (ImageView) v.findViewById(R.id.menu2);
        final ImageView menu3 = (ImageView) v.findViewById(R.id.menu3);
        final ImageView menu4 = (ImageView) v.findViewById(R.id.menu4);

        skrol = (CustomScrollView) v.findViewById(R.id.skrol);

        Log.d("DEBUG_"+TAG, "Initial linear layout");
        menu = (LinearLayout) v.findViewById(R.id.menu);
        content = (LinearLayout) v.findViewById(R.id.scrollContent);
        noConnection = (LinearLayout) v.findViewById(R.id.no_connection);

        Log.d("DEBUG_"+TAG, "Initial object formatter");
        harga = new Formatter();

        Log.d("DEBUG_"+TAG, "Set size layout menu");
        scale = getResources().getDisplayMetrics().density;
        skrol.setScrollViewListener(this);
        pixels = (int) (tinggi * scale + 0.5f);
        menu.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, pixels));

        if (InternetConnection.isAvailable(getActivity())) {
            noConnection.setVisibility(View.GONE);
            Log.d("DEBUG_"+TAG, "Get data random tour");
            String URL = BuildConfig.URL_BASE+"/?method=featured_tour&limit=10&key="+BuildConfig.KEY;
            titleTourRandom.setText("Loading...");
            JsonObjectRequest jor = new JsonObjectRequest(Request.Method.GET, URL, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.d("DEBUG_" + TAG, "Get data berhasil");
                    try {
                        titleTourRandom.setText("Recommended Tour");
                        JSONObject data = response.getJSONObject("data");
                        JSONArray jar = data.getJSONArray("featured");
                        for (int i = 0; i < 10; i++) {
                            JSONObject jobj = jar.getJSONObject(i);
                            Picasso.with(getActivity()).load(BuildConfig.URL_IMAGE + jobj.getString("image")).fit().transform(new RoundedImage(25, 0)).into(hot[i]);
                            final String id = jobj.getString("tour_product_id");
                            final String deskripsi = jobj.getString("tour_master_description");
                            titlehot[i].setText(jobj.getString("tour_master_name"));
                            hargahot[i].setText(harga.RupiahFormat(jobj.getString("raw_price")));
                            hot[i].setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    TourDetailActivity.tourId = id;
                                    TourDetailActivity.deskripsi = deskripsi;
                                    Intent i = new Intent(getActivity(), TourDetailActivity.class);
                                    startActivity(i);
                                }
                            });
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    titleTourRandom.setText("");
                    Log.d("DEBUG_" + TAG, "Failed get data : " + error.toString());
                }
            });
            jor.setRetryPolicy(new DefaultRetryPolicy(
                    15000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            try {
                Volley.newRequestQueue(getActivity()).add(jor);
            } catch (NullPointerException npe) {
            }
        }
        else {
            Log.d("DEBUG_"+TAG, "No internet connection !");
            content.setVisibility(View.GONE);
        }

        Log.d("DEBUG_"+TAG, "Set image menu");
        Picasso.with(getActivity()).load(R.drawable.tour).transform(new RoundedImage(25,0)).into(menu1);
        Picasso.with(getActivity()).load(R.drawable.paket).transform(new RoundedImage(25,0)).into(menu2);
        Picasso.with(getActivity()).load(R.drawable.passport_visa).transform(new RoundedImage(25,0)).into(menu3);
        Picasso.with(getActivity()).load(R.drawable.insurance).transform(new RoundedImage(25,0)).into(menu4);

        menu1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("DEBUG_"+TAG, "Menu 1 clicked");
                startActivity(new Intent(getActivity(), RegionActivity.class));
            }
        });
        menu2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("DEBUG_"+TAG, "Menu 2 clicked");
                Toast.makeText(getActivity(), "Fitur ini belum tersedia.", Toast.LENGTH_SHORT).show();
                //startActivity(new Intent(getActivity(), PackageActivity.class));
            }
        });
        menu3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("DEBUG_"+TAG, "Menu 3 clicked");
                Toast.makeText(getActivity(), "Fitur ini belum tersedia.", Toast.LENGTH_SHORT).show();
                //startActivity(new Intent(getActivity(), PassportVisaActivity.class));
            }
        });
        menu4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("DEBUG_"+TAG, "Menu 4 clied");
                Toast.makeText(getActivity(), "Fitur ini belum tersedia.", Toast.LENGTH_SHORT).show();
                //startActivity(new Intent(getActivity(), InsuranceActivity.class));
            }
        });
        return v;
    }

    @Override
    public void onResume() {
        Log.d("DEBUG_"+TAG, "On resume");
        super.onResume();
        mainActivity.changeTitlePromoMenu();
    }

    @Override
    public void onScrollChanged(CustomScrollView scrollView, int x, int y, int oldx, int oldy) {
        int max = skrol.getChildAt(0).getHeight() / 2;
        if(y<oldy){
            if (up) {
                up = false;
            }
            else {
                down = true;
                if (!(y >= max || oldy >= max)) {
                    tinggi += 2;
                    if (tinggi >= 200) tinggi = 200;
                    else {
                        pixels = (int) (tinggi * scale + 0.5f);
                        menu.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, pixels));
                    }
                }
            }
        }
        else if(y>=oldy){
            if(down) {
                down = false;
            }
            else {
                up = true;
                if (!(y <= 0 || oldy <= 0)) {
                    tinggi -= 2;
                    if (tinggi <= 125) tinggi = 125;
                    else {
                        pixels = (int) (tinggi * scale + 0.5f);
                        menu.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, pixels));
                    }
                }
            }
        }
    }

    @Override
    public void onScrollChangedParallax(int deltaX, int deltaY) {

    }
}

