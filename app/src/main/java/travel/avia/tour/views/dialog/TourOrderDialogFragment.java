package travel.avia.tour.views.dialog;

import android.app.DialogFragment;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import travel.avia.tour.R;
import travel.avia.tour.views.activity.tour.TourBookActivity;
import travel.avia.tour.views.activity.tour.TourDetailActivity;

/**
 * Created by fitrahramadhan on 2/15/16.
 */
public class TourOrderDialogFragment extends DialogFragment implements View.OnClickListener {

    private final static String TAG = "TourOrderDF";

    private static int order[] = new int[7];
    private Button min[] = new Button[5], plus[] = new Button[5];
    private EditText visitor[]=new EditText[5];
    private int bnykNol=0;
    private int minplus;

    public static TourOrderDialogFragment newInstance() {
        return new TourOrderDialogFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.dialog_fragment_tour_order, container, false);
        
        Log.d("DEBUG_"+TAG, "Initial view");
        final Button cancel = (Button) v.findViewById(R.id.tour_order_cancel);
        final Button next = (Button) v.findViewById(R.id.tour_order_next);

        visitor[0] = (EditText) v.findViewById(R.id.tour_order_adult_twin_share);
        visitor[1] = (EditText) v.findViewById(R.id.tour_order_child_twin_share);
        visitor[2] = (EditText) v.findViewById(R.id.tour_order_child_with_extra_bed);
        visitor[3] = (EditText) v.findViewById(R.id.tour_order_child_without_bed);
        visitor[4] = (EditText) v.findViewById(R.id.tour_order_infant);

        min[0] = (Button) v.findViewById(R.id.min_adult);
        plus[0] = (Button) v.findViewById(R.id.plus_adult);
        min[1] = (Button) v.findViewById(R.id.min_child_twin_share);
        plus[1] = (Button) v.findViewById(R.id.plus_child_twin_share);
        min[2] = (Button) v.findViewById(R.id.min_child_with_extra_bed);
        plus[2] = (Button) v.findViewById(R.id.plus_child_with_extra_bed);
        min[3] = (Button) v.findViewById(R.id.min_child_without_bed);
        plus[3] = (Button) v.findViewById(R.id.plus_child_without_bed);
        min[4] = (Button) v.findViewById(R.id.min_infant);
        plus[4] = (Button) v.findViewById(R.id.plus_infant);

        Log.d("DEBUG_"+TAG, "Initial min and plus on click");
        for (minplus=0; minplus<5; minplus++) {
            min[minplus].setOnClickListener(this);
            plus[minplus].setOnClickListener(this);
        }

        Log.d("DEBUG_"+TAG, "Hide view if produk not available");
        visitor[0].setText("1");
        for (int i=1; i<5; i++) {
            if (!TourDetailActivity.jenisProduk[i]) {
                visitor[i].setVisibility(View.GONE);
                min[i].setVisibility(View.GONE);
                plus[i].setVisibility(View.GONE);
            }
            visitor[i].setText("0");
        }

        Log.d("DEBUG_"+TAG, "Initial cancel on click");
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("DEBUG_"+TAG, "Cancel clicked");
                dismiss();
            }
        });

        Log.d("DEBUG_"+TAG, "Inital next on click");
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("DEBUG_"+TAG, "Next clicked");
                boolean proses=true;
                order[5] = 0;
                for (int i=0; i<5; i++) {
                    String isi = visitor[i].getText().toString();
                    Log.d("DEBUG_"+TAG, "Cek input kosong");
                    if (isi.equals("")) {
                        proses = false;
                        break;
                    }
                    else {
                        Log.d("DEBUG_"+TAG, "Hitung member 0");
                        if (i==4) i=6;
                        order[i] = Integer.parseInt(isi);
                        order[5] += order[i];
                        if (order[i]==0) {
                            bnykNol++;
                        }
                    }
                }
                if (proses) {
                    if (bnykNol==5) {
                        Log.d("DEBUG_"+TAG, "Hitung member 0");
                        Toast.makeText(getActivity(), "Semua data tidak boleh nol.", Toast.LENGTH_SHORT).show();
                    }
                    else if (order[0]==0 ){
                        Log.d("DEBUG_"+TAG, "Cek jika adult nol");
                        Toast.makeText(getActivity(), "Minimal ada 1 orang dewasa.", Toast.LENGTH_SHORT).show();
                        visitor[0].setText("1");
                    }
                    else {
                        order[4] = 0;
                        TourBookActivity tbf = new TourBookActivity();
                        tbf.setData(TourDetailActivity.nameTour, TourDetailActivity.deskripsi,
                                TourDetailActivity.image, order, TourDetailActivity.hargaProduk,
                                TourDetailActivity.departure, TourDetailActivity.arrival,
                                TourDetailActivity.day, TourDetailActivity.flight, TourDetailActivity.city,
                                TourDetailActivity.cityId,
                                TourDetailActivity.tourId, TourDetailActivity.roomId,
                                TourDetailActivity.visaCountryId, TourDetailActivity.visaCountryName,
                                TourDetailActivity.visaNameAdult, TourDetailActivity.visaNameChild,
                                TourDetailActivity.visaIdAdult, TourDetailActivity.visaIdChild,
                                TourDetailActivity.visaPriceAdult, TourDetailActivity.visaPriceChild,
                                TourDetailActivity.visaNumAdult, TourDetailActivity.visaNumChild);
                        startActivity(new Intent(getActivity(), TourBookActivity.class));
                    }
                }
                else {
                    Toast.makeText(getActivity(), "Semua data harus diisi.", Toast.LENGTH_SHORT).show();
                }
            }
        });
        return v;
    }

    @Override
    public void onClick(View v) {
        for (minplus=0; minplus<5; minplus++) {
            if (v == min[minplus]) {
                Log.d("DEBUG_"+TAG, "min clikced");
                final String visit = visitor[minplus].getText().toString();
                if (visit.equals("1") && minplus==0) Toast.makeText(getActivity(), "Minimal ada 1 orang dewasa.", Toast.LENGTH_SHORT).show();
                else if ((!visit.equals("0"))) {
                    visitor[minplus].setText((Integer.parseInt(visit)-1)+"");
                }
            }
            else if (v == plus[minplus]) {
                Log.d("DEBUG_"+TAG, "plus clikced");
                visitor[minplus].setText(((Integer.parseInt(visitor[minplus].getText().toString()) + 1) + ""));
            }
        }
    }
}
