package travel.avia.tour.views.activity.tour;

import android.app.DatePickerDialog;
import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import travel.avia.tour.BuildConfig;
import travel.avia.tour.R;
import travel.avia.tour.controls.network.InternetConnection;
import travel.avia.tour.controls.utils.CustomRequest;
import travel.avia.tour.controls.utils.Formatter;
import travel.avia.tour.database.DBDataSource;
import travel.avia.tour.views.dialog.TermConditionDialogFragment;

/**
 * Created by fitrahramadhan on 2/15/16.
 */
public class TourBookActivity extends AppCompatActivity implements
        View.OnTouchListener,
        AdapterView.OnItemSelectedListener,
        CompoundButton.OnCheckedChangeListener,
        View.OnClickListener {

    private static final String TAG = "TourBookActivity";

    public static String  tourId, description, image,title;
    public static String  hargaProduk[] = new String[7], departure, arrival, day, flight, judul,
            city[], cityId[], roomId[] = new String[6], visaCountryId[], visaCountryName[], visaNameAdult[][],
            visaNameChild[][], visaIdAdult[][], visaIdChild[][], visaPriceAdult[][], visaPriceChild[][];
    public static int visaNumAdult[], visaNumChild[];
    private static int  order[], visaPrice;

    private ProgressDialog pd;
    private Button book;
    private int yangdiset;
    private ScrollView scrollView;
    private CheckBox term;
    private Spinner formGender, formCountry, listVisaAdult[], listVisaChild[];
    private EditText formFirstName, formLastName, formBirthdate, formTelpon, formEmail, formAddress,
            formPostcode, formTOF;
    private LinearLayout layoutForm, layoutVisa, layoutRincianVisa, layoutListVisaAdult[], layoutListVisaChild[];
    private Double sumAdult = 0.0, sumChildTwin = 0.0, sumChildExtra = 0.0, sumChildWithout = 0.0,
            sumInfant = 0.0, sumSingle = 0.0, sumInternational = 0.0, subTotal = 0.0;
    private int numPassportAdult[] = {0, 0, 0, 0, 0},numPassportChild[] = {0, 0, 0}, numView,
            numVisaAdult, numVisaChild, i;
    private Double pricePassportAdult[]=new Double[5], pricePassportChild[]=new Double[3];
    private String pricePassportAdultId[]=new String[5], pricePassportChildId[]=new String[3];
    private Switch[] copy, single, passportBaru, switchVisaAdult, switchVisaChild;
    private TextView textPassportKurang7Bulan[], passportBaru4, passportBaru41, passportBaru42,
            passportBaru14, passportBaru141, passportBaru142, passportBaru4Daerah,
            passportBaru4Daerah1, passportBaru4Daerah2, passportBaru14Daerah, passportBaru14Daerah1,
            passportBaru14Daerah2, passportBaruEpaspor, passportBaruEpaspor1, passportBaruEpaspor2,
            passportBaru4Child, passportBaru41Child, passportBaru42Child,
            passportBaru14Child, passportBaru141Child, passportBaru142Child, passportBaruEpasporChild,
            passportBaruEpaspor1Child, passportBaruEpaspor2Child,
            singleSupplementTitle, singleSupplement, singleSupplementPrice;
    private Spinner[] gender, country, passport, meal;
    private EditText[] firstName, lastName, birthDate, passportNumber, expirityPassport;
    private DatePickerDialog.OnDateSetListener mDateSetListener;
    private TextView subtotal;
    private DatePickerDialog datePicker;
    private WebView termShow;
    private final Formatter formatter = new Formatter();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.d("DEBUG_"+TAG, "Set content view");
        setContentView(R.layout.activity_tour_book);

        getPassportPrice();

        Log.d("DEBUG_"+TAG, "Layout summary");
        final TextView juduls = (TextView) findViewById(R.id.tour_book_title);
        final TextView departures = (TextView) findViewById(R.id.tour_book_departure);
        final TextView arrivals = (TextView) findViewById(R.id.tour_book_arrival);
        final TextView days = (TextView) findViewById(R.id.tour_book_days);
        final TextView flights = (TextView) findViewById(R.id.tour_book_flight);
        final TextView person = (TextView) findViewById(R.id.tour_book_person);
        final TextView adult = (TextView) findViewById(R.id.tour_book_adult);
        final TextView childTwinShare = (TextView) findViewById(R.id.tour_book_child_twin_bed);
        final TextView childWithExtraBed = (TextView) findViewById(R.id.tour_book_child_with_extra_bed);
        final TextView childWithoutBed = (TextView) findViewById(R.id.tour_book_child_without_bed);
        final TextView infant = (TextView) findViewById(R.id.tour_book_infant);
        final TextView adultPrice = (TextView) findViewById(R.id.tour_book_adult_price);
        final TextView childTwinSharePrice = (TextView) findViewById(R.id.tour_book_child_twin_bed_price);
        final TextView childWithExtraBedPrice = (TextView) findViewById(R.id.tour_book_child_with_extra_bed_price);
        final TextView childWithoutBedPrice = (TextView) findViewById(R.id.tour_book_child_without_bed_price);
        final TextView infantPrice = (TextView) findViewById(R.id.tour_book_infant_price);
        final TextView internationalAirline = (TextView) findViewById(R.id.tour_book_international_airlinetax);
        final TextView internationalAirlinePrice = (TextView) findViewById(R.id.tour_book_international_airlinetax_price);

        Log.d("DEBUG_"+TAG, "Initial form member");
        copy = new Switch[order[0]];
        single = new Switch[order[0]];
        passportBaru = new Switch[order[5]];
        gender = new Spinner[order[5]];
        country = new Spinner[order[5]];
        passport = new Spinner[order[5]];
        meal = new Spinner[order[5]];
        firstName=new EditText[order[5]];
        lastName=new EditText[order[5]];
        birthDate=new EditText[order[5]];
        passportNumber=new EditText[order[5]];
        textPassportKurang7Bulan = new TextView[order[5]];
        expirityPassport= new EditText[order[5]];
        numVisaAdult = order[0]*visaCountryId.length;
        numVisaChild = (order[1]+order[2]+order[3]+order[6])*visaCountryId.length;
        listVisaAdult = new Spinner[numVisaAdult];
        layoutListVisaAdult = new LinearLayout[numVisaAdult];
        switchVisaAdult = new Switch[numVisaAdult];
        listVisaChild = new Spinner[numVisaChild];
        switchVisaChild = new Switch[numVisaChild];
        layoutListVisaChild = new LinearLayout[numVisaChild];

        Log.d("DEBUG_"+TAG, "Set layout contact");
        formGender = (Spinner) findViewById(R.id.form_book_gender);
        formGender.setAdapter(spinnerAdapter(getResources().getStringArray(R.array.gender_adult)));
        formFirstName = (EditText) findViewById(R.id.form_book_first_name);
        formLastName = (EditText) findViewById(R.id.form_book_last_name);
        formBirthdate = (EditText) findViewById(R.id.form_book_birthdate);
        formTelpon = (EditText) findViewById(R.id.form_book_telpon);
        formEmail = (EditText) findViewById(R.id.form_book_email);
        formAddress = (EditText) findViewById(R.id.form_book_address);
        formCountry = (Spinner) findViewById(R.id.form_book_country);
        String[] city2 = city.clone();
        city2[0] = "Country";
        formCountry.setAdapter(spinnerAdapter(city2));
        formPostcode = (EditText) findViewById(R.id.form_post_code);
        scrollView = (ScrollView) findViewById(R.id.tour_book_scroll_view);

        Log.d("DEBUG_"+TAG, "Layout form");
        layoutForm = (LinearLayout) findViewById(R.id.layout_form);

        singleSupplementTitle = (TextView) findViewById(R.id.title_single_supplement);
        singleSupplement = (TextView) findViewById(R.id.tour_book_single_suplement);
        singleSupplementPrice = (TextView) findViewById(R.id.tour_book_single_suplement_price);
        subtotal = (TextView) findViewById(R.id.subtotal);
        passportBaru4 = (TextView) findViewById(R.id.passport_baru_4);
        passportBaru41 = (TextView) findViewById(R.id.passport_baru_41);
        passportBaru42 = (TextView) findViewById(R.id.passport_baru_42);
        passportBaru14 = (TextView) findViewById(R.id.passport_baru_14);
        passportBaru141 = (TextView) findViewById(R.id.passport_baru_141);
        passportBaru142 = (TextView) findViewById(R.id.passport_baru_142);
        passportBaru4Daerah = (TextView) findViewById(R.id.passport_baru_4_daerah);
        passportBaru4Daerah1 = (TextView) findViewById(R.id.passport_baru_4_daerah1);
        passportBaru4Daerah2 = (TextView) findViewById(R.id.passport_baru_4_daerah2);
        passportBaru14Daerah = (TextView) findViewById(R.id.passport_baru_14_daerah);
        passportBaru14Daerah1 = (TextView) findViewById(R.id.passport_baru_14_daerah1);
        passportBaru14Daerah2 = (TextView) findViewById(R.id.passport_baru_14_daerah2);
        passportBaruEpaspor = (TextView) findViewById(R.id.tour_book_passport_baru_epaspor_title);
        passportBaruEpaspor1 = (TextView) findViewById(R.id.tour_book_passport_baru_epaspor_qty);
        passportBaruEpaspor2 = (TextView) findViewById(R.id.tour_book_passport_baru_epaspor_price);
        passportBaru4Child = (TextView) findViewById(R.id.passport_baru_4_child);
        passportBaru41Child = (TextView) findViewById(R.id.passport_baru_41_child);
        passportBaru42Child = (TextView) findViewById(R.id.passport_baru_42_child);
        passportBaru14Child = (TextView) findViewById(R.id.passport_baru_14_child);
        passportBaru141Child = (TextView) findViewById(R.id.passport_baru_141_child);
        passportBaru142Child = (TextView) findViewById(R.id.passport_baru_142_child);
        passportBaruEpasporChild = (TextView) findViewById(R.id.tour_book_passport_baru_epaspor_title_child);
        passportBaruEpaspor1Child = (TextView) findViewById(R.id.tour_book_passport_baru_epaspor_qty_child);
        passportBaruEpaspor2Child = (TextView) findViewById(R.id.tour_book_passport_baru_epaspor_price_child);

        Log.d("DEBUG_"+TAG, "Layout term");
        formTOF = (EditText) findViewById(R.id.tour_book_termofservice);
        layoutRincianVisa = (LinearLayout) findViewById(R.id.tour_book_layout_visa);
        book = (Button) findViewById(R.id.tour_book_next);
        term = (CheckBox) findViewById(R.id.tour_book_term);
        termShow = (WebView) findViewById(R.id.tour_term_and_condition);

        Log.d("DEBUG_"+TAG, "Set form member adult");
        int peopleIndex = 0;
        int numVisaAdultIndex = 0;
        for (i=0; i<order[0]; i++) {
            View form = TourBookActivity.this.getLayoutInflater().inflate(R.layout.layout_book_tour_adult, null);
            final TextView title = (TextView) form.findViewById(R.id.form_adult_title);
            title.setText("Adult "+(i+1));
            copy[peopleIndex] = (Switch) form.findViewById(R.id.form_adult_copy);
            single[peopleIndex] = (Switch) form.findViewById(R.id.form_adult_single_suplemnet);
            gender[peopleIndex] = (Spinner) form.findViewById(R.id.form_adult_gender);
            gender[peopleIndex].setAdapter(spinnerAdapter(getResources().getStringArray(R.array.gender_adult)));
            country[peopleIndex] = (Spinner) form.findViewById(R.id.form_adult_poi);
            country[peopleIndex].setAdapter(spinnerAdapter(city));
            passport[peopleIndex] = (Spinner) form.findViewById(R.id.form_adult_passport);
            meal[peopleIndex] = (Spinner) form.findViewById(R.id.form_adult_meal);
            meal[peopleIndex].setAdapter(spinnerAdapter(getResources().getStringArray(R.array.type_of_meal)));
            firstName[peopleIndex] = (EditText) form.findViewById(R.id.form_adult_first_name);
            lastName[peopleIndex] = (EditText) form.findViewById(R.id.form_adult_last_name);
            birthDate[peopleIndex] = (EditText) form.findViewById(R.id.form_adult_birt_date);
            passportNumber[peopleIndex] = (EditText) form.findViewById(R.id.form_adult_passport_number);
            textPassportKurang7Bulan[peopleIndex] = (TextView) form.findViewById(R.id.form_adult_passport_7_bulan);
            expirityPassport[peopleIndex] = (EditText) form.findViewById(R.id.form_adult_expirity_passport);
            passportBaru[peopleIndex] = (Switch) form.findViewById(R.id.form_adult_pembuatan_passport);
            if (numVisaAdult>0) {
                for (int i = 0; i < visaCountryId.length; i++) {
                    layoutVisa = (LinearLayout) form.findViewById(R.id.form_adult_layout_visa);
                    View visaView = TourBookActivity.this.getLayoutInflater().inflate(R.layout.layout_tour_book_visa, null);
                    switchVisaAdult[numVisaAdultIndex] = (Switch) visaView.findViewById(R.id.book_visa_country_name);
                    listVisaAdult[numVisaAdultIndex] = (Spinner) visaView.findViewById(R.id.book_visa_list);
                    layoutListVisaAdult[numVisaAdultIndex] = (LinearLayout) visaView.findViewById(R.id.book_visa_layout_visa_list);
                    switchVisaAdult[numVisaAdultIndex].setText("Pembuatan Visa "+visaCountryName[i].toUpperCase());
                    String[] array = new String[visaNumAdult[i]];
                    for (int j=0; j<visaNumAdult[i]; j++) {
                        array[j] = visaNameAdult[i][j];
                    }
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(TourBookActivity.this,
                            R.layout.spinner_item, array);
                    adapter.setDropDownViewResource(R.layout.spinner_item);
                    listVisaAdult[numVisaAdultIndex].setAdapter(adapter);
                    layoutVisa.addView(visaView);
                    numVisaAdultIndex++;
                }
            }
            layoutForm.addView(form);
            peopleIndex++;
        }
        Log.d("DEBUG_"+TAG, "Set form member child");
        int child=1;
        int numVisaChildIndex=0;
        for (i=0; i<order[1]; i++) {
            View form = TourBookActivity.this.getLayoutInflater().inflate(R.layout.layout_book_tour_child, null);
            TextView title = (TextView) form.findViewById(R.id.form_child_title);
            title.setText("Child "+child+" TWIN SHARE");
            gender[peopleIndex] = (Spinner) form.findViewById(R.id.form_child_gender);
            gender[peopleIndex].setAdapter(spinnerAdapter(getResources().getStringArray(R.array.gender_child)));
            country[peopleIndex] = (Spinner) form.findViewById(R.id.form_child_poi);
            country[peopleIndex].setAdapter(spinnerAdapter(city));
            passport[peopleIndex] = (Spinner) form.findViewById(R.id.form_child_passport);
            firstName[peopleIndex] = (EditText) form.findViewById(R.id.form_child_first_name);
            meal[peopleIndex] = (Spinner) form.findViewById(R.id.form_child_meal);
            meal[peopleIndex].setAdapter(spinnerAdapter(getResources().getStringArray(R.array.type_of_meal)));
            lastName[peopleIndex] = (EditText) form.findViewById(R.id.form_child_last_name);
            birthDate[peopleIndex] = (EditText) form.findViewById(R.id.form_child_birt_date);
            passportNumber[peopleIndex] = (EditText) form.findViewById(R.id.form_child_passport_number);
            expirityPassport[peopleIndex] = (EditText) form.findViewById(R.id.form_child_expirity_passport);
            textPassportKurang7Bulan[peopleIndex] = (TextView) form.findViewById(R.id.form_child_passport_7_bulan);
            passportBaru[peopleIndex] = (Switch) form.findViewById(R.id.form_child_pembuatan_passport);
            if (numVisaChild>0) {
                for (int i = 0; i < visaCountryId.length; i++) {
                    layoutVisa = (LinearLayout) form.findViewById(R.id.form_child_layout_visa);
                    View visaView = TourBookActivity.this.getLayoutInflater().inflate(R.layout.layout_tour_book_visa, null);
                    switchVisaChild[numVisaChildIndex] = (Switch) visaView.findViewById(R.id.book_visa_country_name);
                    switchVisaChild[numVisaChildIndex].setText("Pembuatan Visa "+visaCountryName[i].toUpperCase());
                    listVisaChild[numVisaChildIndex] = (Spinner) visaView.findViewById(R.id.book_visa_list);
                    layoutListVisaChild[numVisaChildIndex] = (LinearLayout) visaView.findViewById(R.id.book_visa_layout_visa_list);
                    String[] array = new String[visaNumChild[i]];
                    for (int j=0; j<visaNumChild[i]; j++) {
                        array[j] = visaNameChild[i][j];
                    }
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(TourBookActivity.this,
                            android.R.layout.simple_spinner_item, array);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    listVisaChild[numVisaChildIndex].setAdapter(adapter);
                    layoutVisa.addView(visaView);
                    numVisaChildIndex++;
                }
            }
            layoutForm.addView(form);
            child++;
            peopleIndex++;
        }
        Log.d("DEBUG_"+TAG, "Set form member child 2");
        for (i=0; i<order[2]; i++) {
            View form = TourBookActivity.this.getLayoutInflater().inflate(R.layout.layout_book_tour_child, null);
            TextView title = (TextView) form.findViewById(R.id.form_child_title);
            title.setText("Child "+child+" WITH EXTRA BED");
            gender[peopleIndex] = (Spinner) form.findViewById(R.id.form_child_gender);
            gender[peopleIndex].setAdapter(spinnerAdapter(getResources().getStringArray(R.array.gender_child)));
            country[peopleIndex] = (Spinner) form.findViewById(R.id.form_child_poi);
            country[peopleIndex].setAdapter(spinnerAdapter(city));
            passport[peopleIndex] = (Spinner) form.findViewById(R.id.form_child_passport);
            firstName[peopleIndex] = (EditText) form.findViewById(R.id.form_child_first_name);
            meal[peopleIndex] = (Spinner) form.findViewById(R.id.form_child_meal);
            meal[peopleIndex].setAdapter(spinnerAdapter(getResources().getStringArray(R.array.type_of_meal)));
            lastName[peopleIndex] = (EditText) form.findViewById(R.id.form_child_last_name);
            birthDate[peopleIndex] = (EditText) form.findViewById(R.id.form_child_birt_date);
            passportNumber[peopleIndex] = (EditText) form.findViewById(R.id.form_child_passport_number);
            expirityPassport[peopleIndex] = (EditText) form.findViewById(R.id.form_child_expirity_passport);
            textPassportKurang7Bulan[peopleIndex] = (TextView) form.findViewById(R.id.form_child_passport_7_bulan);
            passportBaru[peopleIndex] = (Switch) form.findViewById(R.id.form_child_pembuatan_passport);
            if (numVisaChild>0) {
                for (int i = 0; i < visaCountryId.length; i++) {
                    layoutVisa = (LinearLayout) form.findViewById(R.id.form_child_layout_visa);
                    View visaView = TourBookActivity.this.getLayoutInflater().inflate(R.layout.layout_tour_book_visa, null);
                    switchVisaChild[numVisaChildIndex] = (Switch) visaView.findViewById(R.id.book_visa_country_name);
                    switchVisaChild[numVisaChildIndex].setText("Pembuatan Visa "+visaCountryName[i].toUpperCase());
                    listVisaChild[numVisaChildIndex] = (Spinner) visaView.findViewById(R.id.book_visa_list);
                    layoutListVisaChild[numVisaChildIndex] = (LinearLayout) visaView.findViewById(R.id.book_visa_layout_visa_list);
                    String[] array = new String[visaNumChild[i]];
                    for (int j=0; j<visaNumChild[i]; j++) {
                        array[j] = visaNameChild[i][j];
                    }
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(TourBookActivity.this,
                            android.R.layout.simple_spinner_item, array);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    listVisaChild[numVisaChildIndex].setAdapter(adapter);
                    layoutVisa.addView(visaView);
                    numVisaChildIndex++;
                }
            }
            layoutForm.addView(form);
            child++;
            peopleIndex++;
        }
        Log.d("DEBUG_"+TAG, "Set form member child 3");
        for (i=0; i<order[3]; i++) {
            View form = TourBookActivity.this.getLayoutInflater().inflate(R.layout.layout_book_tour_child, null);
            TextView title = (TextView) form.findViewById(R.id.form_child_title);
            title.setText("Child "+child+" WITHOUT BED");
            gender[peopleIndex] = (Spinner) form.findViewById(R.id.form_child_gender);
            gender[peopleIndex].setAdapter(spinnerAdapter(getResources().getStringArray(R.array.gender_child)));
            country[peopleIndex] = (Spinner) form.findViewById(R.id.form_child_poi);
            country[peopleIndex].setAdapter(spinnerAdapter(city));
            passport[peopleIndex] = (Spinner) form.findViewById(R.id.form_child_passport);
            meal[peopleIndex] = (Spinner) form.findViewById(R.id.form_child_meal);
            meal[peopleIndex].setAdapter(spinnerAdapter(getResources().getStringArray(R.array.type_of_meal)));
            firstName[peopleIndex] = (EditText) form.findViewById(R.id.form_child_first_name);
            lastName[peopleIndex] = (EditText) form.findViewById(R.id.form_child_last_name);
            birthDate[peopleIndex] = (EditText) form.findViewById(R.id.form_child_birt_date);
            passportNumber[peopleIndex] = (EditText) form.findViewById(R.id.form_child_passport_number);
            expirityPassport[peopleIndex] = (EditText) form.findViewById(R.id.form_child_expirity_passport);
            textPassportKurang7Bulan[peopleIndex] = (TextView) form.findViewById(R.id.form_child_passport_7_bulan);
            passportBaru[peopleIndex] = (Switch) form.findViewById(R.id.form_child_pembuatan_passport);
            if (numVisaChild>0) {
                for (int i = 0; i < visaCountryId.length; i++) {
                    layoutVisa = (LinearLayout) form.findViewById(R.id.form_child_layout_visa);
                    View visaView = TourBookActivity.this.getLayoutInflater().inflate(R.layout.layout_tour_book_visa, null);
                    switchVisaChild[numVisaChildIndex] = (Switch) visaView.findViewById(R.id.book_visa_country_name);
                    switchVisaChild[numVisaChildIndex].setText("Pembuatan Visa "+visaCountryName[i].toUpperCase());
                    listVisaChild[numVisaChildIndex] = (Spinner) visaView.findViewById(R.id.book_visa_list);
                    layoutListVisaChild[numVisaChildIndex] = (LinearLayout) visaView.findViewById(R.id.book_visa_layout_visa_list);
                    String[] array = new String[visaNumChild[i]];
                    for (int j=0; j<visaNumChild[i]; j++) {
                        array[j] = visaNameChild[i][j];
                    }
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(TourBookActivity.this,
                            android.R.layout.simple_spinner_item, array);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    listVisaChild[numVisaChildIndex].setAdapter(adapter);
                    layoutVisa.addView(visaView);
                    numVisaChildIndex++;
                }
            }
            layoutForm.addView(form);
            child++;
            peopleIndex++;
        }
        Log.d("DEBUG_"+TAG, "Set form member infant");
        for (i=0; i<order[6]; i++) {
            View form = TourBookActivity.this.getLayoutInflater().inflate(R.layout.layout_book_tour_infant, null);
            TextView title = (TextView) form.findViewById(R.id.form_infant_title);
            title.setText("Infant " + i + 1);
            gender[peopleIndex] = (Spinner) form.findViewById(R.id.form_infant_gender);
            gender[peopleIndex].setAdapter(spinnerAdapter(getResources().getStringArray(R.array.gender_infant)));
            country[peopleIndex] = (Spinner) form.findViewById(R.id.form_infant_poi);
            country[peopleIndex].setAdapter(spinnerAdapter(city));
            meal[peopleIndex] = (Spinner) form.findViewById(R.id.form_infant_meal);
            meal[peopleIndex].setAdapter(spinnerAdapter(getResources().getStringArray(R.array.type_of_meal)));
            passport[peopleIndex] = (Spinner) form.findViewById(R.id.form_infant_passport);
            firstName[peopleIndex] = (EditText) form.findViewById(R.id.form_infant_first_name);
            lastName[peopleIndex] = (EditText) form.findViewById(R.id.form_infant_last_name);
            birthDate[peopleIndex] = (EditText) form.findViewById(R.id.form_infant_birt_date);
            passportNumber[peopleIndex] = (EditText) form.findViewById(R.id.form_infant_passport_number);
            expirityPassport[peopleIndex] = (EditText) form.findViewById(R.id.form_infant_expirity_passport);
            textPassportKurang7Bulan[peopleIndex] = (TextView) form.findViewById(R.id.form_infant_passport_7_bulan);
            passportBaru[peopleIndex] = (Switch) form.findViewById(R.id.form_infant_pembuatan_passport);
            if (numVisaChild>0) {
                for (int i = 0; i < visaCountryId.length; i++) {
                    layoutVisa = (LinearLayout) form.findViewById(R.id.form_infant_layout_visa);
                    View visaView = TourBookActivity.this.getLayoutInflater().inflate(R.layout.layout_tour_book_visa, null);
                    switchVisaChild[numVisaChildIndex] = (Switch) visaView.findViewById(R.id.book_visa_country_name);
                    switchVisaChild[numVisaChildIndex].setText("Pembuatan Visa "+visaCountryName[i].toUpperCase());
                    listVisaChild[numVisaChildIndex] = (Spinner) visaView.findViewById(R.id.book_visa_list);
                    layoutListVisaChild[numVisaChildIndex] = (LinearLayout) visaView.findViewById(R.id.book_visa_layout_visa_list);
                    String[] array = new String[visaNumChild[i]];
                    for (int j=0; j<visaNumChild[i]; j++) {
                        array[j] = visaNameChild[i][j];
                    }
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(TourBookActivity.this,
                            android.R.layout.simple_spinner_item, array);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    listVisaChild[numVisaChildIndex].setAdapter(adapter);
                    layoutVisa.addView(visaView);
                    numVisaChildIndex++;
                }
            }
            layoutForm.addView(form);
            peopleIndex++;
        }

        Log.d("DEBUG_"+TAG, "Set summary layout");
        departures.setText(departure);
        arrivals.setText(arrival);
        days.setText(day+" DAYS");
        flights.setText(flight);
        person.setText(order[5]+" PERSON(S)");
        adult.setText(order[0]+" ADULT TWIN BED");
        if (order[1]==0) {
            childTwinShare.setVisibility(View.GONE);
            childTwinSharePrice.setVisibility(View.GONE);
        }
        else childTwinShare.setText(order[1] + " CHILD TWIN BED");
        if (order[2]==0) {
            childWithExtraBed.setVisibility(View.GONE);
            childWithExtraBedPrice.setVisibility(View.GONE);
        }
        else childWithExtraBed.setText(order[2]+" CHILD WITH EXTRA BED");
        if (order[3]==0) {
            childWithoutBed.setVisibility(View.GONE);
            childWithoutBedPrice.setVisibility(View.GONE);
        }
        else childWithoutBed.setText(order[3]+" CHILD WITHOUT BED");
        if (order[6]==0) {
            infant.setVisibility(View.GONE);
            infantPrice.setVisibility(View.GONE);
        }
        else infant.setText(order[6]+" INFANT");
        sumAdult = order[0]*Double.parseDouble(hargaProduk[0]);
        adultPrice.setText(formatter.RupiahFormat(sumAdult+""));
        sumChildTwin = order[1]*Double.parseDouble(hargaProduk[1]);
        childTwinSharePrice.setText(formatter.RupiahFormat(sumChildTwin+""));
        sumChildExtra = order[2]*Double.parseDouble(hargaProduk[2]);
        childWithExtraBedPrice.setText(formatter.RupiahFormat(sumChildExtra+""));
        sumChildWithout = order[3]*Double.parseDouble(hargaProduk[3]);
        childWithoutBedPrice.setText(formatter.RupiahFormat(sumChildWithout+""));
        singleSupplement.setText(order[4]+" x "+formatter.RupiahFormat(hargaProduk[4]));
        singleSupplementPrice.setText(formatter.RupiahFormat((order[4] * Double.parseDouble(hargaProduk[4])) + ""));
        singleSupplementTitle.setVisibility(View.GONE);
        singleSupplement.setVisibility(View.GONE);
        singleSupplementPrice.setVisibility(View.GONE);
        internationalAirline.setText(order[5]+" x "+formatter.RupiahFormat(hargaProduk[5]));
        sumInternational = order[5]*Double.parseDouble(hargaProduk[5]);
        internationalAirlinePrice.setText(formatter.RupiahFormat(sumInternational+""));
        sumInfant = order[6]*Double.parseDouble(hargaProduk[6]);
        infantPrice.setText(formatter.RupiahFormat(sumInfant+""));

        termShow.loadData("I AGREE TO THE AVIA <font color='blue'>TERM AND SERVICE</font> AND TO " +
                "RECEIVE IMPORTANT COMMUNICATION FROM AVIA TOUR", "text/html; charset=UTF-8", null);
        juduls.setText(judul);

        Log.d("DEBUG_"+TAG, "Initial on checked change");
        for (i=0; i<order[5]; i++) {
            passportBaru[i].setOnCheckedChangeListener(this);
            passport[i].setOnItemSelectedListener(this);
        }
        Log.d("DEBUG_"+TAG, "Initial item selected");
        for (i=0; i<numVisaAdult; i++) {
            listVisaAdult[i].setOnItemSelectedListener(this);
        }
        for (i=0; i<numVisaChild; i++) {
            listVisaChild[i].setOnItemSelectedListener(this);
        }

        Log.d("DEBUG_"+TAG, "Initial on clicked");
        book.setOnClickListener(this);
        formBirthdate.setOnClickListener(this);

        Log.d("DEBUG_"+TAG, "Initial on change");
        for (i=0; i<order[0]; i++) {
            copy[i].setOnCheckedChangeListener(this);
            single[i].setOnCheckedChangeListener(this);
        }
        for (i=0; i<numVisaAdult; i++) {
            switchVisaAdult[i].setOnCheckedChangeListener(this);
        }
        for (i=0; i<numVisaChild; i++) {
            switchVisaChild[i].setOnCheckedChangeListener(this);
        }

        Log.d("DEBUG_"+TAG, "Set contact on touch");
        termShow.setOnTouchListener(this);
        formFirstName.setOnTouchListener(this);
        formLastName.setOnTouchListener(this);
        formBirthdate.setOnTouchListener(this);
        formTelpon.setOnTouchListener(this);
        formEmail.setOnTouchListener(this);
        formAddress.setOnTouchListener(this);
        formCountry.setOnTouchListener(this);
        formPostcode.setOnTouchListener(this);
        formGender.setOnTouchListener(this);
        for (i=0; i<order[5]; i++) {
            gender[i].setOnTouchListener(this);
            country[i].setOnTouchListener(this);
            firstName[i].setOnTouchListener(this);
            lastName[i].setOnTouchListener(this);
            birthDate[i].setOnTouchListener(this);
            passportNumber[i].setOnTouchListener(this);
            expirityPassport[i].setOnTouchListener(this);
            meal[i].setOnTouchListener(this);
        }
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        Log.d("DEBUG_"+TAG, "touched");
        if (event.getAction() == MotionEvent.ACTION_UP) {
            if (v == termShow) {
                TermConditionDialogFragment newFragment = TermConditionDialogFragment.newInstance();
                newFragment.setStyle(DialogFragment.STYLE_NO_TITLE, 0);
                newFragment.show(getFragmentManager(), "dialog");
            }
            else if (v == formFirstName) {
                formFirstName.setBackgroundColor(getResources().getColor(R.color.filled));
            }
            else if (v == formGender) {
                formGender.setBackgroundColor(getResources().getColor(R.color.filled));
            }
            else if (v == formCountry) {
                formCountry.setBackgroundColor(getResources().getColor(R.color.filled));
            }
            else if (v == formLastName) {
                formLastName.setBackgroundColor(getResources().getColor(R.color.filled));
            }
            else if (v == formBirthdate) {
                formBirthdate.setBackgroundColor(getResources().getColor(R.color.filled));
            }
            else if (v == formTelpon) {
                formTelpon.setBackgroundColor(getResources().getColor(R.color.filled));
            }
            else if (v == formEmail) {
                formEmail.setBackgroundColor(getResources().getColor(R.color.filled));
            }
            else if (v == formAddress) {
                formAddress.setBackgroundColor(getResources().getColor(R.color.filled));
            }
            else if (v == formPostcode) {
                formPostcode.setBackgroundColor(getResources().getColor(R.color.filled));
            }
            else {
                for (numView = 0; numView < order[5]; numView++) {
                    if (v == gender[numView]) {
                        gender[numView].setBackgroundColor(getResources().getColor(R.color.filled));
                    }
                    else if (v == country[numView]) {
                        country[numView].setBackgroundColor(getResources().getColor(R.color.filled));
                    }
                    else if (v == birthDate[numView]) {
                        yangdiset = numView;
                        birthDate[numView].setBackgroundColor(getResources().getColor(R.color.filled));
                        View view = TourBookActivity.this.getCurrentFocus();
                        if (view != null) {
                            InputMethodManager imm = (InputMethodManager) TourBookActivity.this.getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                        }
                        mDateSetListener = new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                monthOfYear++;
                                String tanggal;
                                if (dayOfMonth<10) tanggal = "0"+dayOfMonth+"-";
                                else tanggal = dayOfMonth+"-";
                                if (monthOfYear<10) tanggal += "0"+monthOfYear+"-";
                                else tanggal += monthOfYear+"-";
                                tanggal += year;
                                birthDate[yangdiset].setText(tanggal);
                            }
                        };

                        Calendar calender = Calendar.getInstance();
                        int year = calender.get(Calendar.YEAR);
                        int month = calender.get(Calendar.MONTH);

                        if (numView < order[0]) {
                            datePicker = new DatePickerDialog(TourBookActivity.this, android.R.style.Theme_Holo, mDateSetListener, year - 26, month, 1);
                            calender.add(Calendar.YEAR, -12);
                            datePicker.getDatePicker().setMaxDate(calender.getTimeInMillis());
                            calender.add(Calendar.YEAR, -88);
                            datePicker.getDatePicker().setMinDate(calender.getTimeInMillis());

                        } else if (numView < (order[1] + order[2] + order[3] + order[0])) {
                            datePicker = new DatePickerDialog(TourBookActivity.this, android.R.style.Theme_Holo, mDateSetListener, year - 10, month, 1);
                            calender.add(Calendar.YEAR, -2);
                            calender.add(Calendar.DATE, -1);
                            datePicker.getDatePicker().setMaxDate(calender.getTimeInMillis());
                            calender.add(Calendar.YEAR, -10);
                            datePicker.getDatePicker().setMinDate(calender.getTimeInMillis());
                        } else {
                            datePicker = new DatePickerDialog(TourBookActivity.this, android.R.style.Theme_Holo, mDateSetListener, year - 1, month, 1);
                            datePicker.getDatePicker().setMaxDate(calender.getTimeInMillis());
                            calender.add(Calendar.YEAR, -2);
                            calender.add(Calendar.DATE, +1);
                            datePicker.getDatePicker().setMinDate(calender.getTimeInMillis());
                        }
                        datePicker.show();
                    }
                    else if (v == expirityPassport[numView]) {
                        expirityPassport[numView].setBackgroundColor(getResources().getColor(R.color.filled));
                        yangdiset = numView;
                        View view = TourBookActivity.this.getCurrentFocus();
                        if (view != null) {
                            InputMethodManager imm = (InputMethodManager) TourBookActivity.this.getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                        }

                        mDateSetListener = new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                monthOfYear++;
                                String tanggal;
                                if (dayOfMonth<10) tanggal = "0"+dayOfMonth+"-";
                                else tanggal = dayOfMonth+"-";
                                if (monthOfYear<10) tanggal += "0"+monthOfYear+"-";
                                else tanggal += monthOfYear+"-";
                                tanggal += year;
                                expirityPassport[yangdiset].setText(tanggal);

                                Calendar calender = Calendar.getInstance();
                                int yearNow = calender.get(Calendar.YEAR);
                                int monthNow = calender.get(Calendar.MONTH);
                                if (year == yearNow) {
                                    if ((monthOfYear-monthNow)<8) {
                                        textPassportKurang7Bulan[yangdiset].setVisibility(View.VISIBLE);
                                    }
                                    else {
                                        textPassportKurang7Bulan[yangdiset].setVisibility(View.GONE);
                                    }
                                }
                                else if (year == (yearNow+1)) {
                                    if (((monthOfYear+12)-monthNow)<8) {
                                        textPassportKurang7Bulan[yangdiset].setVisibility(View.VISIBLE);
                                    }
                                    else {
                                        textPassportKurang7Bulan[yangdiset].setVisibility(View.GONE);
                                    }
                                }
                                else {
                                    textPassportKurang7Bulan[yangdiset].setVisibility(View.GONE);
                                }
                            }
                        };
                        Calendar calender = Calendar.getInstance();
                        int year = calender.get(Calendar.YEAR);
                        int month = calender.get(Calendar.MONTH);
                        datePicker = new DatePickerDialog(TourBookActivity.this, android.R.style.Theme_Holo, mDateSetListener, year + 10, month, 1);

                        datePicker.getDatePicker().setMinDate(calender.getTimeInMillis());
                        calender.add(Calendar.YEAR, 50);
                        datePicker.getDatePicker().setMaxDate(calender.getTimeInMillis());

                        datePicker.show();
                    }
                    else if (v == firstName[numView]) {
                        firstName[numView].setBackgroundColor(getResources().getColor(R.color.filled));
                    }
                    else if (v == lastName[numView]) {
                        lastName[numView].setBackgroundColor(getResources().getColor(R.color.filled));
                    }
                    else if (v == passportNumber[numView]) {
                        passportNumber[numView].setBackgroundColor(getResources().getColor(R.color.filled));
                    }
                    else if (meal[numView].getSelectedItemPosition()!=0) {
                            meal[numView].setBackgroundColor(getResources().getColor(R.color.filled));
                    }
                }
            }
        }
        return false;
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        Log.d("DEBUG_"+TAG, "On item selected");
        /*for (i=0; i<numVisaAdult; i++) {
            if (view == listVisaAdult[i]) {
                Log.d("DEBUG_"+TAG, "List visa adult selected");
                updateListVisa();
                return;
            }
        }
        for (i=0; i<numVisaChild; i++) {
            if (view == listVisaChild[i]) {
                Log.d("DEBUG_"+TAG, "List visa child selected");
                updateListVisa();
                return;
            }
        }*/
        updateListVisa();
        //for (i=0; i<order[5]; i++) {
        //    if (view == passport[i]) {
                Log.d("DEBUG_"+TAG, "List passport selected");
                numPassportAdult[0] = 0;
                numPassportAdult[1] = 0;
                numPassportAdult[2] = 0;
                numPassportAdult[3] = 0;
                numPassportAdult[4] = 0;
                numPassportChild[0] = 0;
                numPassportChild[1] = 0;
                numPassportChild[2] = 0;
                for (int j = 0; j < order[5]; j++) {
                    if (passportBaru[j].isChecked()) {
                        if (j < order[0]) numPassportAdult[passport[j].getSelectedItemPosition()]++;
                        else numPassportChild[passport[j].getSelectedItemPosition()]++;
                    }
                }
                if (numPassportAdult[0] == 0) {
                    passportBaru4.setVisibility(View.GONE);
                    passportBaru41.setVisibility(View.GONE);
                    passportBaru42.setVisibility(View.GONE);
                } else {
                    passportBaru4.setVisibility(View.VISIBLE);
                    passportBaru41.setVisibility(View.VISIBLE);
                    passportBaru42.setVisibility(View.VISIBLE);
                    passportBaru41.setText(numPassportAdult[0] + " x " + formatter.RupiahFormat(pricePassportAdult[0]+""));
                    passportBaru42.setText(formatter.RupiahFormat(numPassportAdult[0] * pricePassportAdult[0] + ""));
                }
                if (numPassportAdult[1] == 0) {
                    passportBaru14.setVisibility(View.GONE);
                    passportBaru141.setVisibility(View.GONE);
                    passportBaru142.setVisibility(View.GONE);
                } else {
                    passportBaru14.setVisibility(View.VISIBLE);
                    passportBaru141.setVisibility(View.VISIBLE);
                    passportBaru142.setVisibility(View.VISIBLE);
                    passportBaru141.setText(numPassportAdult[1] + " x " + formatter.RupiahFormat(pricePassportAdult[1]+""));
                    passportBaru142.setText(formatter.RupiahFormat(numPassportAdult[1] * pricePassportAdult[1] + ""));
                }
                if (numPassportAdult[2] == 0) {
                    passportBaru4Daerah.setVisibility(View.GONE);
                    passportBaru4Daerah1.setVisibility(View.GONE);
                    passportBaru4Daerah2.setVisibility(View.GONE);
                } else {
                    passportBaru4Daerah.setVisibility(View.VISIBLE);
                    passportBaru4Daerah1.setVisibility(View.VISIBLE);
                    passportBaru4Daerah2.setVisibility(View.VISIBLE);
                    passportBaru4Daerah1.setText(numPassportAdult[2] + " x " + formatter.RupiahFormat(pricePassportAdult[2]+""));
                    passportBaru4Daerah2.setText(formatter.RupiahFormat(numPassportAdult[2] * pricePassportAdult[2] + ""));
                }
                if (numPassportAdult[3] == 0) {
                    passportBaru14Daerah.setVisibility(View.GONE);
                    passportBaru14Daerah1.setVisibility(View.GONE);
                    passportBaru14Daerah2.setVisibility(View.GONE);
                } else {
                    passportBaru14Daerah.setVisibility(View.VISIBLE);
                    passportBaru14Daerah1.setVisibility(View.VISIBLE);
                    passportBaru14Daerah2.setVisibility(View.VISIBLE);
                    passportBaru14Daerah1.setText(numPassportAdult[3] + " x " + formatter.RupiahFormat(pricePassportAdult[3]+""));
                    passportBaru14Daerah2.setText(formatter.RupiahFormat(numPassportAdult[3] * pricePassportAdult[3] + ""));
                }
                if (numPassportAdult[4] == 0) {
                    passportBaruEpaspor.setVisibility(View.GONE);
                    passportBaruEpaspor1.setVisibility(View.GONE);
                    passportBaruEpaspor2.setVisibility(View.GONE);
                } else {
                    passportBaruEpaspor.setVisibility(View.VISIBLE);
                    passportBaruEpaspor1.setVisibility(View.VISIBLE);
                    passportBaruEpaspor2.setVisibility(View.VISIBLE);
                    passportBaruEpaspor1.setText(numPassportAdult[4] + " x " + formatter.RupiahFormat(pricePassportAdult[4]+""));
                    passportBaruEpaspor2.setText(formatter.RupiahFormat(numPassportAdult[4] * pricePassportAdult[4] + ""));
                }
                if (numPassportChild[0] == 0) {
                    passportBaru4Child.setVisibility(View.GONE);
                    passportBaru41Child.setVisibility(View.GONE);
                    passportBaru42Child.setVisibility(View.GONE);
                } else {
                    passportBaru4Child.setVisibility(View.VISIBLE);
                    passportBaru41Child.setVisibility(View.VISIBLE);
                    passportBaru42Child.setVisibility(View.VISIBLE);
                    passportBaru41Child.setText(numPassportChild[0] + " x " + formatter.RupiahFormat(pricePassportChild[0]+""));
                    passportBaru42Child.setText(formatter.RupiahFormat(numPassportChild[0] * pricePassportChild[0] + ""));
                }
                if (numPassportChild[1] == 0) {
                    passportBaru14Child.setVisibility(View.GONE);
                    passportBaru141Child.setVisibility(View.GONE);
                    passportBaru142Child.setVisibility(View.GONE);
                } else {
                    passportBaru14Child.setVisibility(View.VISIBLE);
                    passportBaru141Child.setVisibility(View.VISIBLE);
                    passportBaru142Child.setVisibility(View.VISIBLE);
                    passportBaru141Child.setText(numPassportChild[1] + " x " + formatter.RupiahFormat(pricePassportChild[1]+""));
                    passportBaru142Child.setText(formatter.RupiahFormat(numPassportChild[1] * pricePassportChild[1] + ""));
                }
                if (numPassportChild[2] == 0) {
                    passportBaruEpasporChild.setVisibility(View.GONE);
                    passportBaruEpaspor1Child.setVisibility(View.GONE);
                    passportBaruEpaspor2Child.setVisibility(View.GONE);
                } else {
                    passportBaruEpasporChild.setVisibility(View.VISIBLE);
                    passportBaruEpaspor1Child.setVisibility(View.VISIBLE);
                    passportBaruEpaspor2Child.setVisibility(View.VISIBLE);
                    passportBaruEpaspor1Child.setText(numPassportChild[2] + " x " + formatter.RupiahFormat(pricePassportChild[2] + ""));
                    passportBaruEpaspor2Child.setText(formatter.RupiahFormat(numPassportChild[2] * pricePassportChild[2] + ""));
                }
        updateSubTotal();
        //        return;
        //    }
       // }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        Log.d("DEBUG_"+TAG, "checked / unchecked");
        for (i=0; i<numVisaAdult; i++) {
            if (switchVisaAdult[i] == buttonView) {
                for (int i = 0; i<numVisaAdult; i++) {
                    if (switchVisaAdult[i].isChecked()) {
                        layoutListVisaAdult[i].setVisibility(View.VISIBLE);
                    }
                    else {
                        layoutListVisaAdult[i].setVisibility(View.GONE);
                    }
                }
                updateListVisa();
                return;
            }
        }
        for (i=0; i<numVisaChild; i++) {
            if (buttonView == switchVisaChild[i]) {
                for (int i = 0; i < numVisaChild; i++) {
                    if (switchVisaChild[i].isChecked()) {
                        layoutListVisaChild[i].setVisibility(View.VISIBLE);
                    } else {
                        layoutListVisaChild[i].setVisibility(View.GONE);
                    }
                }
                updateListVisa();
                return;
            }
        }
        for (i=0; i<order[5]; i++) {
            if (buttonView == passportBaru[i]) {
                Log.d("DEBUG_"+TAG, "List passport selected");
                numPassportAdult[0] = 0;
                numPassportAdult[1] = 0;
                numPassportAdult[2] = 0;
                numPassportAdult[3] = 0;
                numPassportAdult[4] = 0;
                numPassportChild[0] = 0;
                numPassportChild[1] = 0;
                numPassportChild[2] = 0;
                for (int j=0; j<order[5]; j++) {
                    if (passportBaru[j].isChecked()) {
                        if (passportBaru[j].isChecked()) {
                            if (j < order[0]) numPassportAdult[passport[j].getSelectedItemPosition()]++;
                            else numPassportChild[passport[j].getSelectedItemPosition()]++;
                        }
                        passport[j].setVisibility(View.VISIBLE);
                        passportNumber[j].setVisibility(View.GONE);
                        country[j].setVisibility(View.GONE);
                        expirityPassport[j].setVisibility(View.GONE);
                        textPassportKurang7Bulan[j].setVisibility(View.GONE);
                    }
                    else {
                        passport[j].setVisibility(View.GONE);
                        passportNumber[j].setVisibility(View.VISIBLE);
                        country[j].setVisibility(View.VISIBLE);
                        expirityPassport[j].setVisibility(View.VISIBLE);
                        if (expirityPassport[j].getText().toString().trim().equals("")) {
                            textPassportKurang7Bulan[j].setVisibility(View.GONE);
                        }
                        else {
                            String tanggal = expirityPassport[j].getText().toString().trim();
                            String tbt[] = tanggal.split("-");
                            int monthOfYear = Integer.valueOf(tbt[1]);
                            int year = Integer.valueOf(tbt[2]);
                            Calendar calender = Calendar.getInstance();
                            int yearNow = calender.get(Calendar.YEAR);
                            int monthNow = calender.get(Calendar.MONTH);

                            if (year == yearNow) {
                                if ((monthOfYear-monthNow)<8) {
                                    textPassportKurang7Bulan[j].setVisibility(View.VISIBLE);
                                }
                                else {
                                    textPassportKurang7Bulan[j].setVisibility(View.GONE);
                                }
                            }
                            else if (year == (yearNow+1)) {
                                if (((monthOfYear+12)-monthNow)<8) {
                                    textPassportKurang7Bulan[j].setVisibility(View.VISIBLE);
                                }
                                else {
                                    textPassportKurang7Bulan[j].setVisibility(View.GONE);
                                }
                            }
                            else {
                                textPassportKurang7Bulan[j].setVisibility(View.GONE);
                            }
                        }
                    }
                }
                if (numPassportAdult[0] == 0) {
                    passportBaru4.setVisibility(View.GONE);
                    passportBaru41.setVisibility(View.GONE);
                    passportBaru42.setVisibility(View.GONE);
                }
                else {
                    passportBaru4.setVisibility(View.VISIBLE);
                    passportBaru41.setVisibility(View.VISIBLE);
                    passportBaru42.setVisibility(View.VISIBLE);
                    passportBaru41.setText(numPassportAdult[0] + " x " + formatter.RupiahFormat(pricePassportAdult[0]+""));
                    passportBaru42.setText(formatter.RupiahFormat(numPassportAdult[0] * pricePassportAdult[0] + ""));
                }
                if (numPassportAdult[1] == 0) {
                    passportBaru14.setVisibility(View.GONE);
                    passportBaru141.setVisibility(View.GONE);
                    passportBaru142.setVisibility(View.GONE);
                }
                else {
                    passportBaru14.setVisibility(View.VISIBLE);
                    passportBaru141.setVisibility(View.VISIBLE);
                    passportBaru142.setVisibility(View.VISIBLE);
                    passportBaru141.setText(numPassportAdult[1] + " x " + formatter.RupiahFormat(pricePassportAdult[1]+""));
                    passportBaru142.setText(formatter.RupiahFormat(numPassportAdult[1] * pricePassportAdult[1] + ""));
                }
                if (numPassportAdult[2] == 0) {
                    passportBaru4Daerah.setVisibility(View.GONE);
                    passportBaru4Daerah1.setVisibility(View.GONE);
                    passportBaru4Daerah2.setVisibility(View.GONE);
                }
                else {
                    passportBaru4Daerah.setVisibility(View.VISIBLE);
                    passportBaru4Daerah1.setVisibility(View.VISIBLE);
                    passportBaru4Daerah2.setVisibility(View.VISIBLE);
                    passportBaru4Daerah1.setText(numPassportAdult[2] + " x " + formatter.RupiahFormat(pricePassportAdult[2]+""));
                    passportBaru4Daerah2.setText(formatter.RupiahFormat(numPassportAdult[2] * pricePassportAdult[2] + ""));
                }
                if (numPassportAdult[3] == 0) {
                    passportBaru14Daerah.setVisibility(View.GONE);
                    passportBaru14Daerah1.setVisibility(View.GONE);
                    passportBaru14Daerah2.setVisibility(View.GONE);
                }
                else {
                    passportBaru14Daerah.setVisibility(View.VISIBLE);
                    passportBaru14Daerah1.setVisibility(View.VISIBLE);
                    passportBaru14Daerah2.setVisibility(View.VISIBLE);
                    passportBaru14Daerah1.setText(numPassportAdult[3] + " x " + formatter.RupiahFormat(pricePassportAdult[3]+""));
                    passportBaru14Daerah2.setText(formatter.RupiahFormat(numPassportAdult[3] * pricePassportAdult[3] + ""));
                }
                if (numPassportAdult[4] == 0) {
                    passportBaruEpaspor.setVisibility(View.GONE);
                    passportBaruEpaspor1.setVisibility(View.GONE);
                    passportBaruEpaspor2.setVisibility(View.GONE);
                }
                else {
                    passportBaruEpaspor.setVisibility(View.VISIBLE);
                    passportBaruEpaspor1.setVisibility(View.VISIBLE);
                    passportBaruEpaspor2.setVisibility(View.VISIBLE);
                    passportBaruEpaspor1.setText(numPassportAdult[4] + " x " + formatter.RupiahFormat(pricePassportAdult[4]+""));
                    passportBaruEpaspor2.setText(formatter.RupiahFormat(numPassportAdult[4] * pricePassportAdult[4] + ""));
                }
                if (numPassportChild[0] == 0) {
                    passportBaru4Child.setVisibility(View.GONE);
                    passportBaru41Child.setVisibility(View.GONE);
                    passportBaru42Child.setVisibility(View.GONE);
                }
                else {
                    passportBaru4Child.setVisibility(View.VISIBLE);
                    passportBaru41Child.setVisibility(View.VISIBLE);
                    passportBaru42Child.setVisibility(View.VISIBLE);
                    passportBaru41Child.setText(numPassportChild[0] + " x " + formatter.RupiahFormat(pricePassportChild[0]+""));
                    passportBaru42Child.setText(formatter.RupiahFormat(numPassportChild[0] * pricePassportChild[0] + ""));
                }
                if (numPassportChild[1] == 0) {
                    passportBaru14Child.setVisibility(View.GONE);
                    passportBaru141Child.setVisibility(View.GONE);
                    passportBaru142Child.setVisibility(View.GONE);
                }
                else {
                    passportBaru14Child.setVisibility(View.VISIBLE);
                    passportBaru141Child.setVisibility(View.VISIBLE);
                    passportBaru142Child.setVisibility(View.VISIBLE);
                    passportBaru141Child.setText(numPassportChild[1] + " x " + formatter.RupiahFormat(pricePassportChild[1]+""));
                    passportBaru142Child.setText(formatter.RupiahFormat(numPassportChild[1] * pricePassportChild[1] + ""));
                }
                if (numPassportChild[2] == 0) {
                    passportBaruEpasporChild.setVisibility(View.GONE);
                    passportBaruEpaspor1Child.setVisibility(View.GONE);
                    passportBaruEpaspor2Child.setVisibility(View.GONE);
                }
                else {
                    passportBaruEpasporChild.setVisibility(View.VISIBLE);
                    passportBaruEpaspor1Child.setVisibility(View.VISIBLE);
                    passportBaruEpaspor2Child.setVisibility(View.VISIBLE);
                    passportBaruEpaspor1Child.setText(numPassportChild[2] + " x " + formatter.RupiahFormat(pricePassportChild[2]+""));
                    passportBaruEpaspor2Child.setText(formatter.RupiahFormat(numPassportChild[2] * pricePassportChild[2] + ""));
                }
                updateSubTotal();
                return;
            }
        }
        for (i=0; i<order[0]; i++) {
            if (buttonView == copy[i]) {
                boolean hanyaSatu = false;
                for (int j=0; j<order[0]; j++) {
                    if (copy[j].isChecked()) {
                        if (!hanyaSatu) {
                            hanyaSatu = true;
                            gender[j].setSelection(formGender.getSelectedItemPosition());
                            gender[j].setBackgroundColor(getResources().getColor(R.color.filled));
                            firstName[j].setText(formFirstName.getText().toString());
                            firstName[j].setBackgroundColor(getResources().getColor(R.color.filled));
                            lastName[j].setText(formLastName.getText().toString());
                            lastName[j].setBackgroundColor(getResources().getColor(R.color.filled));
                            birthDate[j].setText(formBirthdate.getText().toString());
                            birthDate[j].setBackgroundColor(getResources().getColor(R.color.filled));
                        }
                        else {
                            copy[j].setChecked(false);
                        }
                    }
                    else {
                        gender[j].setSelection(0);
                        firstName[j].setText("");
                        lastName[j].setText("");
                        birthDate[j].setText("");
                    }
                }
                return;
            }
            else if (buttonView == single[i]) {
                order[4] = 0;
                for (int j=0; j<order[0]; j++) {
                    if (single[j].isChecked()) {
                        order[4]++;
                    }
                }
                sumSingle = order[4] * Double.parseDouble(hargaProduk[4]);
                if (order[4]==0) {
                    singleSupplementTitle.setVisibility(View.GONE);
                    singleSupplement.setVisibility(View.GONE);
                    singleSupplementPrice.setVisibility(View.GONE);
                }
                else {
                    singleSupplementTitle.setVisibility(View.VISIBLE);
                    singleSupplement.setVisibility(View.VISIBLE);
                    singleSupplementPrice.setVisibility(View.VISIBLE);
                    singleSupplement.setText(order[4] + " x " + formatter.RupiahFormat(hargaProduk[4]));
                    singleSupplementPrice.setText(formatter.RupiahFormat(sumSingle + ""));
                }
                updateSubTotal();
                return;
            }
        }
    }

    @Override
    public void onClick(View v) {
        Log.d("DEBUG_"+TAG, "clicked");
        if (formBirthdate == v) {
            mDateSetListener = new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                    monthOfYear++;
                    String tanggal;
                    if (dayOfMonth<10) tanggal = "0"+dayOfMonth+"-";
                    else tanggal = dayOfMonth+"-";
                    if (monthOfYear<10) tanggal += "0"+monthOfYear+"-";
                    else tanggal += monthOfYear+"-";
                    tanggal += year;
                    formBirthdate.setText(tanggal);
                }
            };
            datePicker = new DatePickerDialog(TourBookActivity.this,android.R.style.Theme_Holo,mDateSetListener,1990, 0, 1);
            datePicker.show();
        }
        else if (v == book) {
            Log.d("DEBUG_"+TAG, "Check term");
            if (term.isChecked()) {
                Log.d("DEBUG_"+TAG, "Check data");
                boolean lanjut=true;
                if (formGender.getSelectedItemPosition() == 0) {
                    formGender.setBackgroundColor(getResources().getColor(R.color.not_filled));
                    lanjut = false;
                }
                if (formCountry.getSelectedItemPosition() == 0) {
                    formCountry.setBackgroundColor(getResources().getColor(R.color.not_filled));
                    lanjut = false;
                }
                if (formFirstName.getText().toString().trim().equals("")) {
                    formFirstName.setBackgroundColor(getResources().getColor(R.color.not_filled));
                    lanjut = false;
                }
                if (formLastName.getText().toString().trim().equals("")) {
                    formLastName.setBackgroundColor(getResources().getColor(R.color.not_filled));
                    lanjut = false;
                }
                if (formBirthdate.getText().toString().trim().equals("")) {
                    formBirthdate.setBackgroundColor(getResources().getColor(R.color.not_filled));
                    lanjut = false;
                }
                if (formTelpon.getText().toString().trim().equals("")) {
                    formTelpon.setBackgroundColor(getResources().getColor(R.color.not_filled));
                    lanjut = false;
                }
                if (formEmail.getText().toString().trim().equals("")) {
                    formEmail.setBackgroundColor(getResources().getColor(R.color.not_filled));
                    lanjut = false;
                }
                if (formAddress.getText().toString().trim().equals("")) {
                    formAddress.setBackgroundColor(getResources().getColor(R.color.not_filled));
                    lanjut = false;
                }
                for (i=0; i<order[5]; i++) {
                    if (gender[i].getSelectedItemPosition() == 0) {
                        lanjut = false;
                        gender[i].setBackgroundColor(getResources().getColor(R.color.not_filled));
                    }
                    if (firstName[i].getText().toString().equals("")) {
                        lanjut = false;
                        firstName[i].setBackgroundColor(getResources().getColor(R.color.not_filled));
                    }
                    if (lastName[i].getText().toString().equals("")) {
                        lanjut = false;
                        lastName[i].setBackgroundColor(getResources().getColor(R.color.not_filled));
                    }
                    if (birthDate[i].getText().toString().equals("")) {
                        lanjut = false;
                        birthDate[i].setBackgroundColor(getResources().getColor(R.color.not_filled));
                    }
                    if (!(passportBaru[i].isChecked())) {
                        if (passportNumber[i].getText().toString().equals("")) {
                            lanjut = false;
                            passportNumber[i].setBackgroundColor(getResources().getColor(R.color.not_filled));
                        }
                        if (country[i].getSelectedItemPosition() == 0) {
                            lanjut = false;
                            country[i].setBackgroundColor(getResources().getColor(R.color.not_filled));
                        }
                        if (expirityPassport[i].getText().toString().equals("")) {
                            lanjut = false;
                            expirityPassport[i].setBackgroundColor(getResources().getColor(R.color.not_filled));
                        }
                    }
                }
                if (lanjut) {
                    Log.d("DEBUG_"+TAG, "Data lengkap");
                    if (InternetConnection.isAvailable(TourBookActivity.this)) {
                        pd = ProgressDialog.show(TourBookActivity.this, "", "Loading...", true);
                        String url = BuildConfig.URL_BASE + "/?method=booking&key=" + BuildConfig.KEY;
                        CustomRequest req = new CustomRequest(Request.Method.POST, url, null, new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                Log.d("DEBUG_"+TAG, "Data Response : "+response.toString());
                                pd.hide();
                                pd.dismiss();
                                String respon = response.toString();
                                try {
                                    JSONObject data = response.getJSONObject("data");

                                    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                                    DBDataSource dds = new DBDataSource(TourBookActivity.this);
                                    dds.open();
                                    dds.insertBooking(respon, sdf.format(new Date()));
                                    dds.close();

                                    TourReservedActivity.dataResponse = data;
                                    startActivity(new Intent(TourBookActivity.this, TourReservedActivity.class));
                                    finish();
                                } catch (JSONException je) {
                                    Log.d("DEBUG_"+TAG, "JSON Error : "+je.toString());
                                    Toast.makeText(TourBookActivity.this, "JSON Format : We will fix it soon.", Toast.LENGTH_SHORT).show();
                                }
                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Log.d("DEBUG_"+TAG, "Error : "+error.toString());
                                pd.hide();
                                pd.dismiss();
                                if (error.toString().equals("com.android.volley.ServerError")) {
                                    Toast.makeText(TourBookActivity.this, "Server : Please make sure you have good internet access.", Toast.LENGTH_SHORT).show();
                                }
                                else if (error.toString().equals("com.android.volley.TimeOutError")) {
                                    Toast.makeText(TourBookActivity.this, "Time Out : Please make sure you have good internet access.", Toast.LENGTH_SHORT).show();
                                }
                                else if (error.toString().equals("com.android.volley.ParseError")) {
                                    Toast.makeText(TourBookActivity.this, "Parse Error : We will fix it soon.", Toast.LENGTH_SHORT).show();
                                }
                            }
                        }) {
                            @Override
                            protected Map<String, String> getParams() throws AuthFailureError {
                                Log.d("DEBUG_"+TAG, "Kirim post data");
                                Map<String, String> data = new HashMap<String, String>();
                                data.put("tpid", tourId);
                                data.put("contact_title", formGender.getSelectedItemPosition() + "");
                                data.put("contact_first_name", formFirstName.getText().toString());
                                data.put("contact_last_name", formLastName.getText().toString());
                                data.put("contact_middle_name", "");
                                data.put("contact_email", formEmail.getText().toString());
                                data.put("contact_dob", formBirthdate.getText().toString());
                                data.put("contact_phone", formTelpon.getText().toString());
                                data.put("contact_address", formAddress.getText().toString());
                                data.put("contact_postcode", formPostcode.getText().toString());
                                data.put("contact_country", cityId[formCountry.getSelectedItemPosition()]);
                                int member = 0;
                                for (int i = 0; i < order[0]; i++) {
                                    data.put("passenger_title[" + (i + 1) + "]", gender[member].getSelectedItemPosition() + "");
                                    data.put("passenger_first_name[" + (i + 1) + "]", firstName[member].getText().toString());
                                    data.put("passenger_last_name[" + (i + 1) + "]", lastName[member].getText().toString());
                                    data.put("passenger_middle_name[" + (i + 1) + "]", "");
                                    data.put("passenger_dob[" + (i + 1) + "]", birthDate[member].getText().toString());
                                    int makanan = meal[member].getSelectedItemPosition();
                                    if (makanan == 3) makanan = 12;
                                    else if (makanan == 0) makanan = 1;
                                    data.put("passenger_meal[" + (i + 1) + "]", makanan + "");
                                    if (single[member].isChecked())
                                        data.put("passenger_room_type[" + (i + 1) + "]", roomId[4]);
                                    else
                                        data.put("passenger_room_type[" + (i + 1) + "]", roomId[0]);
                                    int ji = i * visaCountryId.length;
                                    for (int j = 0; j < visaCountryId.length; j++) {
                                        if (switchVisaAdult[ji + j].isChecked()) {
                                            data.put("passenger_visa[" + (i + 1) + "][" + visaCountryId[j] + "]", "1");
                                            data.put("passenger_visa_id[" + (i + 1) + "][" + visaCountryId[j] + "]", visaIdAdult[j]
                                                    [listVisaAdult[ji + j].getSelectedItemPosition()]);
                                        }
                                    }
                                    if (passportBaru[member].isChecked()) {
                                        data.put("passenger_passport_select[" + (i + 1) + "]", pricePassportAdultId[passport[member]
                                                .getSelectedItemPosition()]);
                                        data.put("passenger_passport[" + (i + 1) + "]", "1");
                                        data.put("passenger_passport_number[" + (i + 1) + "]", "");
                                        data.put("passenger_poi[" + (i + 1) + "]", "");
                                        data.put("passenger_passport_expirity[" + (i + 1) + "]", "");
                                    } else {
                                        data.put("passenger_passport_select[" + (i + 1) + "]", "0");
                                        data.put("passenger_passport_number[" + (i + 1) + "]", passportNumber[member].getText().toString());
                                        data.put("passenger_poi[" + (i + 1) + "]", cityId[country[member].getSelectedItemPosition()]);
                                        data.put("passenger_passport_expirity[" + (i + 1) + "]", expirityPassport[member].getText().toString());
                                    }
                                    member++;
                                }
                                int numChild = 1;
                                int numVisa = 0;
                                for (int i = 0; i < order[1]; i++) {
                                    data.put("child_title[" + numChild + "]", gender[member].getSelectedItemPosition() + "");
                                    data.put("child_first_name[" + numChild + "]", firstName[member].getText().toString());
                                    data.put("child_last_name[" + numChild + "]", lastName[member].getText().toString());
                                    data.put("child_middle_name[" + numChild + "]", "");
                                    data.put("child_dob[" + numChild + "]", birthDate[member].getText().toString());
                                    int makanan = meal[member].getSelectedItemPosition();
                                    if (makanan == 3) makanan = 12;
                                    else if (makanan == 0) makanan = 1;
                                    data.put("child_meal[" + numChild + "]", makanan + "");
                                    data.put("child_room_type[" + numChild + "]", roomId[1]);
                                    for (int j = 0; j < visaCountryId.length; j++) {
                                        if (switchVisaChild[numVisa].isChecked()) {
                                            data.put("child_visa[" + numChild + "][" + visaCountryId[j] + "]", "1");
                                            data.put("child_visa_id[" + numChild + "][" + visaCountryId[j] + "]",
                                                    visaIdChild[j][listVisaChild[numVisa].getSelectedItemPosition()]);
                                        }
                                        numVisa++;
                                    }
                                    if (passportBaru[member].isChecked()) {
                                        data.put("child_passport_select[" + numChild + "]", pricePassportChildId[passport[member]
                                                .getSelectedItemPosition()]);
                                        data.put("child_passport[" + numChild + "]", "1");
                                        data.put("child_passport_number[" + numChild + "]", "");
                                        data.put("child_poi[" + numChild + "]", "");
                                        data.put("child_passport_expirity[" + numChild + "]", "");
                                    } else {
                                        data.put("child_passport_select[" + numChild + "]", "0");
                                        data.put("child_passport_number[" + numChild + "]", passportNumber[member].getText().toString());
                                        data.put("child_poi[" + numChild + "]", cityId[country[member].getSelectedItemPosition()]);
                                        data.put("child_passport_expirity[" + numChild + "]", expirityPassport[member].getText().toString());
                                    }
                                    member++;
                                    numChild++;
                                }
                                for (int i = 0; i < order[2]; i++) {
                                    data.put("child_title[" + numChild + "]", gender[member].getSelectedItemPosition() + "");
                                    data.put("child_first_name[" + numChild + "]", firstName[member].getText().toString());
                                    data.put("child_last_name[" + numChild + "]", lastName[member].getText().toString());
                                    data.put("child_middle_name[" + numChild + "]", "");
                                    data.put("child_dob[" + numChild + "]", birthDate[member].getText().toString());
                                    int makanan = meal[member].getSelectedItemPosition() + 1;
                                    if (makanan == 3) makanan = 12;
                                    else if (makanan == 0) makanan = 1;
                                    data.put("child_meal[" + numChild + "]", makanan + "");
                                    data.put("child_room_type[" + numChild + "]", roomId[2]);
                                    for (int j = 0; j < visaCountryId.length; j++) {
                                        if (switchVisaChild[numVisa].isChecked()) {
                                            data.put("child_visa[" + numChild + "][" + visaCountryId[j] + "]", "1");
                                            data.put("child_visa_id[" + numChild + "][" + visaCountryId[j] + "]",
                                                    visaIdChild[j][listVisaChild[numVisa].getSelectedItemPosition()]);
                                        }
                                        numVisa++;
                                    }
                                    if (passportBaru[member].isChecked()) {
                                        data.put("child_passport_select[" + numChild + "]", pricePassportChildId[passport[member]
                                                .getSelectedItemPosition()]);
                                        data.put("child_passport[" + numChild + "]", "1");
                                        data.put("child_poi[" + numChild + "]", "");
                                        data.put("child_passport_expirity[" + numChild + "]", "");
                                    } else {
                                        data.put("child_passport_select[" + numChild + "]", "0");
                                        data.put("child_passport_number[" + numChild + "]", passportNumber[member].getText().toString());
                                        data.put("child_poi[" + numChild + "]", cityId[country[member].getSelectedItemPosition()]);
                                        data.put("child_passport_expirity[" + numChild + "]", expirityPassport[member].getText().toString());
                                    }
                                    member++;
                                    numChild++;
                                }
                                for (int i = 0; i < order[3]; i++) {
                                    data.put("child_title[" + numChild + "]", gender[member].getSelectedItemPosition() + "");
                                    data.put("child_first_name[" + numChild + "]", firstName[member].getText().toString());
                                    data.put("child_last_name[" + numChild + "]", lastName[member].getText().toString());
                                    data.put("child_middle_name[" + numChild + "]", "");
                                    data.put("child_dob[" + numChild + "]", birthDate[member].getText().toString());
                                    int makanan = meal[member].getSelectedItemPosition();
                                    if (makanan == 3) makanan = 12;
                                    else if (makanan == 0) makanan = 1;
                                    data.put("child_meal[" + numChild + "]", makanan + "");
                                    data.put("child_room_type[" + numChild + "]", roomId[3]);
                                    for (int j = 0; j < visaCountryId.length; j++) {
                                        if (switchVisaChild[numVisa].isChecked()) {
                                            data.put("child_visa[" + numChild + "][" + visaCountryId[j] + "]", "1");
                                            data.put("child_visa_id[" + numChild + "][" + visaCountryId[j] + "]",
                                                    visaIdChild[j][listVisaChild[numVisa].getSelectedItemPosition()]);
                                        }
                                        numVisa++;
                                    }
                                    if (passportBaru[member].isChecked()) {
                                        data.put("child_passport_select[" + numChild + "]", pricePassportChildId[passport[member]
                                                .getSelectedItemPosition()]);
                                        data.put("child_passport[" + numChild + "]", "1");
                                        data.put("child_passport_number[" + numChild + "]", "");
                                        data.put("child_poi[" + numChild + "]", "");
                                        data.put("child_passport_expirity[" + numChild + "]", "");
                                    } else {
                                        data.put("child_passport_select[" + numChild + "]", "0");
                                        data.put("child_passport_number[" + numChild + "]", passportNumber[member].getText().toString());
                                        data.put("child_poi[" + numChild + "]", cityId[country[member].getSelectedItemPosition()]);
                                        data.put("child_passport_expirity[" + numChild + "]", expirityPassport[member].getText().toString());
                                    }
                                    member++;
                                    numChild++;
                                }
                                for (int i = 0; i < order[6]; i++) {
                                    data.put("infant_title[" + (i + 1) + "]", gender[member].getSelectedItemPosition() + "");
                                    data.put("infant_first_name[" + (i + 1) + "]", firstName[member].getText().toString());
                                    data.put("infant_last_name[" + (i + 1) + "]", lastName[member].getText().toString());
                                    data.put("infant_middle_name[" + (i + 1) + "]", "");
                                    data.put("infant_dob[" + (i + 1) + "]", birthDate[member].getText().toString());
                                    int makanan = meal[member].getSelectedItemPosition();
                                    if (makanan == 3) makanan = 12;
                                    else if (makanan == 0) makanan = 1;
                                    data.put("infant_meal[" + (i + 1) + "]", makanan + "");
                                    data.put("infant_room_type[" + (i + 1) + "]", roomId[5]);
                                    for (int j = 0; j < visaCountryId.length; j++) {
                                        if (switchVisaChild[numVisa].isChecked()) {
                                            data.put("infant_visa[" + (i + 1) + "][" + visaCountryId[j] + "]", "1");
                                            data.put("infant_visa_id[" + (i + 1) + "][" + visaCountryId[j] + "]",
                                                    visaIdChild[j][listVisaChild[numVisa].getSelectedItemPosition()]);
                                        }
                                        numVisa++;
                                    }
                                    if (passportBaru[member].isChecked()) {
                                        data.put("infant_passport_select[" + (i + 1) + "]", pricePassportChildId[passport[member]
                                                .getSelectedItemPosition()]);
                                        data.put("infant_passport[" + (i + 1) + "]", "1");
                                        data.put("infant_passport_number[" + (i + 1) + "]", "");
                                        data.put("infant_poi[" + (i + 1) + "]", "");
                                        data.put("infant_passport_expirity[" + (i + 1) + "]", "");
                                    } else {
                                        data.put("infant_passport_select[" + (i + 1) + "]", "0");
                                        data.put("infant_passport_number[" + (i + 1) + "]", passportNumber[member].getText().toString());
                                        data.put("infant_poi[" + (i + 1) + "]", cityId[country[member].getSelectedItemPosition()]);
                                        data.put("infant_passport_expirity[" + (i + 1) + "]", expirityPassport[member].getText().toString());
                                    }
                                    member++;
                                }
                                data.put("booking_notes", formTOF.getText().toString());
                                data.put("agree_termofservice", "on");
                                Log.d("DEBUG_"+TAG, "Data : "+data.toString());
                                return data;
                            }
                        };
                        req.setRetryPolicy(new DefaultRetryPolicy(
                                30000,
                                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                        Volley.newRequestQueue(TourBookActivity.this).add(req);
                    }
                    else {
                        Log.d("DEBUG_"+TAG, "No internet connection !");
                    }
                }
                else {
                    Log.d("DEBUG_"+TAG, "Data tidak lengkap");
                    Toast.makeText(TourBookActivity.this, "Lengkapi semua data terlebih dahulu", Toast.LENGTH_SHORT).show();
                    scrollView.scrollTo(0,0);
                }
            }
            else {
                Log.d("DEBUG_"+TAG, "Term and conditions belum disetujui");
                Toast.makeText(TourBookActivity.this, "Anda harus menyetujui Term and Conditions untuk melakukan Booking.",
                        Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void setData(String title, String description, String image, int[] order, String[] hargaProduk, String departure,
                        String arrival, String day, String flight, String city[], String cityId[],
                        String tourId, String[] roomId, String[] visaCountryId,
                        String[] visaCountryName, String[][] visaNameAdult, String[][] visaNameChild,
                        String[][] visaIdAdult, String[][] visaIdChild, String[][] visaPriceAdult,
                        String[][] visaPriceChild, int[] visaNumAdult, int[] visaNumChild) {
        Log.d("DEBUG_"+TAG, "Set data from detail tour");
        this.visaCountryId = visaCountryId;
        this.visaCountryName = visaCountryName;
        this.visaNameAdult = visaNameAdult;
        this.visaNameChild = visaNameChild;
        this.visaIdAdult = visaIdAdult;
        this.visaIdChild = visaIdChild;
        this.visaPriceAdult = visaPriceAdult;
        this.visaPriceChild = visaPriceChild;
        this.visaNumAdult = visaNumAdult;
        this.visaNumChild = visaNumChild;
        this.judul = title;
        this.description = description;
        this.image = image;
        this.order = order;
        this.hargaProduk = hargaProduk;
        this.departure = departure;
        this.arrival = arrival;
        this.day = day;
        this.flight = flight;
        this.city = city;
        this.cityId = cityId;
        this.tourId = tourId;
        this.roomId = roomId;
    }

    private void updateListVisa() {
        Log.d("DEBUG_"+TAG, "update visa");
        layoutRincianVisa.removeAllViews();
        visaPrice = 0;
        int visaOrderAdult[][] = new int[visaCountryId.length][20];//visaNumAdult];\
        for (i = 0; i < numVisaAdult; i++) {
            if (switchVisaAdult[i].isChecked()) {
                visaOrderAdult[i % visaCountryId.length][listVisaAdult[i].getSelectedItemPosition()]++;
            }
        }
        for (i = 0; i<visaCountryId.length; i++) {
            for (int j = 0; j<20; j++) {
                if (visaOrderAdult[i][j] != 0) {
                    View visaView = TourBookActivity.this.getLayoutInflater().inflate(R.layout.layout_book_tour_visa, null);
                    TextView titleVisa = (TextView) visaView.findViewById(R.id.book_tour_title_visa);
                    TextView priceVisa = (TextView) visaView.findViewById(R.id.book_tour_price_visa);
                    TextView numVisa = (TextView) visaView.findViewById(R.id.book_tour_num_visa);
                    titleVisa.setText(visaNameAdult[i][j] + " (adult) " + visaCountryName[i].toUpperCase());
                    numVisa.setText(visaOrderAdult[i][j]+" x "+formatter.RupiahFormat(visaPriceAdult[i][j]));
                    double price = Double.valueOf(visaPriceAdult[i][j])*visaOrderAdult[i][j];
                    visaPrice += price;
                    priceVisa.setText(formatter.RupiahFormat(Double.toString(price)));
                    layoutRincianVisa.addView(visaView);
                }
            }
        }

        //Child
        int visaOrderChild[][] = new int[visaCountryId.length][20];//visaNumChild];\
        for (i = 0; i < numVisaChild; i++) {
            if (switchVisaChild[i].isChecked()) {
                visaOrderChild[i % visaCountryId.length][listVisaChild[i].getSelectedItemPosition()]++;
            }
        }
        for (i = 0; i<visaCountryId.length; i++) {
            for (int j = 0; j<20; j++) {
                if (visaOrderChild[i][j] != 0) {
                    View visaView = TourBookActivity.this.getLayoutInflater().inflate(R.layout.layout_book_tour_visa, null);
                    TextView titleVisa = (TextView) visaView.findViewById(R.id.book_tour_title_visa);
                    TextView priceVisa = (TextView) visaView.findViewById(R.id.book_tour_price_visa);
                    TextView numVisa = (TextView) visaView.findViewById(R.id.book_tour_num_visa);
                    if (order[6]>0) {
                        titleVisa.setText(visaNameChild[i][j] + " (Child/Infant) " + visaCountryName[i].toUpperCase());
                    }
                    else {
                        titleVisa.setText(visaNameChild[i][j] + " (Child) " + visaCountryName[i].toUpperCase());
                    }
                    numVisa.setText(visaOrderChild[i][j]+" x "+formatter.RupiahFormat(visaPriceChild[i][j]));
                    double price = Double.valueOf(visaPriceChild[i][j])*visaOrderChild[i][j];
                    visaPrice += price;
                    priceVisa.setText(formatter.RupiahFormat(Double.toString(price)));
                    layoutRincianVisa.addView(visaView);
                }
            }
        }
        updateSubTotal();
    }

    private void getPassportPrice() {
        Log.d("DEBUG_"+TAG, "Get Passport Price");
        pd = ProgressDialog.show(TourBookActivity.this, "", "Loading...", true);
        String url= BuildConfig.URL_BASE+"/?method=list_passport&key="+BuildConfig.KEY;
        JsonObjectRequest jar = new JsonObjectRequest(Request.Method.GET,url,new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    response = response.getJSONObject("data");
                    response = response.getJSONObject("passport");
                    JSONArray hargaPassport;
                    JSONObject harga;

                    hargaPassport = response.getJSONArray("2");
                    harga = hargaPassport.getJSONObject(0);
                    pricePassportAdultId[0] = harga.getString("tour_passport_price_id");
                    pricePassportAdult[0] = harga.getDouble("value");
                    harga = hargaPassport.getJSONObject(1);
                    pricePassportChildId[0] = harga.getString("tour_passport_price_id");
                    pricePassportChild[0] = harga.getDouble("value");

                    hargaPassport = response.getJSONArray("3");
                    harga = hargaPassport.getJSONObject(0);
                    pricePassportAdultId[1] = harga.getString("tour_passport_price_id");
                    pricePassportAdult[1] = harga.getDouble("value");
                    harga = hargaPassport.getJSONObject(1);
                    pricePassportChildId[1] = harga.getString("tour_passport_price_id");
                    pricePassportChild[1] = harga.getDouble("value");

                    hargaPassport = response.getJSONArray("4");
                    harga = hargaPassport.getJSONObject(0);
                    pricePassportAdultId[2] = harga.getString("tour_passport_price_id");
                    pricePassportAdult[2] = harga.getDouble("value");

                    hargaPassport = response.getJSONArray("5");
                    harga = hargaPassport.getJSONObject(0);
                    pricePassportAdultId[3] = harga.getString("tour_passport_price_id");
                    pricePassportAdult[3] = harga.getDouble("value");

                    hargaPassport = response.getJSONArray("6");
                    harga = hargaPassport.getJSONObject(0);
                    pricePassportAdultId[4] = harga.getString("tour_passport_price_id");
                    pricePassportAdult[4] = harga.getDouble("value");
                    harga = hargaPassport.getJSONObject(1);
                    pricePassportChildId[2] = harga.getString("tour_passport_price_id");
                    pricePassportChild[2] = harga.getDouble("value");
                    pd.hide();
                    updateSubTotal();
                }
                catch (JSONException je) {
                    Log.d("DEBUG_"+TAG, "JSON Error : "+je.toString());
                    pd.hide();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("DEBUG_"+TAG, "Error : "+error.toString());
                pd.hide();
            }
        });
        jar.setRetryPolicy(new DefaultRetryPolicy(
                30000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Volley.newRequestQueue(TourBookActivity.this).add(jar);
    }

    private void updateSubTotal() {
        Log.d("DEBUG_"+TAG, "update total");
        subTotal = visaPrice + sumAdult + sumChildWithout + sumChildTwin + sumChildExtra +
                sumInternational + sumInfant + sumSingle + numPassportAdult[0] * pricePassportAdult[0] +
                numPassportAdult[1] * pricePassportAdult[1] + numPassportAdult[2] * pricePassportAdult[2] + numPassportAdult[3] *
                pricePassportAdult[3] + numPassportAdult[4] * pricePassportAdult[4]
                + numPassportChild[0] * pricePassportChild[0] +
                numPassportChild[1] * pricePassportChild[1] + numPassportChild[2] * pricePassportChild[2];
        subtotal.setText(formatter.RupiahFormat(subTotal + ""));
    }

    private ArrayAdapter<String> spinnerAdapter(String[] data) {
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(TourBookActivity.this, R.layout.adapter_spinner, data) {
            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                if (position == 0)
                    ((TextView) v).setTextColor(getResources().getColorStateList(R.color.spinner_place_holder));
                else
                    ((TextView) v).setTextColor(getResources().getColorStateList(android.R.color.black));
                return v;
            }
            public View getDropDownView(int position, View convertView, ViewGroup parent) {
                View v = super.getDropDownView(position, convertView, parent);
                if (position==0)
                    ((TextView) v).setTextColor(getResources().getColorStateList(R.color.spinner_place_holder));
                else
                    ((TextView) v).setTextColor(getResources().getColorStateList(android.R.color.black));
                return v;
            }
        };
        return adapter;
    }
}
