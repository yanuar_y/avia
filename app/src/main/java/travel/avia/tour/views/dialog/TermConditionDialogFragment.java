package travel.avia.tour.views.dialog;

import android.app.DialogFragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import travel.avia.tour.R;

public class TermConditionDialogFragment extends DialogFragment {

    private TextView termConditions, close;

    public static TermConditionDialogFragment newInstance() {
        return new TermConditionDialogFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.dialog_fragment_term_book_tour, container, false);

        Log.d("DEBUG_"+"TermConditionDF", "Inisialisasi");

        termConditions = (TextView) v.findViewById(R.id.term_book_tour_text);
        close = (TextView) v.findViewById(R.id.term_book_close_text);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("DEBUG_"+"TermConditionDF", "Dismiss Dialog");
                dismiss();
            }
        });
        termConditions.setText("TERMS & CONDITIONS AVIA TOUR\n\n" +
                "Berikut ini syarat dan ketentuan dalam pendaftaran product tour atau service lainnya dari Avia Tour.\n" +
                "Sebaiknya Anda membaca dengan jelas syarat dan kondisi di bawah sebelum mendaftar.\n" +
                "Dengan melanjutkan dan melakukan reservasi serta pembayaran deposit, maka Anda dianggap telah memahami dan menerima kondisi di bawah ini.\n" +
                "\n" +
                "TOUR BOOKING :\n" +
                "Pendaftaran peserta tour dapat melalui telepon & email yang tercantum di dalam website www.avia.travel.\n" +
                "Harga tour, visa, airport tax, fuel surcharge dapat berubah sewaktu-waktu dengan/tanpa pemberitahuan terlebih dahulu, jika terjadi perubahan nilai tukar (kurs) atau kenaikan tarif penerbangan.\n" +
                "\n" +
                "PENDAFTARAN & PELUNASAN :\n" +
                "\n" +
                "Pendaftaran dengan melampirkan paspor yang masih berlaku minimal 6 bulan (untuk beberapa rute tertentu minimal 1 tahun) dan Deposit biaya tour sebesar Idr 5.000.000/peserta atau sesuai dengan acara perjalanan yang dipilih dan ditambah biaya visa (apabila ada).\n" +
                "Pelunasan biaya tour paling lambat 14 hari sebelum tanggal keberangkatan (down payment / deposit adalah non-refundable / tidak bisa dikembalikan kepada peserta).\n" +
                "Pendaftaran atau pemesanan tempat yang dilakukan melalui telepon akan ditunggu pembayaran depositnya atau pelunasannya beserta paspor paling lambat 2 hari setelah tanggal pemesanan/pendaftaran.\n" +
                "Pendaftaran yang dilakukan kurang dari 15 hari sebelum tanggal keberangkatan harus segera melunasi biaya tour.\n" +
                "\n" +
                "BIAYA TOUR TERMASUK :\n" +
                "\n" +
                "Ticket pesawat udara p.p kelas ekonomi (non-endorsable, non-refundable dan non-reroutable berdasarkan group fare atau harga promosi lainnya). Terkecuali sebelumnya telah diberitahukan di mana biaya ticket penerbangan tidak termasuk.\n" +
                "Penginapan di hotel-hotel bertaraf internasional berdasarkan 2 (dua) orang dalam satu kamar (twin sharing).\n" +
                "Acara tour, transportasi (dalam bentuk rombongan) beserta semua ticket masuk, sesuai dengan acara perjalanan yang tercantum.\n" +
                "Makan sesuai dengan keterangan yang tercantum di dalam acara perjalanan. (MP - Makan Pagi; MS - Makan Siang; MM - Makan Malam).\n" +
                "Bagasi cuma-cuma maksimum 1 potong dengan berat maksimum 30 kg dan 1 handbag kecil untuk dibawa ke dalam kabin pesawat, atau sesuai dengan peraturan maskapai penerbangan yang dipakai.\n" +
                "Satu buah tas cantik bagi setiap peserta.\n" +
                "Tour leader dari Avia Tour.\n" +
                "\n" +
                "BIAYA TOUR TIDAK TERMASUK :\n" +
                "\n" +
                "Airport tax, fuel surcharge, asuransi penerbangan dan tax lainnya yang berlaku di negara/tempat yang dikunjungi.\n" +
                "Biaya pembuatan dokumen perjalanan seperti : paspor, visa, entry permit dll.\n" +
                "Pengeluaran pribadi seperti : mini bar, telepon, room service, cucian (laundry), tambahan makanan dan minuman serta pengeluaran lainnya yang bersifat pribadi.\n" +
                "Tour tambahan (optional tour) yang mungkin diadakan selama perjalanan.\n" +
                "Hotel tambahan / upgrade hotel dari yang sudah diatur dalam perjalanan.\n" +
                "Asuransi perjalanan.\n" +
                "Excess baggage (biaya kelebihan barang bawaan di atas 30 kg).\n" +
                "Biaya bea masuk bagi barang yang dikenakan bea masuk oleh duane di Jakarta maupun di negara-negara yang dikunjungi.\n" +
                "Biaya single supplement bagi peserta yang menempati 1 kamar sendiri.\n" +
                "Tips untuk porter-porter di hotel dan airport yang dituju (bila ada).\n" +
                "Tips untuk lokal guide, pengemudi lokal, pelayan restoran dan tour leader (informasi standard tips akan kami beritahukan dalam Hal Penting untuk peserta sebelum keberangkatan).\n" +
                "PPN 1%\n" +
                "Sesuai dengan produk tour yang di pilih. masing-masing produk memiliki list hal yang \"termasuk\" dan \"tidak termasuk\". Silahkan menghubungi kami untuk informasi lebih jelas. \n" +
                "\n" +
                "PERUBAHAN & PERPANJANGAN / DEVIASI / PENAMBAHAN / PENYIMPANGAN rute perjalanan di luar rute perjalanan yang telah dijadwalkan :\n" +
                "\n" +
                "Diperbolehkan minimal 1 bulan sebelum tanggal keberangkatan atau sebelum ticket dikeluarkan dan biaya yang timbul akan dibebankan kepada peserta.\n" +
                "Dapat dilakukan apabila jumlah peserta yang berangkat dan yang pulang telah memenuhi kuota dari ketentuan maskapai penerbangan.\n" +
                "Pihak Avia Tour akan mengusahakan tetapi tidak menjamin konfirmasi pesawat, hotel dan sebagainya bila peserta menghendaki perubahan atau perpanjangan (deviasi) dari jadwal paket tour semula.\n" +
                "Apabila perubahan & perpanjangan / deviasi / penambahan / penyimpangan rute perjalanan telah disetujui, akan dikenakan biaya sesuai dengan ketentuan maskapai penerbangan dan tidak dapat kembali ke jadwal semula.\n" +
                "Apabila permintaan deviasi tidak dapat disetujui oleh pihak maskapai penerbangan, maka peserta akan kembali ke jadwal semula.\n" +
                "Tidak ada pengembalian biaya tour untuk peserta yang deviasi dengan mempersingkat acara tour.\n" +
                "Ticket pesawat udara yang tidak terpakai tidak dapat diuangkan kembali (non-refundable).\n" +
                "PEMBATALAN / PENGUNDURAN DIRI / PINDAH TANGGAL KEBERANGKATAN / PINDAH ACARA TOUR \n" +
                "Terkecuali diinformasikan berbeda terlebih dahulu, maka biaya pembatalan / pengunduran diri / pindah tanggal keberangkatan / pindah acara tour adalah sebagai berikut :\n" +
                "\n" +
                "setelah pendaftaran: Uang Muka Pendaftaran (non-refundable)\n" +
                "30 - 15 hari kalender sebelum tanggal keberangkatan: 50% dari biaya tour\n" +
                "14 - 06 hari kalender sebelum tanggal keberangkatan: 75% dari biaya tour\n" +
                "05 hari kalender sebelum tanggal keberangkatan : 100% dari biaya tour\n" +
                "Biaya pembatalah di atas juga berlaku bagi :\n" +
                "\n" +
                "peserta yang terlambat memberikan kelengkapan persyaratan visa dari batas waktu yang telah ditentukan oleh Avia Tour dan mengakibatkan peserta tidak dapat berangkat tepat pada waktunya karena permohonan visanya masih diproses oleh kedutaan.\n" +
                "\n" +
                "TANGGUNG JAWAB :\n" +
                "Avia Tour selalu berusaha untuk memberikan pelayanan terbaik dan bertanggung jawab selama perjalanan, namun sebagai pelaksana,\n" +
                "\n" +
                "AVIA TOUR DAN SELURUH AGEN TIDAK BERTANGGUNG JAWAB DAN TIDAK BISA DITUNTUT ATAS :\n" +
                "\n" +
                "Kecelakaan, kerusakan, kehilangan dan keterlambatan tibanya bagasi ataupun segala macam ketidak-nyamanan yang disebabkan oleh maskapai penerbangan, hotel dan alat angkutan lainnya.  Penggantian didasarkan pada ketentuan maskapai penerbangan atau penyedia jasa pengangkutan yang digunakan.\n" +
                "Kegagalan, gangguan dan keterlambatan atau pembatalan dari pesawat udara / kereta api / alat angkutan lainnya yang menyebabkan kerugian waktu, tambahan biaya pergantian hotel, transportasi udara ataupun tidak digunakannya visa kunjungan yang telah dimiliki oleh peserta tour.\n" +
                "Kehilangan barang pribadi, koper, titipan barang di airport, hotel dan tindakan kriminal yang menimpa peserta tour selama perjalanan.\n" +
                "Perubahan acara perjalanan akibat dari bencana alam, perang, wabah penyakit, aksi teroris, kerusuhan dan lain sebagainya yang bersifat ‘Force Majeur’. (Force Majeure : Suatu kejadian yang terjadi di luar kemampuan manusia dan tidak dapat dihindarkan sehingga suatu kegiatan tidak dapat dilaksanakan sebagaimana mestinya.) Ketentuan-ketentuan ini dapat berubah sewaktu-waktu tanpa pemberitahuan terlebih dahulu, tergantung dari kebijakan pihak airlines, hotel & agen di luar negeri.\n" +
                "Segala pengeluaran tambahan yang disebabkan oleh ‘Force Majeur’.\n" +
                "Pengembalian biaya tour, reservasi hotel, transportasi udara dan lain-lain yang tidak terpakai yang disebabkan oleh ‘Force Majeur’.\n" +
                "Meninggalnya peserta akibat sakit yang diderita, kecelakaan, dll.\n" +
                "Dan sebab-sebab ketidak-nyamanan lainnya di luar kemampuan kami.\n" +
                "\n" +
                "AVIA TOUR DAN SELURUH AGEN-AGEN BERHAK :\n" +
                "\n" +
                "Membatalkan atau menunda tanggal keberangkatan apabila jumlah peserta kurang dari jumlah minimum peserta sesuai produk tour masing-masing.\n" +
                "Menunda tanggal keberangkatan yang dikarenakan oleh masalah ‘overbook’ maskapai penerbangan.\n" +
                "Merubah (memutar) rute & acara perjalanan sesuai dengan kondisi & konfirmasi dari maskapai penerbangan atau berdasarkan kondisi hotel di masing-masing kota / negara.\n" +
                "Mengganti hotel-hotel yang akan digunakan berhubung hotel tersebut sudah penuh dan lain-lain. Atau apabila dalam periode tour di kota-kota yang dikunjungi sedang berlangsung pameran / konferensi, atau hotel yang ditawarkan sedang penuh, maka akan diganti dengan hotel-hotel lain yang setaraf di kota terdekat atau sesuai dengan pertimbangan kondisi setempat dan konfirmasi.\n" +
                "Meminta peserta tour untuk keluar dari rombongan apabila peserta tour yang bersangkutan mencoba membuat kerusuhan, mengacaukan acara tour, meminta dengan paksa dan memberikan informasi yang tidak benar mengenai acara tour, dll.\n" +
                "Membatalkan pendaftaran peserta tour yang belum membayar uang muka atau pelunasan sesuai batas waktu yang telah ditentukan.\n" +
                "\n" +
                "VISA\n" +
                "\n" +
                "Dalam hal aplikasi visa (apabila rute perjalanan memerlukan visa), peserta bersedia memenuhi kelengkapan persyaratan dokumen sesuai jadwal dan ketentuan dari pihak Kedutaan.\n" +
                "Setiap kedutaan/konsulat mempunyai perbedaaan ketentuan dalam hal persyaratan data penunjang serta lamanya pembuatan visa, namun kami dapat membantu Anda dalam proses permohonan pembuatan visa (kecuali untuk negara-negara tertentu Anda harus mengajukannya sendiri), namun Avia Tour tidak menjamin diterimanya/dikabulkan permohonan tersebut.\n" +
                "Sesuai dengan ketentuan dari Kedutaan, biaya visa tetap harus dilunasi walaupun visa tidak disetujui oleh pihak Kedutaan.\n" +
                "Avia Tour tidak bertanggung jawab atas biaya-biaya tambahan maupun pengembalian uang dari biaya-biaya tour jika ternyata peserta tersebut mengalami deportasi atau penolakan dan wewenang imigrasi dengan alasan apapun, termasuk yang disebabkan oleh dokumen perjalanan yang tidak memenuhi syarat, karantina, peraturan-peraturan keimigrasian, hukum-hukum atau kebutuhan-kebutuhan yang dapat menyebabkan gangguan/perusakan terhadap manusia dan benda-benda.\n" +
                "Harga visa dapat berubah sewaktu-waktu tanpa pemberitahuan dan mengikuti kurs yang berlaku atau apabila ada perubahan kebijakan dari Kedutaan.\n" +
                "\n" +
                "PASSPORT\n" +
                "\n" +
                "Setiap kedutaan/kantor imigrasi/konsulat mempunyai perbedaaan ketentuan dalam hal persyaratan data penunjang serta lamanya pembuatan PASSPORT. Kami tidak bertanggung jawab atas biaya biaya tambahan maupun waktu penyelesaian dokumen PASSPORT.\n" +
                "Tidak ada pengembalian uang dari biaya-biaya tour atau bentuk pengembalian lain jika ternyata peserta tersebut mengalami deportasi atau penolakan dan wewenang imigrasi setempat dengan alasan apapun (walaupun telah memiliki visa), termasuk yang disebabkan oleh dokumen perjalanan yang tidak memenuhi syarat, karantina, peraturan-peraturan keimigrasian, hukum-hukum atau kebutuhan-kebutuhan yang dapat menyebabkan gangguan/perusakan terhadap manusia dan benda-benda.\n" +
                "Avia Tour tidak dapat melakukan pengembalian uang atas ditolaknya pembuatan PASSPORT ataupun VISA.\n" +
                "\n" +
                "PERMINTAAN KHUSUS \n" +
                "Jika ada permintaan khusus seperti menu makanan (vegetarian, diet dll), kamar yang bersebelahan / saling berhubungan, kursi tempat duduk di pesawat dan lain-lain, harap memberitahukan ke pihak kami pada saat pemesanan tour (reservasi) karena permintaan khusus tersebut membutuhkan waktu untuk konfirmasi / ketersediaannya dari pihak hotel / penerbangan / restoran. Untuk permintaan kursi tempat duduk di pesawat khusus, ada kalanya membutuhkan penambahan biaya dari maskapai penerbangan.\n" +
                "\n" +
                "LAIN-LAIN :\n" +
                "\n" +
                "Bagi peserta yang menempati Kamar Triple (1 kamar ditempati 3 orang), kami tidak menjamin tamu akan mendapat 3 tempat tidur yang terpisah, mengingat tempat tidur yang didapat tergantung dari kondisi hotel yang ada (bisa berupa 1 tempat tidur / kasur saja (ondol) / sofa bed / rollaway bed) dan tamu tidak mendapat pengurangan biaya.\n" +
                "Kamar yang tersedia di beberapa rute dapat berupa traditional room.\n" +
                "Untuk rute tertentu, hotel di beberapa area tidak memiliki fasilitas air conditioner.\n" +
                "Dalam beberapa acara perjalanan, peserta tour wajib mengunjungi toko lokal yang telah ditunjuk dan akan dikenakan biaya tambahan apabila tidak mengunjunginya.\n" +
                "Beberapa obyek wisata / acara perjalanan yang ditawarkan merupakan  fenomena  alam  yang dapat  diprediksi  tetapi  tidak  dapat  dipastikan,  sehingga ketersediaannya tergantung kondisi cuaca dan alam setempat. Bila tidak tersedia pada saat tour karena kondisi alam, tidak ada pengembalian biaya.\n" +
                "Bagi pendaftar yang berusia diatas 70 tahun atau memiliki keterbatasan fungsi anggota tubuh atau indera atau keterbatasan secara mental wajib didampingi olenh anggota keluarga, teman atau saudara yang akan bertanggung jawab selama perjalanan tour.\n" +
                "Bila permohonan visa ditolak, sedangkan ticket pesawat udara / kereta / kapal atau transportasi lainnya serta ticket objek wisata dan akomodasi sudah diterbitkan atau dibayarkan sebelum permohonan visa disetujui, karena keharusan sehubungan dengan tenggang waktu yang ditentukan perusahaan penerbangan (airlines) atau supplier lainnya, maka biaya visa tidak dapat dikembalikan dan Peserta tetap dikenakan denda pembatalan dan administrasi sesuai dengan kondisi terkait pihak airlines, hotel dan agen di luar negeri.\n" +
                "Demi kenyamanan dan kelancaran perencanaan perjalanan tour, Avia Tour berhak untuk menerbitkan dan membayarkan ticket pesawat udara domestik / internasional, kereta, kapal atau transportasi lainnya, serta ticket masuk objek wisata dan akomodasi tanpa melakukan konfirmasi lisan maupun tertulis kepada pendaftar yang telah melakukan deposit.\n" +
                "Jadwal tour dapat berubah sewaktu-waktu tanpa mengurangi isi dalam acara tour tersebut dengan  mengikuti kondisi yang memungkinkan.\n" +
                " \n" +
                "\n" +
                "PACKAGE:\n" +
                "\n" +
                "NO BOOKING WILL BE MADE UNTIL CONFIRMATION FROM OUR STAFF.\n" +
                "\n" +
                "BOOKING ANDA AKAN DI CONFIRM LEBIH LANJUT OLEH STAFF AVIA TOUR.\n" +
                "\n" +
                "PLS EMAIL AVIAWEB@AVIA-TOUR.COM FOR MORE INFO\n" +
                "\n" +
                "Minimum jumlah peserta adalah 2 pax (orang). \n" +
                "Menyertakan Foto Copy Passport/KTP.\n" +
                "Uang Muka Pendaftaran tidak dapat dikembalikan (down payment non-refundable) dan jumlah minimum Rp.1.000.000,00/ peserta.\n" +
                "Waktu Booking/ Pembukuan minimal 3-7 hari kerja sebelum jadwal keberangkatan.\n" +
                "Dalam hal aplikasi visa (jika ada), Peserta bersedia memenuhi kelengkapan persyaratan dokumen sesuai jadwal dan ketentuan dari pihak Kedutaan, dan biaya visa tetap harus dilunasi walaupun visa tidak disetujui oleh pihak Kedutaan.\n" +
                "Pelunasan dilakukan setelah booking/ pembukuan Confirm.\n" +
                "Peserta akan dikenai Biaya Cancellation Fee sebesar 100% jika melakukan last minute cancel (1 hari sebelum keberangkatan atau pada hari keberangkatan).\n" +
                "Acara perjalanan dapat berubah / diputar berdasarkan kondisi tiap-tiap penerbangan dan hotel di masing-masing kota / negara. \n" +
                "Apabila dalam periode package di kota-kota yang dikunjungi sedang berlangsung pameran / konferensi, atau hotel yang ditawarkan sedang penuh, maka akan diganti dengan hotel-hotel lain yang setaraf di kota terdekat.\n" +
                "Apabila terjadi Force Majeur (kondisi di luar kendali seperti : kehilangan, kerusakan, gangguan, keterlambatan sarana angkutan/transportasi, bencana alam dll) yang dapat mempengaruhi acara, maka acara akan dirubah dan bersifat non-refundable (tidak dapat dikembalikan).\n" +
                "Dan biaya package tidak termasuk segala pengeluaran tambahan yang disebabkan oleh Force Majeur.\n" +
                "Deviasi akan dikenakan biaya sesuai dengan kondisi Airlines yang bersangkutan.\n" +
                "Harga masih dapat berubah sewaktu-waktu dari pihak hotel atau tanpa pemberitahuan sebelumnya.\n" +
                "Dengan membayar uang muka pendaftaran biaya package, Anda dianggap telah memahami dan menerima syarat & kondisi di atas. Syarat & Kondisi Package selengkapnya berlaku sesuai dengan yang tertera di dalam brosur program Package Avia / website\n" +
                "\n" +
                "BIAYA PACKAGE TERMASUK:\n" +
                "Sesuai dengan produk yang di pilih. masing-masing produk memiliki list hal yang \"termasuk\" dan \"tidak termasuk\" silahkan menghubungi kami untuk info lebih jelas.\n" +
                "\n" +
                "\n" +
                "BIAYA PACKAGE TIDAK TERMASUK:\n" +
                "Harga belum termasuk Tax dan Fuel Surcharge (masih dapat berubah setiap saat tergantung dari pihak ke 3)\n" +
                "Harga belum termasuk Airport Tax Jakarta, Tax Internasional (jika ada), biaya visa (jika ada) dan asuransi perjalanan pribadi dapat berubah sewaktu-waktu, dan akan disesuaikan kepada peserta sebelum keberangkatan.\n" +
                "Biaya pengeluaran pribadi seperti laundry, minibar dll\n" +
                "Biaya tour tambahan\n" +
                "Tiket pesawat (tergantung package yang dipilih)\n" +
                "Sesuai dengan produk yang di pilih. masing-masing produk memiliki list hal yang \"termasuk\" dan \"tidak termasuk\" silahkan menghubungi kami untuk info lebih jelas.\n" +
                "\n" +
                "PERMINTAAN KHUSUS: \n" +
                "Jika ada permintaan khusus seperti menu makanan (vegetarian,diet dan lain-lain), kamar yang bersebelahan/saling berhubungan, kursi tempat duduk di pesawat dan lain-lain, harap memberitahukan ke pihak kami pada saat pemesanan tur (reservasi) karena permintaan khusus tersebut membutuhkan waktu untuk konfirmasi/ketersediaannya dan tidak terjamin dari pihak hotel/penerbangan/restoran.\n" +
                "\n" +
                "ATTRACTION:\n" +
                "Produk yang di jual oleh aviatour adalah atas kerjasama dengan pihak ke tiga (agen / supplier ) yang menyediakan atraksi tersebut. \n" +
                "Konfirmasi bookingan hanya dapat di berikan oleh Aviatour setelah menerima pembayaran oleh anda / pelanggan. ketersediaan kursi / availability adalah sepenuhnya hak pihak ke tiga.\n" +
                "Produk yang sudah di book tidak dapat di batalkan terkecuali dapat informasi dari pihak Supplier Attraction tersebut. \n" +
                "\n" +
                "CATATAN:\n" +
                "AVIA TOUR dan agen-agennya dapat sewaktu-waktu merubah/menyesuaikan acara tour dan hotel sesuai dengan keadaan setempat, baik susunan maupun urutannya.\n" +
                "\n" +
                "Harga yang tercantum di website dapat berubah sewaktu-waktu dan tidak mengikat.\n" +
                "\n" +
                "Untuk syarat pengajuan Visa ke Kedutaan , Travel Insurance yang digunakan adalah tipe Premier\n" +
                "\n" +
                "Untuk informasi lebih lanjut bisa menghubungi staff Avia Tour dengan mengisi contact form atau menghubungi telepon 021 4223838 / 021 422 3888.\n" +
                "\n" +
                "Terima kasih atas perhatian Anda.");
        return v;
    }
}
