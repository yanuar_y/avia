package travel.avia.tour.views.activity.tour;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import travel.avia.tour.R;
import travel.avia.tour.controls.utils.Formatter;

/**
 * Created by fitrahramadhan on 5/6/16.
 */
public class TourReservedActivity extends AppCompatActivity {

    private final static String TAG = "TourReservedAct";

    private TextView bookingCode, tourName, departureDate, arrivalDate, flight, bookingNote,
            contactName, contactEmail, contactAddress, contactPhone, totalPrice;
    private static LinearLayout layoutDetailTransaction;
    public static JSONObject dataResponse;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("DEBUG_"+TAG, "Set content view");
        setContentView(R.layout.activity_tour_reserved);

        Log.d("DEBUG_"+TAG, "Set icon action bar");
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Log.d("DEBUG_"+TAG, "Create object formatter");
        Formatter formatter = new Formatter();

        Log.d("DEBUG_"+TAG, "Initial layout");
        bookingCode = (TextView) findViewById(R.id.tour_reserved_booking_code);
        tourName = (TextView) findViewById(R.id.tour_reserved_tour_name);
        departureDate = (TextView) findViewById(R.id.tour_reserved_departure_date);
        arrivalDate = (TextView) findViewById(R.id.tour_reserved_arrival_date);
        flight = (TextView) findViewById(R.id.tour_reserved_flight);
        bookingNote = (TextView) findViewById(R.id.tour_reserved_booking_note);
        contactName = (TextView) findViewById(R.id.tour_reserved_contact_name);
        contactEmail = (TextView) findViewById(R.id.tour_reserved_contact_email);
        contactAddress = (TextView) findViewById(R.id.tour_reserved_contact_address);
        contactPhone = (TextView) findViewById(R.id.tour_reserved_contact_number);
        totalPrice = (TextView) findViewById(R.id.tour_reserved_total_price);
        layoutDetailTransaction = (LinearLayout) findViewById(R.id.tour_reserved_detail_transaction);

        try {
            Log.d("DEBUG_"+TAG, "Set summary data");
            JSONObject summary = dataResponse.getJSONObject("summary");
            bookingCode.setText(summary.getString("booking_code"));
            getSupportActionBar().setTitle("BOOKING ID "+summary.getString("booking_code"));
            tourName.setText(summary.getString("tour_name"));
            departureDate.setText(summary.getString("departure_date"));
            arrivalDate.setText(summary.getString("return_date"));
            flight.setText(summary.getString("flight"));
            bookingNote.setText(summary.getString("booking_notes"));
            JSONObject contactPerson = dataResponse.getJSONObject("contact_person");
            contactName.setText(contactPerson.getString("name"));
            contactEmail.setText(contactPerson.getString("email"));
            contactAddress.setText(contactPerson.getString("address"));
            contactPhone.setText(contactPerson.getString("contact_number"));
            JSONObject detailTransaction = dataResponse.getJSONObject("detail_transaction");
            JSONArray listTour = detailTransaction.getJSONArray("tour");
            int numListTour = listTour.length();
            JSONObject tour;
            Log.d("DEBUG_"+TAG, "Set data Tour");
            for (int i=0; i<numListTour; i++) {
                tour = listTour.getJSONObject(i);
                View layoutTransaction = this.getLayoutInflater().inflate(R.layout.layout_tour_reserved_detail_transaction, null);
                TextView totalTransaction = (TextView) layoutTransaction.findViewById(R.id.detail_transaction_total);
                TextView itemTransaction = (TextView) layoutTransaction.findViewById(R.id.detail_transaction_item);
                TextView priceTransaction = (TextView) layoutTransaction.findViewById(R.id.detail_transaction_price);
                totalTransaction.setText("Jumlah : "+tour.getString("total"));
                itemTransaction.setText(tour.getString("item"));
                priceTransaction.setText(formatter.RupiahFormat(tour.getString("subtotal")));
                layoutDetailTransaction.addView(layoutTransaction);
            }
            Log.d("DEBUG_"+TAG, "Set data Passport");
            try {
                JSONArray passport = dataResponse.getJSONArray("passport");
                int numPassport = passport.length();
                for (int i = 0; i < numPassport; i++) {
                    tour = passport.getJSONObject(i);
                    View layoutTransaction2 = this.getLayoutInflater().inflate(R.layout.layout_tour_reserved_detail_transaction, null);
                    TextView totalTransaction2 = (TextView) layoutTransaction2.findViewById(R.id.detail_transaction_total);
                    TextView itemTransaction2 = (TextView) layoutTransaction2.findViewById(R.id.detail_transaction_item);
                    TextView priceTransaction2 = (TextView) layoutTransaction2.findViewById(R.id.detail_transaction_price);
                    totalTransaction2.setText("Jumlah : "+tour.getString("qty"));
                    itemTransaction2.setText(tour.getString("tour_passport_price_name").trim());
                    priceTransaction2.setText(formatter.RupiahFormat(tour.getString("subtotal")));
                    layoutDetailTransaction.addView(layoutTransaction2);
                }
            }
            catch (JSONException j){}
            Log.d("DEBUG_"+TAG, "Se data Visa");
            try {
                JSONArray visa = dataResponse.getJSONArray("visa");
                int numVisa = visa.length();
                for (int i = 0; i < numVisa; i++) {
                    tour = visa.getJSONObject(i);
                    View layoutTransaction = this.getLayoutInflater().inflate(R.layout.layout_tour_reserved_detail_transaction, null);
                    TextView totalTransaction = (TextView) layoutTransaction.findViewById(R.id.detail_transaction_total);
                    TextView itemTransaction = (TextView) layoutTransaction.findViewById(R.id.detail_transaction_item);
                    TextView priceTransaction = (TextView) layoutTransaction.findViewById(R.id.detail_transaction_price);
                    totalTransaction.setText("Jumlah : "+tour.getString("qty"));
                    itemTransaction.setText(tour.getString("visa")+" ("+tour.getString("visa_category")+") "+tour.getString("country").toUpperCase());
                    priceTransaction.setText(formatter.RupiahFormat(tour.getString("subtotal")));
                    layoutDetailTransaction.addView(layoutTransaction);
                }
            }
            catch (JSONException e){

            }
            Log.d("DEBUG_"+TAG, "Set total price");
            totalPrice.setText(formatter.RupiahFormat(detailTransaction.getString("total")));
        }
        catch (JSONException je) {
            Toast.makeText(this, "Error JSON Format", Toast.LENGTH_LONG).show();
            this.finish();
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            Log.d("DEBUG_"+TAG, "Back clicked");
            finish();
        }
        return true;
    }
}
