package travel.avia.tour.views.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;

import travel.avia.tour.R;
import travel.avia.tour.controls.gcm.Registrasi;
import travel.avia.tour.controls.network.InternetConnection;

/**
 * Created by fitrahramadhan on 9/15/15.
 */
public class SplashScreenActivity extends Activity {
    private static final String TAG = "SplashScreen";

    BroadcastReceiver registrasi;
    private static final int PLAY_SERVICES = 9000;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splashscreen);
        registrasi = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (InternetConnection.isAvailable(SplashScreenActivity.this)) {
                    Thread splashThread = new Thread() {
                        @Override
                        public void run() {
                            try {
                                sleep(2000);
                            }
                            catch(InterruptedException e) {
                                Log.d("DEBUG_"+TAG, "error?");
                            }
                            finally {
                                Intent i = new Intent(SplashScreenActivity.this, MainActivity.class);
                                startActivity(i);
                                finish();
                            }
                        }
                    };
                    splashThread.start();
                }
                else {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(SplashScreenActivity.this);
                    alertDialog.setTitle("Avia Tour");
                    alertDialog.setMessage("Make sure your internet connection?");
                    alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Intent i = new Intent(SplashScreenActivity.this, SplashScreenActivity.class);
                            startActivity(i);
                            finish();
                        }
                    });
                    alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    });
                    alertDialog.show();
                }
            }
        };
        if (cekPlayService()) {
            Intent i = new Intent (SplashScreenActivity.this, Registrasi.class);
            startService(i);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(registrasi, new IntentFilter("Selesai"));
    }

    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(registrasi);
    }

    protected boolean cekPlayService() {
        int kodehasil = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (kodehasil != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(kodehasil)) {
                GooglePlayServicesUtil.getErrorDialog(kodehasil, this, PLAY_SERVICES).show();
            }
            else {
                finish();
            }
            return false;
        }
        return true;
    }
}
