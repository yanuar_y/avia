package travel.avia.tour.views.activity.tour;

import android.app.DialogFragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import travel.avia.tour.BuildConfig;
import travel.avia.tour.R;
import travel.avia.tour.controls.network.InternetConnection;
import travel.avia.tour.controls.utils.Formatter;
import travel.avia.tour.views.adapter.recyclerview.ListTourAdapter;
import travel.avia.tour.views.custom.recyclerview.ItemClickSupport;
import travel.avia.tour.views.dialog.SortListTourDialogFragment;

/**
 * Created by fitrahramadhan on 4/29/16.
 */
public class SearchTourActivity extends AppCompatActivity {

    private static final String TAG = "SearchTourAct";

    private static String tourName, regionId, countryId, duration, departure;
    public static int sort = 0;

    private RecyclerView list_tour;
    private ProgressBar progressBar;
    private LinearLayout dataNotFound, noConnection;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_list_tour);

        Log.d("DEBUG_"+TAG, "Initial home button");
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Log.d("DEBUG_"+TAG, "Initial view");
        list_tour = (RecyclerView) findViewById(R.id.list_tour_region);
        progressBar = (ProgressBar) findViewById(R.id.list_tour_progress);
        dataNotFound = (LinearLayout) findViewById(R.id.list_tour_not_found);
        noConnection = (LinearLayout) findViewById(R.id.list_tour_no_connection);

        list_tour.setLayoutManager(new LinearLayoutManager(this));

        getTour();
    }

    public void getTour() {
        Log.d("DEBUG_"+TAG, "get data tour");
        if (InternetConnection.isAvailable(this)) {
            Log.d("DEBUG_"+TAG, "get tour");
            list_tour.setVisibility(View.INVISIBLE);
            dataNotFound.setVisibility(View.INVISIBLE);
            progressBar.setVisibility(View.VISIBLE);
            noConnection.setVisibility(View.INVISIBLE);
            String url = BuildConfig.URL_BASE +
                    "/?method=search"+
                    "&key="+ BuildConfig.KEY +
                    "&region="+ regionId +
                    "&name_destination="+ tourName+
                    "&country="+ countryId+
                    "&days="+ duration+
                    "&month_year="+ departure+
                    "&sortTour="+ (sort+1);
            JsonObjectRequest jar = new JsonObjectRequest(Request.Method.GET,url,new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.d("DEBUG_"+TAG, "get tour berhasil");
                    Log.d("DEBUG_"+TAG, "Data : "+response.toString());
                    progressBar.setVisibility(View.INVISIBLE);
                    int numTour = 0;
                    JSONObject tour;
                    try {
                        JSONObject listTour = response.getJSONObject("data");
                        try {

                            while (true) {
                                tour = listTour.getJSONObject(numTour+"");
                                numTour++;
                            }
                        }
                        catch (JSONException jje) {
                            if (numTour == 0) {
                                dataNotFound.setVisibility(View.VISIBLE);
                            } else {
                                String[] imageTour = new String[numTour];
                                String[] titleTour = new String[numTour];
                                String[] priceTour = new String[numTour];
                                final String[] idTour = new String[numTour];
                                final String[] descriptionTour = new String[numTour];
                                Formatter harga = new Formatter();
                                for (int i = 0; i < numTour; i++) {
                                    tour = listTour.getJSONObject(i + "");
                                    imageTour[i] = BuildConfig.URL_IMAGE + tour.getString("image_name");
                                    titleTour[i] = tour.getString("tour_master_name");
                                    priceTour[i] = "Start form " + harga.RupiahFormat(tour.getString("price_in_idr"));
                                    descriptionTour[i] = tour.getString("tour_master_description");
                                    idTour[i] = tour.getString("tour_product_id");
                                }
                                ListTourAdapter adapterListNewest = new ListTourAdapter(SearchTourActivity.this,
                                        imageTour, titleTour, priceTour);
                                list_tour.setAdapter(adapterListNewest);
                                list_tour.setVisibility(View.VISIBLE);
                                ItemClickSupport.addTo(list_tour).setOnItemClickListener
                                        (new ItemClickSupport.OnItemClickListener() {
                                            @Override
                                            public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                                                TourDetailActivity.tourId = idTour[position];
                                                TourDetailActivity.deskripsi = descriptionTour[position];
                                                Intent i = new Intent(SearchTourActivity.this, TourDetailActivity.class);
                                                startActivity(i);
                                            }
                                        });
                            }
                        }

                        /*JSONArray listTour = response.getJSONArray("data");
                        numTour = listTour.length();
                        if (numTour == 0) {
                            dataNotFound.setVisibility(View.VISIBLE);
                        } else {
                            String[] imageTour = new String[numTour];
                            String[] titleTour = new String[numTour];
                            String[] priceTour = new String[numTour];
                            final String[] idTour = new String[numTour];
                            final String[] descriptionTour = new String[numTour];
                            Formatter harga = new Formatter();
                            for (int i = 0; i < numTour; i++) {
                                tour = listTour.getJSONObject(i);
                                imageTour[i] = BuildConfig.URL_IMAGE + tour.getString("image_name");
                                titleTour[i] = tour.getString("tour_master_name");
                                priceTour[i] = "Start form " + harga.RupiahFormat(tour.getString("price_in_idr"));
                                descriptionTour[i] = tour.getString("tour_master_description");
                                idTour[i] = tour.getString("tour_product_id");
                            }
                            ListTourAdapter adapterListNewest = new ListTourAdapter(SearchTourActivity.this,
                                    imageTour, titleTour, priceTour);
                            list_tour.setAdapter(adapterListNewest);
                            list_tour.setVisibility(View.VISIBLE);
                            ItemClickSupport.addTo(list_tour).setOnItemClickListener
                                    (new ItemClickSupport.OnItemClickListener() {
                                        @Override
                                        public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                                            TourDetailActivity.tourId = idTour[position];
                                            TourDetailActivity.deskripsi = descriptionTour[position];
                                            Intent i = new Intent(SearchTourActivity.this, TourDetailActivity.class);
                                            startActivity(i);
                                        }
                                    });
                        }*/
                    } catch (JSONException je) {
                        Log.d("DEBUG_"+TAG, "JSON Error : "+je.toString());
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.d("DEBUG_" + TAG, "Error : " + error.toString());
                    list_tour.setVisibility(View.INVISIBLE);
                    dataNotFound.setVisibility(View.INVISIBLE);
                    progressBar.setVisibility(View.INVISIBLE);
                    noConnection.setVisibility(View.VISIBLE);
                }
            });
            jar.setRetryPolicy(new DefaultRetryPolicy(
                    30000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            Volley.newRequestQueue(SearchTourActivity.this).add(jar);
        }
        else {
            Log.d("DEBUG_"+TAG, "No internet");
            list_tour.setVisibility(View.INVISIBLE);
            dataNotFound.setVisibility(View.INVISIBLE);
            progressBar.setVisibility(View.INVISIBLE);
            noConnection.setVisibility(View.VISIBLE);
        }
    }

    public void setData(String tourName, String regionId, String countryId, String duration, String departure) {
        Log.d("DEBUG_"+TAG, "Set data search");
        this.tourName = tourName;
        this.regionId = regionId;
        this.countryId = countryId;
        this.duration = duration;
        this.departure = departure;
    }

    @Override
    public void onBackPressed() {
        finish();
        super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        Log.d("DEBUG_"+TAG, "Set menu");
        getMenuInflater().inflate(R.menu.menu_sort, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            Log.d("DEBUG_"+TAG, "home cliked");
            finish();
        }
        else if (id == R.id.sort) {
            Log.d("DEBUG_"+TAG, "Sort clicked");
            SortListTourDialogFragment newFragment = SortListTourDialogFragment.newInstance();
            newFragment.setStyle(DialogFragment.STYLE_NO_TITLE, 0);
            newFragment.show(getFragmentManager(), "dialog");
        }
        return true;
    }
}
