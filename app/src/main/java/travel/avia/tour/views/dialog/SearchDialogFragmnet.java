package travel.avia.tour.views.dialog;

import android.app.DialogFragment;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;

import travel.avia.tour.BuildConfig;
import travel.avia.tour.R;
import travel.avia.tour.views.activity.tour.SearchTourActivity;

public class SearchDialogFragmnet extends DialogFragment {

    private static final String TAG = "SearchDialog";

    private String idRegion[], idCountry[], idDuration[], idDeparture[];
    private String nameRegion[], nameCountry[], nameDuration[], nameDeparture[];

    private EditText tourName;
    private Spinner region, departure, country, duration;
    private Button search;

    public static SearchDialogFragmnet newInstance() {
        return new SearchDialogFragmnet();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.dialog_fragment_search, container, false);

        Log.d("DEBUG_"+TAG, "Initial and get data region");
        region = (Spinner) v.findViewById(R.id.search_region);
        getRegion();
        Log.d("DEBUG_"+TAG, "Initial and get data departure");
        departure = (Spinner) v.findViewById(R.id.search_departure);
        getDeparture();
        Log.d("DEBUG_"+TAG, "Initial and get data country");
        country = (Spinner) v.findViewById(R.id.search_country);
        region.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                getCountry();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        Log.d("DEBUG_"+TAG, "Initial and get data duration");
        duration = (Spinner) v.findViewById(R.id.search_duration);
        getDuration();
        tourName = (EditText) v.findViewById(R.id.search_tour_name);

        search = (Button) v.findViewById(R.id.search_tour);

        Log.d("DEBUG_"+TAG, "Search clicked");
        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SearchTourActivity lta = new SearchTourActivity();
                try {
                    lta.setData(tourName.getText().toString(),
                            idRegion[region.getSelectedItemPosition()],
                            idCountry[country.getSelectedItemPosition()],
                            idDuration[duration.getSelectedItemPosition()],
                            idDeparture[departure.getSelectedItemPosition()]);
                }
                catch (NullPointerException npe) {
                    lta.setData(tourName.getText().toString(),
                            "",
                            "",
                            idDuration[duration.getSelectedItemPosition()],
                            idDeparture[departure.getSelectedItemPosition()]);
                }
                Intent i = new Intent(getActivity(), SearchTourActivity.class);
                startActivity(i);
            }
        });
        
        return v;
    }

    private void getRegion() {
        String url= BuildConfig.URL_BASE+"/?method=list_region&key="+BuildConfig.KEY;
        JsonObjectRequest jar = new JsonObjectRequest(Request.Method.GET,url,new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("DEBUG_"+TAG, "Get data berasil");
                try {
                    JSONArray listRegion = response.optJSONArray("data");
                    int numRegion = listRegion.length();
                    idRegion = new String[numRegion+1];
                    nameRegion = new String[numRegion+1];
                    idRegion[0] = "";
                    nameRegion[0] = "All Region";
                    JSONObject region;
                    for (int i = 0; i < numRegion; i++) {
                        region = listRegion.getJSONObject(i);
                        idRegion[i+1] = region.getString("tour_region_id");
                        nameRegion[i+1] = region.getString("tour_region_name").toUpperCase();
                    }
                }
                catch (JSONException je) {
                    Log.d("DEBUG_"+TAG, "JSON Error : "+je.toString());
                }
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
                        R.layout.spinner_item, nameRegion);
                region.setAdapter(adapter);
            }
        },  new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("DEBUG_"+TAG, "Error : "+error.toString());
                getRegion();
            }
        });
        jar.setRetryPolicy(new DefaultRetryPolicy(
                15000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Volley.newRequestQueue(getActivity()).add(jar);
    }
    
    private void getDuration() {
        nameDuration = new String[6];
        idDuration = new String[6];
        nameDuration[0] = "All Duration";
        nameDuration[1] = "1-2 Days";
        nameDuration[2] = "3-5 Days";
        nameDuration[3] = "6-10 Days";
        nameDuration[4] = "11-15 Days";
        nameDuration[5] = "16-20 Days";
        idDuration[0] = "";
        idDuration[1] = "1-2";
        idDuration[2] = "3-5";
        idDuration[3] = "6-10";
        idDuration[4] = "11-15";
        idDuration[5] = "16-20";
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.spinner_item, nameDuration);
        duration.setAdapter(adapter);
    }

    private void getDeparture() {
        Calendar cal = Calendar.getInstance();
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH);
        nameDeparture = new String[25-month];
        idDeparture = new String[25-month];
        nameDeparture[0] = "All Departure Month";
        idDeparture[0] = "";
        int indexMonth = 1;
        for (int i=month; i<12; i++) {
            nameDeparture[indexMonth] = getMonth(i)+" "+year;
            if (i<10) {
                idDeparture[indexMonth] = "0"+i+"-"+year;
            }
            else {
                idDeparture[indexMonth] = i+"-"+year;
            }
            indexMonth++;
        }
        year++;
        for (int i=0; i<12; i++) {
            nameDeparture[indexMonth] = getMonth(i)+" "+year;
            if (i<10) {
                idDeparture[indexMonth] = "0"+i+"-"+year;
            }
            else {
                idDeparture[indexMonth] = i+"-"+year;
            }
            indexMonth++;
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
                R.layout.spinner_item, nameDeparture);
        departure.setAdapter(adapter);
    }

    private String getMonth(int month){
        String[] monthNames = {"January", "February", "March", "April", "May", "June", "July",
                "August", "September", "October", "November", "December"};
        return monthNames[month];
    }

    private void getCountry() {
        country.setVisibility(View.GONE);
        if (!(region.getSelectedItemPosition()==0)) {
            String url= BuildConfig.URL_BASE+"/?method=get_country_from_region&region_id="+idRegion
                    [region.getSelectedItemPosition()]+"&key="+BuildConfig.KEY;
            JsonObjectRequest jar = new JsonObjectRequest(Request.Method.GET,url,new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        JSONArray listCountry = response.optJSONArray("data");
                        int numCountry= listCountry.length();
                        idCountry = new String[numCountry+1];
                        nameCountry = new String[numCountry+1];
                        idCountry[0] = "";
                        nameCountry[0] = "All Country";
                        JSONObject country;
                        for (int i = 0; i < numCountry; i++) {
                            country = listCountry.getJSONObject(i);
                            idCountry[i+1] = country.getString("tour_country_id");
                            nameCountry[i+1] = country.getString("tour_country_name").toUpperCase();
                        }
                    }
                    catch (JSONException je) {}
                    country.setVisibility(View.VISIBLE);
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
                            R.layout.spinner_item, nameCountry);
                    country.setAdapter(adapter);
                }
            },  new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            });
            Volley.newRequestQueue(getActivity()).add(jar);
        }
        else {
            nameCountry = new String[1];
            idCountry = new String[1];
            nameCountry[0] = "All Country";
            idCountry[0] = "";
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
                    R.layout.spinner_item, nameCountry);
            country.setAdapter(adapter);
        }
    }
}
