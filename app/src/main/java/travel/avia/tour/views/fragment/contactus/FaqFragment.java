package travel.avia.tour.views.fragment.contactus;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import travel.avia.tour.BuildConfig;
import travel.avia.tour.R;
import travel.avia.tour.views.adapter.expandablelistview.ListFaqAdapter;

/**
 * Created by fitrahramadhan on 2/7/16.
 */
public class FaqFragment extends Fragment implements
        ExpandableListView.OnGroupClickListener,
        ExpandableListView.OnChildClickListener {

    private final static String TAG  = "FaqFragment";

    private List<String> parentJudul, List1;
    private HashMap<String,List<String>> childMap;

    private ExpandableListView elv;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_faq, container, false);

        getActivity().setTitle("FAQs");

        Log.d("DEBUG_"+TAG, "Intial expandable lisview");
        elv = (ExpandableListView) v.findViewById(R.id.faq);

        Log.d("DEBUG_"+TAG, "Get data faq");
        getFaq();

        return v;
    }

    @Override
    public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
        return false;
    }

    @Override
    public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
        return false;
    }

    private void getFaq() {
        String url= BuildConfig.URL_BASE+"/?method=faq&key="+BuildConfig.KEY;;
        JsonObjectRequest jar = new JsonObjectRequest(Request.Method.GET,url,new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("DEBUG_"+TAG, "Get data berhasil");
                parentJudul = new ArrayList<>();
                childMap = new HashMap<>();

                String[] parent;
                String[] discover;
                try {
                    response = response.getJSONObject("data");
                    response = response.getJSONObject("faq");
                    JSONArray faq = response.getJSONArray("General Inquiry");
                    int numFaq = faq.length();
                    parent  = new String[numFaq];
                    discover = new String[1];

                    JSONObject dataFaq;
                    for (int i=0; i<numFaq; i++) {
                        dataFaq = faq.getJSONObject(i);
                        parent[i] = dataFaq.getString("faq_name");
                    }
                    Collections.addAll(parentJudul, parent);
                    for (int i=0; i<numFaq; i++) {
                        dataFaq = faq.getJSONObject(i);
                        List1 = new ArrayList<>();
                        discover[0] = dataFaq.getString("faq_content");
                        Collections.addAll(List1, discover);
                        childMap.put(parentJudul.get(i), List1);
                    }
                }
                catch (JSONException je) {
                    Log.d("DEBUG_"+TAG, "JSON Error : "+je.toString());
                }
                ListFaqAdapter adapter = new ListFaqAdapter(getActivity(), parentJudul, childMap);
                elv.setAdapter(adapter);
                elv.setOnGroupClickListener(FaqFragment.this);
                elv.setOnChildClickListener(FaqFragment.this);
            }
        },  new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("DEBUG_"+TAG, "Error : "+error.toString());
                getFaq();
            }
        });
        Volley.newRequestQueue(getActivity()).add(jar);
    }
}
